#!/bin/bash

export DOAUAU=1
export DOQA=0
export DOEMBEDDING=0
export CENTRAL=1
export RRHO=0.3
export ZVERTEX=30
export MAXRAP=1.0
export DCA=1.0
REFMULTMIN=396
REFMULTMAX=100000
NJETSREMOVE=2

TIMESTAMP=$(date +%s)
MAKELISTS=1

i=0
for RPARAM in 0.2 0.3 0.4
do
export RPARAM
let "i=i+1"
if [ $i -gt 1 ]; then
		MAKELISTS=0
fi
if [ $RPARAM == "0.2" ]; then
  ACUT=0.05
fi
if [ $RPARAM == "0.3" ]; then
   ACUT=0.2
fi
if [ $RPARAM == "0.4" ]; then
   #ACUT=0.35
	ACUT=0.4
	NJETSREMOVE=1
fi
if [ $RPARAM == "0.5" ]; then
   ACUT=0.65
fi
export ACUT

if [ $CENTRAL -eq 1 ]; then
      CENTRALITY="0-10"
elif [ $CENTRAL -eq 0 ]; then
      CENTRALITY="60-80" 
		NJETSREMOVE=1
		REFMULTMIN=10
		REFMULTMAX=31
fi 
export REFMULTMIN
export REFMULTMAX

SUFF="_HFpico" #outputdir suffix
COLSYS="AuAu"

BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/star_run11_filelist.list"
if [ $DOAUAU -eq 0 ]; then #pp data
	BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run12/star_run12_pp_filelist.list"
	COLSYS="pp"
	NJETSREMOVE=1
fi
export NJETSREMOVE

OUTDIR="$HOME/jet_analysis/STARJet/out/MB/"
BASEDIR="$HOME/jet_analysis/STARJet/analysis_root4star/submitter"
LOGDIR="$BASEDIR/log"
export MACRODIR="$HOME/jet_analysis/STARJet/analysis_root4star"
SCRIPTNAME="run_job.sh"

BASENAME="incl"
if [ $DOEMBEDDING -eq 1 ]; then
	BASENAME="emb"
fi
if [ $DOQA -eq 1 ]; then
	BASENAME="qa"
fi

TYPE="${BASENAME}_${COLSYS}_R${RPARAM}_${CENTRALITY}cent_A${ACUT}_rhoR${RRHO}_nrem${NJETSREMOVE}$SUFF"

NFILES=400 #how many files merge into one batch
if [ $DOAUAU -eq 0 ]; then #pp data
	NFILES=100 #how many files merge into one batch
fi
#MAX=1
LINES=`cat $BIG_FILELIST | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"

N=1
if [ $DOEMBEDDING -eq 1 ]; then
	N=20 #take every Nth batch
fi
let ADD=$NFILES*$N
for ((FIRST=1; FIRST < $MAX; FIRST += $ADD))
  do
	LAST=$((FIRST + NFILES - 1 ))	
	if [ $LAST -gt $LINES ]; then
	LAST=$LINES
	fi
	LISTNAME="list_${TIMESTAMP}_${FIRST}"
	FILELIST_SMALL="${BASEDIR}/filelists/${LISTNAME}.list"

	#echo "lines: $FIRST - $LAST - $LINES"
	if [ $MAKELISTS -eq 1 ]; then
		./split_list.sh $FIRST $LAST $LINES $BIG_FILELIST $FILELIST_SMALL
		echo "generating list: $FILELIST_SMALL"
	fi
  	export JOBNAME="${BASENAME}_${DOAUAU}_${DOEMBEDDING}_${DOQA}_R${RPARAM}_${FIRST}"
  	export OUT_PATH="${OUTDIR}/${TYPE}/${FIRST}"
	export IN_FILELIST="$FILELIST_SMALL"

  if [ ! -e $OUT_PATH ]; then
      mkdir -p $OUT_PATH
  fi

qsub -P star -m n -l h_vmem=2G -l h_rt=14:00:00 -N $JOBNAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
done #jobs
done #R

