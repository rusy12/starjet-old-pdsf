#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet_root4star

cd $MACRODIR

root4star -q -b -l run_analysis.C 2>&1
