#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE"
    exit 1
}

TRG="MB" #HT, MB
export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BGD - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


BASEDIR="$ANALYSISDIR"
LOGDIR="$BASEDIR/submitter/log"
export MacroDIR="$BASEDIR/unfolding"
WRKDIR="$BASEDIR/../out/$TRG"
export SVD=1 # SVD unfolding instead of Bayes
#export NBINS=100
export NBINS=VAR
export NITER=7 #number of k-terms
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
   export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
export EFFICORR=1 # do efficiency correction
export SUF="_alpha" #output dir suffix

export PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15"

for CENTRAL in 0 #1
do
if [ $CENTRAL -eq 1 ]; then
	SUFFIX=""
	PTLEADCUTS="5 6 7"
else
	SUFFIX="_peripheral"
	PTLEADCUTS="4 5 6"
fi
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

for SYSSUFF in "_normal" "_m5" "_p5" "_2u1g" "_v2" "_AuAu" #choice of different results for sys. err. calculation: "_normal" | "_m5" | "_p5" | "_2u1g" | "_AuAu"
do
export SYSSUFF

for RPARAM in 0.5 0.2 0.3 0.4
do
export RPARAM
export DATA_PATH="$WRKDIR/inclusive${SUFFIX}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding${SUFFIX}/rmatrix${SYSSUFF}"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUFF}/epsilon"

#choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
for BININGCH in 1 4 
do
export BININGCH 

	for PTTHRESH in `echo $PTLEADCUTS`
   do
     export PTTHRESH
      NAME="unfSVD_R${RPARAM}_pTl${PTTHRESH}_$PRIOR"
		qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_unfoldSVD.sh
		#cd $MacroDIR
		#root -b -l -q unfold_roounfold.C
	done #pTleading
done #bining
done #R
done #suffix
done #centrality
