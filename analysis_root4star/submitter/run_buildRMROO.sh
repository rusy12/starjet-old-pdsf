#!/bin/bash
source  $HOME/privatemodules/pdsf.bashrc
module load use.own
module load starjet/starjet_root4star

cd $WORKDIR

for PRIOR in `echo $PRIORS` 
do
	export PRIOR
	root4star -l buildResponseROO.C -q -b
done
