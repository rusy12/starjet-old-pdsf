#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet_root4star

cd $WORKDIR

#root -l make_epsilon.C -q -b
root4star -l buildResponseM.C -q -b
