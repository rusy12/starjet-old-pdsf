#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE"
    exit 1
}

TRG="MBHT" #HT, MB
export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BGD - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


BASEDIR="$ANALYSISDIR"
LOGDIR="$BASEDIR/submitter/log"
export MacroDIR="$BASEDIR/unfolding"
WRKDIR="$BASEDIR/../out/$TRG"
#SCRIPTNAME="run_unfold_allpTlead_allpriors.sh"
SCRIPTNAME="run_unfold_allpriors.sh"
	#SCRIPTNAME="run_unfold_allpTlead.sh"
export SMOOTH=0 #smooth unfolded spectrum between iterations, for Bayes only
#export NBINS=100
export NBINS=VAR
export NITER=8 #number of k-terms
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
   export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
export EFFICORR=1 # do efficiency correction
export SUF="_eta" #output dir suffix

export PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15"

for SVD in 0 1 # 1:SVD unfolding | 0:Bayesian unfolding
do
export SVD 
if [ $SVD -eq 1 ]; then
	UNFTYPE="SVD"
else
	UNFTYPE="Bayes"
fi


for CENTRAL in 2 #0 1 #0: peripheral Au+Au | 1: central Au+Au | 2: p+p
do
export CENTRAL
USE2DHISTO=1
if [ $CENTRAL -eq 1 ]; then
	SUFFIX=""
	PTLEADCUTS="5 6 7"
elif [ $CENTRAL -eq 0 ]; then
	SUFFIX="_peripheral"
	PTLEADCUTS="4 5 6 7"
else #p+p
	SUFFIX="_pp"
	PTLEADCUTS="3 4 5 6"
	USE2DHISTO=0 #for combined MB+HT spectrum we have only 1D histograms
fi
export USE2DHISTO
export PTLEADCUTS
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

for SYSSUFF in "_tofp5" "_tofm5" "_normal" "_p5" "_m5" "_g" "_u"   # "_v2" "_AuAu" #choice of different results for sys. err. calculation
do
export SYSSUFF

for RPARAM in 0.2 0.3 0.4
do
export RPARAM
export DATA_PATH="$WRKDIR/inclusive${SUFFIX}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding${SUFFIX}/rmatrix${SYSSUFF}"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUFF}/epsilon"

#choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
for BININGCH in 4 #0 1 #2 3 4 #1 4 
do
export BININGCH 

	for PTTHRESH in `echo $PTLEADCUTS`
   do
     export PTTHRESH
		#for PRIOR in `echo $PRIORS`
		#do
   	#export PRIOR
      NAME="unf${UNFTYPE}_R${RPARAM}_pTl${PTTHRESH}_$PRIOR"
		qsub -P star -m n -l h_vmem=1G -l h_rt=5:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
		#cd $MacroDIR
		#root -b -l -q unfold_roounfold.C
	#done #prior
	done #pTleading
done #bining
done #R
done #suffix
done #centrality
done #unfolding
