#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_pyt | dete | BGD)"
    exit 1
}

export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


TRG="MB"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export WORKDIR="$BASEDIR/response_matrix"
for CENTRAL in  2 #2
do
if [ $CENTRAL -eq 1 ]; then
CENTSUFF=""
PTLEADCUTS="5 6 7"
elif [ $CENTRAL -eq 0 ]; then
CENTSUFF="_peripheral"
PTLEADCUTS="4 5 6 7"
else #p+p
CENTSUFF="_pp"
PTLEADCUTS="3 4" #"0 1 2 3 4 5 6 7"
fi
PRIORS=2 #"2 4 5 6 7 8 9 10 11 12 13 14 15" #0: -, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4.5) 5: pT^(-5) 6:pT^(-5.5) 7: levy 8: levy II
export PRIORS

for SUFF in "_u" "_g"  "_m5" "_p5" "_tofm5" "_tofp5" "_normal" #"_AuAu" "_v2" 
do
for RPARAM in 0.2 0.3 0.4
do
export RPARAM
export RM_PATH="$STARJETBASEDIR/out/${TRG}/embedding${CENTSUFF}/rmatrix${SUFF}"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${SUFF}"
export PRIOR_PATH="$STARJETBASEDIR/out/prior"

for PTTHRESH in `echo $PTLEADCUTS`
do
      export PTTHRESH
      NAME="buildRMROO_${RMATRIX_TYPE}_R${RPARAM}"
		mkdir -p ${RM_PATH}
		qsub -P star -l h_vmem=2G -l h_rt=10:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRMROO.sh  
		#sh run_buildRMROO.sh $TRG $TYPE
done #pTtresh
done #R
done #SUFFIX
done #centrality
