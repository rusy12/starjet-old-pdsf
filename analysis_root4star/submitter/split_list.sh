#!/bin/bash
lStart=$1
lEnd=$2
lAll=$3
FILE=$4
FILEOUT=$5

lCount="$((lEnd-lStart+1))"
toEnd="$((lAll-lStart+1))"

tailnumber="$toEnd"
tail -n"${tailnumber}" ${FILE} | head -n${lCount} > $FILEOUT
