#!/bin/bash
export TRIGGER="MB" #trigger, use "MB" for AuAu data and "MB" or "HT0","HT1","HT2" for pp data
export DOAUAU=0 #AuAu or pp collisions
export DOQA=1 #do QA of tracks and events
export DOEMBEDDING=0 #perform jet embedding in order to calculate delta-pT
export DOEVENTCUTS=1 #apply event cuts
export RRHO=0.3 #size of jets used to calculate background energy density
export ZVERTEX=30 #maximum value of z vertex position (TPC)
export ZTPCZVPD=4.0 #maximum distance between VPD and TPC z vertex position
export TOFBEMC=0 #require for each track a match in BEMC or TOF in pp collisions to remove pile-up
export MAXRAP=1.0 #max PSEUDOrapidity acceptance for tracks; (NB: maximum PSEUDOrapidity acceptance for jets is |eta|<MAXRAP-R)
export DCA=1.0 #track maximum Distance of Closest Approach

#SUFF="_HFpico_R02Area009" #outputdir suffix
SUFF="_PILEUP-ranking_lt0" #outputdir suffix

TIMESTAMP=$(date +%s)
MAKELISTS=1
i=0
for CENTRAL in 1 #1 #use only one value for pp data (it doesn't matter which one)
do
export CENTRAL
let "i=i+1"
if [ $i -gt 1 ]; then
		MAKELISTS=0
fi

if [ $CENTRAL -eq 1 ]; then
     CENTRALITY="0-10"
	  REFMULTMIN=396
	  REFMULTMAX=100000
	  NJETSREMOVE=2

elif [ $CENTRAL -eq 0 ]; then
      CENTRALITY="60-80" 
		REFMULTMIN=10
		REFMULTMAX=31
		NJETSREMOVE=1
fi 
export REFMULTMIN
export REFMULTMAX

COLSYS="AuAu"

BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/star_run11_filelist.list"
if [ $DOAUAU -eq 0 ]; then #pp data
	BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run12/star_run12_pp_filelist.list"
	COLSYS="pp"
	NJETSREMOVE=1
fi
export NJETSREMOVE

OUTDIR="$STARJETBASEDIR/out/$TRIGGER/"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export MACRODIR=$BASEDIR
SCRIPTNAME="run_analysis.sh"

BASENAME="incl"
if [ $DOEMBEDDING -eq 1 ]; then
	BASENAME="emb"
fi
if [ $DOQA -eq 1 ]; then
	BASENAME="qa"
fi

TYPE="${BASENAME}_${COLSYS}_R${RPARAM}_${CENTRALITY}cent_A${ACUT}_rhoR${RRHO}_nrem${NJETSREMOVE}$SUFF"

NFILES=300 #how many files merge into one batch
if [ $CENTRAL -eq 0 ]; then
	NFILES=300
fi
if [ $DOEMBEDDING -eq 1 ]; then 
	NFILES=40
fi
if [ $DOAUAU -eq 0 ]; then #pp data
	NFILES=50 #how many files merge into one batch
	if [ $DOEMBEDDING -eq 1 ]; then 
		NFILES=8 #how many files merge into one batch
	fi
fi
#MAX=2
LINES=`cat $BIG_FILELIST | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"

N=1
if [ $DOQA -eq 1 ]; then
	N=5 #2 #take every Nth batch
elif [ $DOEMBEDDING -eq 1 ]; then
	N=20 #take every Nth batch
fi
let ADD=$NFILES*$N
for ((FIRST=1; FIRST < $MAX; FIRST += $ADD))
  do
	LAST=$((FIRST + NFILES - 1 ))	
	if [ $LAST -gt $LINES ]; then
	LAST=$LINES
	fi
	LISTNAME="list_${TIMESTAMP}_${FIRST}"
	FILELIST_SMALL="${BASEDIR}/submitter/filelists/${LISTNAME}.list"

	#echo "lines: $FIRST - $LAST - $LINES"
	if [ $MAKELISTS -eq 1 ]; then
		./split_list.sh $FIRST $LAST $LINES $BIG_FILELIST $FILELIST_SMALL
		echo "generating list: $FILELIST_SMALL"
	fi
  	export JOBNAME="${BASENAME}_${DOAUAU}_${DOEMBEDDING}_${DOQA}_R${RPARAM}_${FIRST}"
  	export OUT_PATH="${OUTDIR}/${TYPE}/${FIRST}"
	export IN_FILELIST="$FILELIST_SMALL"

  if [ ! -e $OUT_PATH ]; then
     mkdir -p $OUT_PATH
  fi

	WTIME="24:00:00"
	if [ $DOQA -eq 1 ]; then
		WTIME="5:00:00"
	elif [ $DOEMBEDDING -eq 1 ]; then
		WTIME="24:00:00"
	fi

qsub -P star -m n -l h_vmem=2G -l h_rt=$WTIME -N $JOBNAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
#sh $SCRIPTNAME
done #jobs
done #central

