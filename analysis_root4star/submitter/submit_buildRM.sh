#!/bin/bash
TRG="MB"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export WORKDIR="$BASEDIR/response_matrix"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=0 # correct delta pT for event plane bias
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #path to correction histograms

for CENTRAL in 1 # 0 1
do
export CENTRAL
if [ $CENTRAL -eq 0 ]; then
	SUFF="_peripheral" 
	PTLEADCUTS="4 5 6 7" 
elif [ $CENTRAL -eq 1 ]; then
	SUFF="" 
	PTLEADCUTS="5 6 7"
else
	SUFF="_pp" 
	PTLEADCUTS="0 1 2 3 4 5 6 7" 
fi

export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/${TRG}/embedding$SUFF"

for RPARAM in 0.2 0.3 0.4
do
export RPARAM
for PTLEAD in `echo $PTLEADCUTS`
do
	export PTLEAD
   NAME="buildRM_R${RPARAM}_pTl${PTLEAD}"

mkdir -p ${PATH_TO_DELTA_PT_HISTOGRAMS}/rmatrix
qsub -P star -m n -l h_vmem=2G -l h_rt=01:30:00 -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRM.sh 
done #pTlead
done #R
done #centrality
