#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_dete | dete )" 
    exit 1
}

TRG="MB" #HT, MB
export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "TRIGGER: $TRG"
echo "RMATRIX_TYPE: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

#prior_type=(flat pythiadete pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex)
prior_type=(flat flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)

BASEPATH="$HOME/jet_analysis/STARJet"
WRKDIR="$BASEPATH/out/$TRG"
export SVD=0 # SVD unfolding instead of Bayes
export SMOOTH=0 #smooth unfolded solutions in between iterations
#export NBINS=200
export NBINS=VAR
export NITER=7 #number of iterations
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
	export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
#export PTCUTOFF=0 #from witch pT to start with unfolding
EFFICORR=1 # do efficiency correction
SUFF2="_alpha" #output dir suffix

for CENTRAL in 0 #central|peripheral collisions
do
if [ $CENTRAL -eq 1 ]; then
	SUFF=""
	PTLEADCUTS="5 6 7"
else
	SUFF="_peripheral" 
	PTLEADCUTS="4 5 6"
fi
if [ $RMATRIX_TYPE == "BG_sp" ]; then
	export EFFICORR=0 
else
	export EFFICORR
fi
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

if [ $SVD -eq 0 ]; then
UTYPE="Bayes"
else
UTYPE="SVD"
fi

for SYSSUFF in "_u" "_p5" "_m5" "_normal" #"_AuAu" "_v2" #choice of different results for sys. err. calculation: "_normal" | "_m5" | "_p5" | "_2u1g" | "_AuAu"
do

for RPARAM in 0.2 0.3 0.4
do
export RPARAM
export PRIOR_PATH="$BASEPATH/out/MB/prior"
export DATA_PATH="$WRKDIR/inclusive$SUFF"
export RMATRIX_PATH="$WRKDIR/embedding${SUFF}/rmatrix${SYSSUFF}"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFF}${SYSSUFF}/epsilon"

for BININGCH in 1 4 #choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
do
export BININGCH 
for PRIOR in 2 4 5 6 7 8 9 10 11 12 13 14 15 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:pT^(-6) 7: levy I 8: levy II
do
	export PRIOR
	if [ $SECONDUNFOLD -eq 0 ]; then
		OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUFF2}${SYSSUFF}/"${prior_type[$PRIOR]}
	else
		OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_U2_initer${INPUTITER}/"${prior_type[$PRIOR]}
	fi
   echo "creating directory: $OUT_DIR"
   mkdir -p $OUT_DIR
	export OUT_DIR

	for PTTHRESH in `echo $PTLEADCUTS`
	do
	  export PTTHRESH
		if [ $NBINS == "VAR" ]; then	
	  root -l -b -q unfold_roounfold_uneqbin.C
		else
	  root -b -l -q unfold_roounfold.C
		fi
	  #root -b -l -q unfold_bayes.C
	  #root -l -q -b unfold_bayes_uneqbin.C
	done #pT threshold
done #prior
done #bining
done #R
done #systematic set
done #centrality
