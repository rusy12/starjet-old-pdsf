#include <TSystem>

class StMaker;
class StChain;
class StPicoDstMaker;
class StJetAna;


StChain *chain;
void run_alex(Int_t nEvents = 10000000)
{
//Load all the System libraries
	
   gROOT->LoadMacro("$STAR/StRoot/StMuDSTMaker/COMMON/macros/loadSharedLibraries.C");
	loadSharedLibraries();

	gSystem->Load("$FASTJETDIR/lib/libfastjet.so");
	gSystem->Load("$FASTJETDIR/lib/libfastjettools.so");
/*
	gSystem->Load("$PYTHIA6/libPythia6.so");
	gSystem->Load("$PICOREADDIR/libStPicoDstMaker.so");
	gSystem->Load("$ANALYSISDIR/libSJetRun.so");
	gSystem->Load("$ROOTSYS/lib/libEGPythia6.so");
	gSystem->Load("$UNFOLDINGDIR/libUnfold.so");
	gSystem->Load("$ROOUNFOLD/libRooUnfold.so");
*/

	gSystem->Load("StPicoDstMaker");
   gSystem->Load("StRefMultCorr");
   gSystem->Load("StJetTrackEvent");
   gSystem->Load("StJetAna");

        StJetAna *anaMaker = new StJetAna();
			anaMaker->Init();

		
anaMaker->Make_Alexpico(nEvents);


	
anaMaker->Finish();
	cout << "****************************************** " << endl;
	cout << "Work done... now its time to close up shop!"<< endl;
	cout << "****************************************** " << endl;
	cout << "total number of events  " << nEvents << endl;
	cout << "****************************************** " << endl;
	
}

