#include <TSystem>

class StMaker;
class StChain;
class StPicoDstMaker;
class StJetAna;


StChain *chain;
void run(const Char_t *inputFile="/global/homes/r/rusnak/jet_analysis/run11/test.list", const Char_t *outputFile="/global/homes/r/rusnak/jet_analysis/STARJet/out/test/test.root")
{
//        Int_t nEvents = 10000000;
	Int_t nEvents = 1000;	
//Load all the System libraries
	
   gROOT->LoadMacro("$STAR/StRoot/StMuDSTMaker/COMMON/macros/loadSharedLibraries.C");
	loadSharedLibraries();

	gSystem->Load("$FASTJETDIR/lib/libfastjet.so");
	gSystem->Load("$FASTJETDIR/lib/libfastjettools.so");
/*
	gSystem->Load("$PYTHIA6/libPythia6.so");
	gSystem->Load("$PICOREADDIR/libStPicoDstMaker.so");
	gSystem->Load("$ANALYSISDIR/libSJetRun.so");
	gSystem->Load("$ROOTSYS/lib/libEGPythia6.so");
	gSystem->Load("$UNFOLDINGDIR/libUnfold.so");
	gSystem->Load("$ROOUNFOLD/libRooUnfold.so");
*/

	gSystem->Load("StPicoDstMaker");
   gSystem->Load("StRefMultCorr");
   gSystem->Load("StJetAna");
   gSystem->Load("StMyAnalysisMaker");

	chain = new StChain();

	StPicoDstMaker *picoMaker = new StPicoDstMaker(0,inputFile,"picoDst");


	chain->Init();
	cout<<"chain->Init();"<<endl;

        //StMyAnalysisMaker *anaMaker = new StMyAnalysisMaker("ana",picoMaker,outputFile);
        StJetAna *anaMaker = new StJetAna("ana",picoMaker,outputFile);
anaMaker->Init();
	int total = picoMaker->chain()->GetEntries();
        cout << " Total entries = " << total << endl;
        if(nEvents>total) nEvents = total;
	for (Int_t i=0; i<nEvents; i++){
	  if(i%100==0)
		cout << "Working on eventNumber " << i << endl;
		
	  chain->Clear();
	  int iret = chain->Make(i);
		
	  if (iret) { cout << "Bad return code!" << iret << endl; break;}

	  //total++;
		
anaMaker->Make();


	}
	
anaMaker->Finish();
	cout << "****************************************** " << endl;
	cout << "Work done... now its time to close up shop!"<< endl;
	cout << "****************************************** " << endl;
	chain->Finish();
	cout << "****************************************** " << endl;
	cout << "total number of events  " << nEvents << endl;
	cout << "****************************************** " << endl;
	
	delete chain;
	
	
}

