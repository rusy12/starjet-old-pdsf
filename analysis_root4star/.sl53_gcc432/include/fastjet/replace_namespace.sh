#!/bin/bash
FINDSTRING="FASTJET_OVERRIDE"
REPLACESTRING=""

#FINDSTRING="FASTJET_END_NAMESPACE"
#REPLACESTRING="} \/\/ fastjet namespace"

#FINDSTRING="FASTJET_BEGIN_NAMESPACE"
#REPLACESTRING="namespace fastjet {"

for fl in *.hh; do
     mv $fl $fl.old
     sed "s/${FINDSTRING}/${REPLACESTRING}/g" $fl.old > $fl
     rm -f $fl.old
done
