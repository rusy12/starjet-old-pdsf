/********************************************************************
* .sl53_gcc432/obj/StRoot/StMyAnalysisMaker/StMyAnalysisMaker_Cint.h
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************************/
#ifdef __CINT__
#error .sl53_gcc432/obj/StRoot/StMyAnalysisMaker/StMyAnalysisMaker_Cint.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#define G__PRIVATE_GVALUE
#include "cint/G__ci.h"
extern "C" {
extern void G__cpp_setup_tagtableStMyAnalysisMaker_Cint();
extern void G__cpp_setup_inheritanceStMyAnalysisMaker_Cint();
extern void G__cpp_setup_typetableStMyAnalysisMaker_Cint();
extern void G__cpp_setup_memvarStMyAnalysisMaker_Cint();
extern void G__cpp_setup_globalStMyAnalysisMaker_Cint();
extern void G__cpp_setup_memfuncStMyAnalysisMaker_Cint();
extern void G__cpp_setup_funcStMyAnalysisMaker_Cint();
extern void G__set_cpp_environmentStMyAnalysisMaker_Cint();
}


#include "TObject.h"
#include "TMemberInspector.h"
#include "StMyAnalysisMaker.h"
#include <algorithm>
namespace std { }
using namespace std;

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TClass;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TBuffer;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TMemberInspector;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TObject;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TNamed;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_vectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_reverse_iteratorlEvectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TString;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TDataSet;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_iteratorlEbidirectional_iterator_tagcOTObjectmUcOlongcOconstsPTObjectmUmUcOconstsPTObjectmUaNgR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_maplEstringcOTObjArraymUcOlesslEstringgRcOallocatorlEpairlEconstsPstringcOTObjArraymUgRsPgRsPgR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TVectorTlEfloatgR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TVectorTlEdoublegR;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TH1F;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_StMaker;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_StPicoDst;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_StPicoDstMaker;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_TH2F;
extern G__linked_taginfo G__StMyAnalysisMaker_CintLN_StMyAnalysisMaker;

/* STUB derived class for protected member access */
