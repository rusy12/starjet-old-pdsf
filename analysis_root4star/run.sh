#!/bin/bash

export DOAUAU=0
export DOQA=1
export DOEMBEDDING=0
#export RPARAM=0.4
#export ACUT=0.2
export RRHO=0.3
export NJETSREMOVE=1
export REFMULTMIN=396
export REFMULTMAX=100000
export ZVERTEX=30
export MAXRAP=1.0
export DCA=1.0

export OUT_PATH="/global/homes/r/rusnak/jet_analysis/STARJet/out_test/"
IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/test.list"
#IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/test_alex.list"
if [ $DOAUAU -eq 0 ]; then
	IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run12/test.list"
fi
export IN_FILELIST

root4star -b -q 'run_analysis.C(50000)'
#root4star -b -q 'run_alex.C(1000)'
