#!/bin/bash
export JETTYPE="pythia"
export REVERSE=0 #0: BGxDete 1: DetexBG


for CENTRAL in 0 1 #central or peripheral collisions
do

if [ $CENTRAL -eq 1 ]; then
CSUFFIX=""
PTLEADCUTS="5 6 7"
else
CSUFFIX="_peripheral"
PTLEADCUTS="4 6 5"
fi
export CSUFFIX

for TSUFFIX in  "_u" "_normal" "_m5" "_p5" #"_AuAu" "_v2" 
do
export TSUFFIX
for RPARAM in 0.2 0.3 0.4 #0.5
do
export RPARAM
for PTTHRESH in `echo $PTLEADCUTS` 
   do
   export PTTHRESH
   root -l -b multiply_matrix.C -q
done
done
done
done
