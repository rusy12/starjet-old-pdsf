#!/bin/bash
TRG="MB"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=0 # correct delta pT for event plane bias
export CENTRAL=1

if [ $CENTRAL -eq 1 ]; then
SUFF=""
PTLEADCUTS=5 #"5 6 7"
else
SUFF="_peripheral"
PTLEADCUTS="4 5 6"
fi

export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #path to correction histograms
export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/$TRG/embedding$SUFF"

for RPARAM in 0.2 #0.3 0.4
do
	export RPARAM
   #export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/$TRG/embedding_R${RPARAM}$SUFF"
for PTLEAD in `echo $PTLEADCUTS`
do
   export PTLEAD
	root4star -l buildResponseM.C -q -b
done
done
