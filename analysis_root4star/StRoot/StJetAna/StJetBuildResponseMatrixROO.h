#ifndef __StJetBuildResponseMatrixROO__hh
#define __StJetBuildResponseMatrixROO__hh

#include "TString.h"

class TF1;
class TH1D;
class TH2D;
class TFile;

class StJetBuildResponseMatrixROO
{
 public:
  StJetBuildResponseMatrixROO(Float_t R, Float_t pTthresh, Int_t priorNo, TString type, Int_t nEvents=1E9, TH1D* hpriorhisto=NULL);
  ~StJetBuildResponseMatrixROO();
  
  void BuildDeltaPtResponseMatrix();
  
 private:
  Double_t SmearWithDeltaPt(Double_t pT);

 protected:
  TFile *fout;
  TFile *finput;
  TFile *fepsilon;

  TH2D *hRMin;
  TH1D *hdpT[500];
  TH2D *hResponse;
  TH1D *hMCtrue;
  TH1D *hMCreco;
  TH1D *hprior0;
  TF1 *fprior;
  TF1 *fpythia;

  /*
  TH1D *hepsilon;
  TH1D *hMCreco_eff;
  TH2D *hResponse_eff;
  TH2D *hResponse_15;
  TH2D *hResponse_15_eff;
  TH1D *hMCtrue_15;
  TH1D *hMCreco_15;
  TH1D *hMCreco_15_eff;
*/

  Int_t nbins;
  Int_t nevts;
  Int_t prior;
  
  Double_t pTmin;
  Double_t pTmax;
  Double_t pTcutoff;
  Double_t pTlead;

  TString mtype;
  TString str;
  TString path;
  TString prior_path;
  TString pyEmb_path;

  ClassDef(StJetBuildResponseMatrixROO, 1)
};

#endif
