#ifndef StJetAna_h
#define StJetAna_h

#include <bitset>
#include "StRoot/StPicoDstMaker/StPicoDst.h"
#include "StRoot/StPicoDstMaker/StPicoTrack.h"
#include "StRoot/StPicoDstMaker/StPicoEvent.h"
#include "StRoot/StPicoDstMaker/StPicoDstMaker.h"
#include "StRoot/StRefMultCorr/StRefMultCorr.h"
#include "StRoot/StRefMultCorr/CentralityMaker.h"
// #include "StRoot/StJetTrackEvent/StJetTrackEvent.h"
/*
#include "StRoot/StJetPico/TStarJetPicoReader.h"
#include "StRoot/StJetPico/TStarJetPicoEvent.h"
#include "StRoot/StJetPico/TStarJetPicoEventHeader.h"
#include "StRoot/StJetPico/TStarJetPicoEventCuts.h"
#include "StRoot/StJetPico/TStarJetPicoPrimaryTrack.h"
#include "StRoot/StJetPico/TStarJetPicoTrackCuts.h"
#include "StRoot/StJetPico/TStarJetPicoUtils.h"
*/
#include "StThreeVectorF.hh"
#include "StMaker.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TChain.h"
#include "TSystem.h"
#include "TRandom.h"
#include "TLorentzVector.h"

//FastJet 3
#include "fastjet/config.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/Selector.hh"
#include "fastjet/tools/Subtractor.hh"

class StPicoDst;
class StPicoDstMaker;

using namespace std;
using namespace fastjet;

#include "fastjet/tools/JetMedianBackgroundEstimator.hh"


class StJetAna : public StMaker {
  public:
    StJetAna(const char *name, StPicoDstMaker *picoMaker/*, const char *outName*/);
    StJetAna();
    virtual ~StJetAna();
    
    virtual Int_t Init();
    virtual Int_t Make_HFpico();
    //virtual Int_t Make_Alexpico(int nev);
    //virtual Int_t Make_Janpico(int nev);
    virtual void  Clear(Option_t *opt="");
    virtual Int_t Finish();
    
    void    DeclareHistograms(bool embedding, bool qa);
    void    WriteHistograms(bool embedding, bool qa);
    void    FillJetHistos(float pT_jet, float pTcorr_jet, float area_jet, float pTlead, float eta_jet, float phi_jet, int r, int nparticles, float weight=1.0);
	 void 	FillJetHistosEmb(float pTcorr_jet, float area_jet, float pTlead, float pT_emb,int r, float weight=1.0/*,float eta_jet, float phi_jet, float eta_emb,float phi_emb*/);
	 void		FillEventHistos(float zvertex, float zVPD, float ranking, int refmult, int day, int id, float bbcrate, int ntracks, float weight=1.0);
	 void 	FillQAHistos(float track_pT, float track_eta, float track_phi, float dca, float zVertex, float zVPD, bool TTOF_match, float weight=1.0);
	 void 	JetReco(vector<PseudoJet> input_vector_data, float R, short Remove_N_hardest, float weight=1.0, short embedding=-1);
	 vector<PseudoJet> EmbedJet(short jetType, float pT_emb, float R, vector<PseudoJet> container/*, float* eta_emb, float* phi_emb*/);
	 bool 	FindEmbeddedJet(vector<PseudoJet> constituents,float pT_emb);
	 bool 	BadRun(int id);
	 bool 	BadDay(int day, short run=12);


  private:
   StPicoDstMaker *mPicoDstMaker;
   StPicoDst      *mPicoDst;
    
   TString    mOutName;
	TString sInFileList;
	TString sInputdir;
	TString sTrigger;
	short kDoEmbedding;
	bool kDoQA;
	bool kDoAuAu;
	bool kDoEventCuts;
	bool kreqTOF;
   float fR;
	float fAcut;
   float fMaxRap;
   float fDCACut;
   float fZVertexCut;
   float fDeltaZ;
   float fR_bg;
	float fpTminCut;	
	float fpTmaxCut;	
   float fFitOverMaxPointsCut;
	float fGhost_maxrap;
	int npTlead;
	int nRefmultcutMin;
   int nRefmultcutMax;
   int nJetsRemove;
	int nSigmaCut;
	int nFitPointsCut;
	int nEmb;
	int nR;
	
	//event histograms
	TH1I *hrefmult;
	TH1I *hevents;
	TH1I *hevents_nw;
	TH1I *heventid;
	TH1F *hzvertex;
	TH1D *hdeltaz;
	TH2D* hz_refmult;
	TH2D* hday_refmult;
	TH2D* hday_z;
	TH2D* hbbcrate_ntr;
	TH1D* hmeanrefmult;
	TH1D* hmeanz;
	TH1D* hranking;

	//jet histograms
	TH2D *hpT_pTlead[4];
	TH2D *heta_phi[4];
	TH1D *hjetarea[4];
	TH1D *hjetarea_cut[4];
	TH2D *hjetpTarea[4];
	TH2D *hjetpTcorrArea[4];
	TH2D *delta_pt_BG_sp[15][4];
	TH1D *hrho[4];
	TH1D *hpT_pTl[15][4];	
	TH2D *hjetstructure[4];
	TH2D *hnparticlesinjets[4];

	//track histograms
	TH1D *hpT_tr;
	TH1D* hTOFmatch;
	TH2D *heta_phi_tr;
	TH2D *heta_phi_tr1gev;
	TH2D *hdca_z_tr;
	TH2D *hdca_pT;
	TH1D *hdca_tr;

    ClassDef(StJetAna, 1)
};

#endif

