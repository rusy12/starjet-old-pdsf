This directory contains the main code for the STAR jet analysis.
Here is the basic description of the subdirectories:

"analysis"
Contains old analysis code, is not used anymore.

"analysis_root4star"
Contains the latest analysis code. Currently it can be used only with Heavy Flavor picodsts.
My old picodsts and Alex's picodsts can be analyzed with the old code in "analysis" directory.

"modules"
Contains modules which can be loaded with "module" command and which setup all the necessary system variables and paths to the software
(e.g. ROOUnfold, Root, Fastjet, etc.).

"Unfolding"
Originaly contained the unfolding code, this was however replaced with ROOUnfold software and now is used only for producing the response matrices.

