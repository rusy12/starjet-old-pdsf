#!/bin/bash

export TRIGGER="MB"
export DOAUAU=1
export DOQA=0
   export SAVETREE=0 # when doing QA, save also TTree with event variables
export DOEMBEDDING=1
	export PYTHIAEMB=1 #embed pythia jets instead of single particle
	export PARTON="u" #when embedding pythia jets, do we want to fragment u quark ("u") or gluon ("g")
export DOEVENTCUTS=1
#export RPARAM=0.4
#export ACUT=0.2
export GLOBAL=0 #use global or primary tracks
export RRHO=0.3
export NJETSREMOVE=1
export REFMULTMIN=10
export REFMULTMAX=10000
export ZVERTEX=30
export ZTPCZVPD=4.0
export MAXRAP=1.0
export DCA=1.0
export CHI2=100.0 #TPC track fit minimal chi2
export NFITNMAX=0.55 # #TPC fit points / # possible fit points
export NFIT=15 # #TPC fit points 
export TOFBEMC=1 #require hit in TOF or BEMC in pp collisions for pile-up removal
export BBCMIN=0
export BBCMAX=1000000000
NEVENTS=1000

ALEXPICO=0

export OUT_PATH="/global/homes/r/rusnak/jet_analysis/STARJet/out_test/"
if [ $ALEXPICO -eq 0 ]; then
	IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/test.list"
else
	IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/test_alex.list"
fi
if [ $DOAUAU -eq 0 ]; then
	IN_FILELIST="/global/homes/r/rusnak/jet_analysis/run12/test.list"
fi
export IN_FILELIST

root -b -q "run_analysis.C($NEVENTS,$ALEXPICO)"
#root4star -b -q 'run_alex.C(1000)'
