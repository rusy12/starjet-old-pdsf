#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_pyt | dete | BGD)"
    exit 1
}

export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


TRG="MB"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export WORKDIR="$BASEDIR/response_matrix"
SCRIPTNAME=run_buildRMROO.csh

for CENTRAL in 1
do
if [ $CENTRAL -eq 1 ]; then
CENTSUFF="_central"
PTLEADCUTS="5 6 7"
elif [ $CENTRAL -eq 0 ]; then
CENTSUFF="_peripheral"
PTLEADCUTS="2 3 4 5 6 7"
else #p+p
CENTSUFF="_pp"
PTLEADCUTS="3 4" #"0 1 2 3 4 5 6 7"
fi
PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15" #0: -, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4.5) 5: pT^(-5) 6:pT^(-5.5) 7: levy 8: levy II
export PRIORS

for SYSSUF in "_trcuts2" #"_pythia" "_main" "_RRho02" "_RRho04" "_nrem-1" # "_global" 
do

if [ $SYSSUF == "_main" ]; then
   TSUFFIX_ARR="_normal _pp _g _u _m5 _p5 _v2"
else
   TSUFFIX_ARR="_normal"
fi


for TSUFF in `echo $TSUFFIX_ARR`
do
for RPARAM in 0.2 0.3 0.4 0.5
do
export RPARAM
export RM_PATH="$STARJETBASEDIR/out/${TRG}/embedding${CENTSUFF}${SYSSUF}/rmatrix${TSUFF}"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${TSUFF}"
export PRIOR_PATH="$STARJETBASEDIR/out/prior"

for PTTHRESH in `echo $PTLEADCUTS`
do
      export PTTHRESH
      JOBNAME="buildRMROO_${CENTSUFF}_${RMATRIX_TYPE}_R${RPARAM}_pTlead${PTTHRESH}${SYSSUF}_${TSUFF}"
		mkdir -p ${RM_PATH}
		#qsub -P star -l h_vmem=2G -l h_rt=14:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRMROO.csh  
		WTIME="14:00:00"
		sbatch -p shared-chos -t $WTIME --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
		#sh run_buildRMROO.sh $TRG $TYPE
done #pTtresh
done #R
done #SUFFIX
done #centrality
done
