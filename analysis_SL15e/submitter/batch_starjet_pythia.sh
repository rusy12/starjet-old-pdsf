#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet


cd $MacroDir
echo root -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\)
root -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\) 2>&1
