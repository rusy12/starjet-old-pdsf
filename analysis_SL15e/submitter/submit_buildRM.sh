#!/bin/bash
TRG="MB"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export WORKDIR="$BASEDIR/response_matrix"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=0 # correct delta pT for event plane bias
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #path to correction histograms
SCRIPTNAME=run_buildRM.csh

for SYSSUF in "trcuts2" #"main" "RRho02" "RRho04" "nrem-1" "pythia" #"global" #
do
for CENTRAL in 1 
do
export CENTRAL
if [ $CENTRAL -eq 0 ]; then
	SUFF="_peripheral" 
	PTLEADCUTS="2 3 4 5 6 7" 
elif [ $CENTRAL -eq 1 ]; then
	SUFF="_central" 
	PTLEADCUTS="5 6 7"
else
	SUFF="_pp" 
	PTLEADCUTS="0 1 2 3 4 5 6 7" 
fi

export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/${TRG}/embedding${SUFF}_${SYSSUF}"

for RPARAM in 0.2 0.3 0.4 0.5
do
export RPARAM
for PTLEAD in `echo $PTLEADCUTS`
do
	export PTLEAD
   JOBNAME="buildRM_R${RPARAM}_pTl${PTLEAD}"

mkdir -p ${PATH_TO_DELTA_PT_HISTOGRAMS}/rmatrix_normal
#echo "creating dir ${PATH_TO_DELTA_PT_HISTOGRAMS}/rmatrix_normal"
if [ $V2CORR -eq 1 ]; then
	mkdir -p ${PATH_TO_DELTA_PT_HISTOGRAMS}/rmatrix_v2
fi
WTIME="1:30:00"
sbatch -p shared-chos -t $WTIME --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
done #pTlead
done #R
done #centrality
done
