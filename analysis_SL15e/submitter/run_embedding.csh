#!/bin/csh
#setenv PARTICLE "K"
#setenv FILELIST "filelists/K_file.list"
#setenv CENTRAL 0

cd $MacroDir

starver SL12d_embed
pwd
#ls -latr
root4star -b <<EOF
.L StMiniMcTree.C
StMiniMcTree uu
uu.Loop()
.q
EOF
