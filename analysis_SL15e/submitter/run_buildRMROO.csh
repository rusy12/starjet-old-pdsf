#!/bin/csh
starver SL15e
source ../set_paths.csh
cd $WORKDIR

foreach PRIOR ( `echo $PRIORS` )

	setenv PRIOR $PRIOR
	root4star -l buildResponseROO.C -q -b
end
