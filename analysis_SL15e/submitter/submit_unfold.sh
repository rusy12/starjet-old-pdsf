#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE"
    exit 1
}

export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BGD - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


BASEDIR="$ANALYSISDIR"
LOGDIR="$BASEDIR/submitter/log"
export MacroDIR="$BASEDIR/unfolding"
SCRIPTNAME="run_unfold_allpTlead_allpriors.csh"
#SCRIPTNAME="run_unfold_allpriors.csh"
	#SCRIPTNAME="run_unfold_allpTlead.sh"
export SMOOTH=0 #smooth unfolded spectrum between iterations, for Bayes only
#export NBINS=100
export NBINS=VAR
export NITER=7 #number of k-terms
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
   export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
export EFFICORR=1 # do efficiency correction
#export SUF="_Xcheck-newRooUnf" #output dir suffix
export SUF="_omicron" #output dir suffix

export PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15"

for SVD in 0 1 # 1:SVD unfolding | 0:Bayesian unfolding
do
export SVD 
if [ $SVD -eq 1 ]; then
	UNFTYPE="SVD"
else
	UNFTYPE="Bayes"
fi


for CENTRAL in 1 #0: peripheral Au+Au | 1: central Au+Au | 2: p+p
do
export CENTRAL
TRG="MB" #HT, MB
USE2DHISTO=1
if [ $CENTRAL -eq 1 ]; then
	SUFFIX="_central"
	PTLEADCUTS="5 6 7"
elif [ $CENTRAL -eq 0 ]; then
	SUFFIX="_peripheral"
	PTLEADCUTS="2 3 4 5 6 7"
else #p+p
	SUFFIX="_pp"
	PTLEADCUTS="3 4 5 6"
	USE2DHISTO=0 #for combined MB+HT spectrum we have only 1D histograms
	TRG="MBHT" #HT, MB
fi
export USE2DHISTO
export PTLEADCUTS
WRKDIR="$BASEDIR/../out/$TRG"

if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

for SYSSUF in "_trcuts2" #"_RRho02" "_RRho04" "_nrem-1" # "_pythia" "_main"# "_global" 
do
	if [ $SYSSUF == "_global" ]; then
		SYSSUF2="_global"
	else
		SYSSUF2=""
	fi

	if [ $SYSSUF == "_main" ]; then
   	TSUFFIX_ARR="_normal _pp _g _u _m5 _p5 _v2"
	else
   	TSUFFIX_ARR="_normal"
	fi


for TSUFF in `echo $TSUFFIX_ARR`
do
export TSUFF

for RPARAM in 0.2 0.3 0.4 #0.5
do
export RPARAM
export DATA_PATH="$WRKDIR/inclusive${SUFFIX}${SYSSUF}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding${SUFFIX}${SYSSUF}/rmatrix${TSUFF}"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUF2}${TSUFF}/epsilon"

#choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
for BININGCH in 0 1 4 #0 1 #2 3 4 #1 4 
do
export BININGCH 

	#for PTTHRESH in `echo $PTLEADCUTS`
   #do
     #export PTTHRESH
		#for PRIOR in `echo $PRIORS`
		#do
   	#export PRIOR
      JOBNAME="unf${UNFTYPE}_R${RPARAM}_pTl${PTTHRESH}_${PRIOR}_bin${BININGCH}$SYSSUF$TSUFF"
		WTIME="8:00:00"
		#qsub -P star -m n -l h_vmem=1G -l h_rt=8:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
		sbatch -p shared-chos -t $WTIME --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
		#cd $MacroDIR
		#root -b -l -q unfold_roounfold.C
	#done #prior
	#done #pTleading
done #bining
done #R
done #suffix
done #centrality
done #unfolding
done
