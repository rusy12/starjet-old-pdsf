#!/bin/bash
export TRIGGER="MB" #trigger, use "MB" for AuAu data and "MB" or "HT0","HT1","HT2" for pp data
export DOAUAU=1 #AuAu or pp collisions
export DOQA=0 #do QA of tracks and events
	export SAVETREE=0 # when doing QA, save also TTree with event variables (refmult, z,...) - WARNING: output file can be large!
export DOEMBEDDING=1 #perform jet embedding in order to calculate delta-pT
	export PYTHIAEMB=0 #embed pythia jets instead of single particle
	export PARTON="g" #when embedding pythia jets, do we want to fragment u quark ("u") or gluon ("g")
export DOEVENTCUTS=1 #apply event cuts
export GLOBAL=0 #use global or primary tracks
export RRHO=0.3 #0.3 | size of jets used to calculate background energy density
export ZVERTEX=30 #30 |  maximum value of z vertex position (TPC)
export ZTPCZVPD=4.0 #maximum distance between VPD and TPC z vertex position (used only for pp)
export TOFBEMC=0 #require for each track a match in BEMC or TOF in pp collisions to remove pile-up
export BBCMIN=0
export BBCMAX=1000000000
export MAXRAP=1.0 #1.0 | max PSEUDOrapidity acceptance for tracks; (NB: maximum PSEUDOrapidity acceptance for jets is |eta|<MAXRAP-R)
export DCA=1.0 #1.0 | track maximum Distance of Closest Approach
export CHI2=100.0 #100 | track fit minimal chi2 value
export NFIT=20 # 15 | minimal number of TPC fit points 
export NFITNMAX=0.52 #0.52 |  number of TPC fit points / # possible points
export ALEXPICO=0 #use Alex's picoDst instead of Heavy Flavor group picoDst

SUFF="_check" #output directory suffix
#SUFF="_cutsSameAsEmb_OKnevents_R05_PythiaEmb4g" #output directory suffix

TIMESTAMP=$(date +%s)
MAKELISTS=1
i=0
for CENTRAL in 1 #0 # 1 #use only one value for pp data (it doesn't matter which one)
do
export CENTRAL
let "i=i+1"
if [ $i -gt 1 ]; then
		MAKELISTS=0
fi

if [ $CENTRAL -eq 1 ]; then
     CENTRALITY="0-10"
	  REFMULTMIN=396 #drop events with refmult  less or equal REFMULTMIN
	  #REFMULTMIN=1
	  REFMULTMAX=100000 #drop events with refmult greater than REFMULTMAX
	  NJETSREMOVE=2 #2

elif [ $CENTRAL -eq 0 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="60-80" 
		REFMULTMIN=10 #80%: 10; 55%: 31
		REFMULTMAX=43 # 60-80%: 43 
		NJETSREMOVE=1 #1
elif [ $CENTRAL -eq -1 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="60-65" 
		REFMULTMIN=31 #80%: 10; 55%: 31
		REFMULTMAX=43 # 60-80%: 43 
		NJETSREMOVE=1 #1
elif [ $CENTRAL -eq -2 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="50-60" 
		REFMULTMIN=43 #80%: 10; 55%: 31
		REFMULTMAX=76 # 50-80%: 76
		NJETSREMOVE=1 #1
fi 
export REFMULTMIN
export REFMULTMAX

COLSYS="AuAu"

BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run11/star_run11_filelist.list"
PICOTYPE="HFpico"
if [ $DOAUAU -eq 0 ]; then #pp data
	BIG_FILELIST="/global/homes/r/rusnak/jet_analysis/run12/star_run12_pp_filelist.list"
	COLSYS="pp"
	NJETSREMOVE=1
fi
if [ $ALEXPICO -eq 1 ]; then
	BIG_FILELIST="$HOME/jet_analysis/run11/alex_${CENTRALITY}.list"
	PICOTYPE="ALEXpico"
fi
export NJETSREMOVE

OUTDIR="$STARJETBASEDIR/out/$TRIGGER/"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
export MACRODIR=$BASEDIR
SCRIPTNAME="run_analysis.csh"

BASENAME="incl"
if [ $DOEMBEDDING -eq 1 ]; then
	BASENAME="emb"
fi
if [ $DOQA -eq 1 ]; then
	BASENAME="qa"
fi

TYPE="${BASENAME}_${COLSYS}_${CENTRALITY}cent_${PICOTYPE}_SL15_evtcts${DOEVENTCUTS}_A${ACUT}_rhoR${RRHO}_nrem${NJETSREMOVE}_nFitnMax${NFITNMAX}_NFIT${NFIT}_dca${DCA}_chi2-${CHI2}_glob${GLOBAL}$SUFF"


NFILES=300 #how many files merge into one batch
if [ $CENTRAL -eq 1 ]; then
	NFILES=150
fi
if [ $DOEMBEDDING -eq 1 ]; then 
	NFILES=30
fi
if [ $DOAUAU -eq 0 ]; then #pp data
	NFILES=50 #how many files merge into one batch
	if [ $DOEMBEDDING -eq 1 ]; then 
		NFILES=8 #how many files merge into one batch
	fi
fi
if [ $ALEXPICO -eq 1 ]; then
	NFILES=1
fi

if [ $DOQA -eq 1 ]; then
	NFILES=$((${NFILES}*2))
fi

LINES=`cat $BIG_FILELIST | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
#MAX=2
echo "MAX set to: $MAX"

N=1 #take every Nth batch
if [ $DOQA -eq 1 ]; then
	N=5 #5 #take every Nth batch
elif [ $DOEMBEDDING -eq 1 ]; then
	N=20 #20 take every Nth batch
fi
let ADD=$NFILES*$N
#for FIRST in 51001 62401 72901
for ((FIRST=1; FIRST < $MAX; FIRST += $ADD))
  do
	LAST=$((FIRST + NFILES - 1 ))	
	if [ $LAST -gt $LINES ]; then
	LAST=$LINES
	fi
	LISTNAME="list_${TIMESTAMP}_${FIRST}"
	FILELIST_SMALL="${BASEDIR}/submitter/filelists/${LISTNAME}.list"

	#echo "lines: $FIRST - $LAST - $LINES"
	if [ $MAKELISTS -eq 1 ]; then
		./split_list.sh $FIRST $LAST $LINES $BIG_FILELIST $FILELIST_SMALL
		echo "generating list: $FILELIST_SMALL"
	fi
  	export JOBNAME="${BASENAME}_${DOAUAU}_${DOEMBEDDING}_${DOQA}_R${RPARAM}_${FIRST}_trcuts2"
  	export OUT_PATH="${OUTDIR}/${TYPE}/${FIRST}"
	export IN_FILELIST="$FILELIST_SMALL"

  if [ ! -e $OUT_PATH ]; then
     mkdir -p $OUT_PATH
  fi

	WTIME="18:00:00"
	if [ $DOQA -eq 1 ]; then
		WTIME="18:00:00"
	elif [ $DOEMBEDDING -eq 1 ]; then
		WTIME="24:00:00"
	fi

#qsub -P star -m n -l h_vmem=2G -l h_rt=$WTIME -N $JOBNAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
sbatch -p shared-chos -t $WTIME --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
#sbatch -p shared-chos -t $WTIME --qos=regular --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
#sh $SCRIPTNAME
done #jobs
done #central

