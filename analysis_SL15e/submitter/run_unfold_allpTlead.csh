#!/bin/csh
starver SL15e
source ../set_paths.csh

set prior_type=(flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)

cd $MacroDIR

if ($SVD == 1) then
	set UNFTYPE="SVD"
else
	set UNFTYPE="Bayes"
endif

foreach PTTHRESH ( `echo $PTLEADCUTS` )
   setenv PTTHRESH
#for PRIOR in `echo $PRIORS`
#do
   set OUT_DIR="$DATA_PATH/Unfolded_R${RPARAM}_${UNFTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUF}${SYSSUFF}/${prior_type[$PRIOR]}"
   #echo "creating directory: $OUT_DIR"
   #rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
	setenv OUT_DIR
   #export PRIOR

  root4star -b -l -q unfold_roounfold_uneqbin.C
end
