#!/bin/bash

export MacroDir="$ANALYSISDIR/pythia"
LOGDIR="$ANALYSISDIR/submitter/log"
SCRIPTNAME="run_starjet_pythia.csh"

PTHARD=(3 4 5 7 9 11 15 20 25 30 35 40 50 60 -1)
#hard=60pThard-1; echo "{double a=0" `grep "Cross section" $hard/??/pythia_*hard*_set??.o* | awk -F":" '{print "+"$3}'` ";cout<<\"fqwang \"<<a/10<<endl;}" > aa.C; root -b -q aa.C
DATADIR=$HOME/jet_analysis/PYTHIA_Fuqiang/Pythia64xx/data
XSEC=(1.295 3.147e-1 1.363e-1 2.294e-2 5.510e-3 2.223e-3 3.423e-4 4.700e-5 8.435e-6 1.768e-6 4.047e-7 1.199e-7 6.643e-9 2.919e-10)
#DATADIR=$HOME/jet_analysis/data/Pythia6422
#XSEC=(1.29717 0.314636 0.136377 0.0229622 0.00550543 0.00222398 0.000342505 4.69687e-05 8.43953e-06 1.76916e-06 4.04159e-07 1.19695e-07 6.6343e-09 2.91385e-10)

export CHARGED=1 #0,1,2(parton),6(parton)#use 6 for parton
export MAXRAP=1
export RPARAM=0.3


if [ $RPARAM == "0.2" ]; then
    #ACUT=0.09
    ACUT=0.07
elif [ $RPARAM == "0.3" ]; then
    ACUT=0.2
elif [ $RPARAM == "0.4" ]; then
    ACUT=0.4
    #ACUT=0.35
elif [ $RPARAM == "0.5" ]; then
    ACUT=0.65
elif [ $RPARAM == "0.6" ]; then
    ACUT=0.8
fi
export ACUT
echo "R: $RPARAM"
echo "Area cut set to: $ACUT"

export NEVENTS=0

#################################################################################
#cd $MacroDir

for PTBIN in `seq 0 13` #13
do
    PTMIN=${PTHARD[$PTBIN]}
    PTMAX=${PTHARD[$PTBIN+1]}
    export XSECTION=${XSEC[$PTBIN]}

    for ((SET=0; SET < 10; SET += 1)) #10
    do
	if [ $SET -lt 10 ]; then
	    SETSET=0$SET
	else
	    SETSET=$SET
	fi

	export INPUTDIR=$DATADIR/${PTMIN}pThard${PTMAX}/$SETSET
	#export OUTPUTDIR=$INPUTDIR
	export OUTPUTDIR="$HOME/jet_analysis/STARJet/analysis/out/PYTHIA6_Fuqiang/${PTMIN}pThard${PTMAX}/$SETSET"

	if [ ! -e $OUTPUTDIR ]; then
	    mkdir -p $OUTPUTDIR
	fi

	#if [ -e $OUTPUTDIR/starjet_pythia_R0.4_charged7.root ]; then
	    #mkdir $OUTPUTDIR/save
	    #mv $OUTPUTDIR/starjet_pythia_*_charged7.* $OUTPUTDIR/save
	    #mv $OUTPUTDIR/save/starjet_pythia_*_charged0.* $OUTPUTDIR
	    #mv $OUTPUTDIR/save/starjet_pythia_*_charged1.* $OUTPUTDIR
	#fi
	#mkdir $OUTPUTDIR/R0.4Acut0.4
	#mv $OUTPUTDIR/R0.4Acut0.4/*_R0.4_charged6.o4* $OUTPUTDIR/
	#mv $OUTPUTDIR/R0.4Acut0.4/*_R0.4_charged6.e4* $OUTPUTDIR/
	#mv $OUTPUTDIR/R0.4Acut0.4/*_R0.4_charged6.root $OUTPUTDIR/

	#if [ ! -e $OUTPUTDIR/starjet_pythia_R0.6_charged6.root ]; then
	JOBNAME="starjet_pythia_${PTMIN}pThard${PTMAX}_set${SETSET}_R${RPARAM}_charged${CHARGED}"
	WTIME="16:00:00"
	#qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_starjet_pythia.csh
	sbatch -p shared-chos -t $WTIME --mem=2000 -J $JOBNAME -o "$LOGDIR/${JOBNAME}.log" -e "$LOGDIR/${JOBNAME}.err" $SCRIPTNAME
	#fi
    done
done
