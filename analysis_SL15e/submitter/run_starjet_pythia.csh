#!/bin/csh

starver SL15e
source ../set_paths.csh

cd $MacroDir
echo root4star -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\)
root4star -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\) #2>&1
