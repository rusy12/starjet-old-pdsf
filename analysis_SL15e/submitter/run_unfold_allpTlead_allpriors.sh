#!/bin/bash
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet_root4star

prior_type=(measured flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)

cd $MacroDIR

if [ $SVD -eq 1 ]; then
	UNFTYPE="SVD"
else
	UNFTYPE="Bayes"
fi

for PTTHRESH in `echo $PTLEADCUTS`
do
   export PTTHRESH
for PRIOR in `echo $PRIORS`
do
   export PRIOR
	#OUT_DIR="../out_test/unfolding/system$CENTRAL/Unfolded_R${RPARAM}_${UNFTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUF}${SYSSUFF}/"${prior_type[$PRIOR]}
   OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_${UNFTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUF}${SYSSUFF}/"${prior_type[$PRIOR]}
   #echo "creating directory: $OUT_DIR"
   #rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
	export OUT_DIR

#if [ $NBINS == "VAR" ]; then
  root4star -b -l -q unfold_roounfold_uneqbin.C
#else
#  root4star -b -l -q unfold_roounfold.C
#fi

done
done
