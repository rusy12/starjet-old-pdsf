#!/bin/bash

BASEDIR="$ANALYSISDIR"
LOGDIR="$BASEDIR/submitter/log"
export MacroDir="$ANALYSISDIR/embedding"
SCRIPTNAME="run_embedding.csh"
NAME="embedding"
export GLOBAL=1 #primary or global tracks

for PARTICLE in "P" "K" #"jet" #
do
export PARTICLE 
export FILELIST="filelists/${PARTICLE}_file.list" 

if [ $PARTICLE == "jet" ]; then
	export CENTRAL=2
	qsub -P star -m n -l h_vmem=1G -l h_rt=1:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
else
	for CENTRAL in 1 0 
	do
		export CENTRAL
		qsub -P star -m n -l h_vmem=1G -l h_rt=1:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
	done
fi
done
