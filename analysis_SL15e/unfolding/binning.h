//set of binning arrays
	const int nbinsarr0=27; //number of bins created from the array bellow 
	float binarr0[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,1,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,40,60,100};

	const int nbinsarr1a=26;
	float binarr1a[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,1,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,40,60};
	const int nbinsarr1b=17;
	float binarr1b[]={0,1,2,3,4,5,6,7,8,10,12,14,16,20,25,30,40,60};


	const int nbinsarr2a=23;
	float binarr2a[]={-40,-20,-10,-5,-3,-1,0,1,2,3,4,5,6,7,8,9,10,12,15,20,25,30,40,50};
	const int nbinsarr2b=11;
	float binarr2b[]={0,2,4,5,6,7,10,15,20,25,30,50};

	const int nbinsarr3a=14;
	float binarr3a[]={-25,-20,-15,-10,-5,0,5,10,15,20,25,30,35,40,50};
	const int nbinsarr3b=7;
	float binarr3b[]={0,5,10,15,20,30,40,50};

   const int nbinsarr4a=15;
	float binarr4a[]={-20,-10,-5,0,3,4,5,6,7,10,15,20,25,30,35,40};
	const int nbinsarr4b=10;
	float binarr4b[]={0,3,4,5,6,7,10,15,20,30,40};

/*
	const int nbinsarr2a=33;
	float binarr2a[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,35,40,50,60,70,80,100};
	const int nbinsarr2b=12;
	float binarr2b[]={0,1,2,3,5,7,10,13,18,23,28,36,100};

	const int nbinsarr3a=21;
	float binarr3a[]={-20,-15,-10,-5,-3,-1,0,1,2,3,5,7,9,11,14,18,24,30,40,50,70,100};
	const int nbinsarr3b=13;
	float binarr3b[]={0,1,2,3,5,7,9,11,14,18,24,30,38,100};
   
	const int nbinsarr4a=21;
	float binarr4a[]={-20,-15,-10,-5,-3,-1,0,1,2,3,5,7,9,11,14,18,24,30,40,50,70,100};
	const int nbinsarr4b=13;
	float binarr4b[]={0,1,2,3,5,7,9,11,14,17,22,27,37,100};
 */


