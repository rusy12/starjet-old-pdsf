#!/bin/bash

#MacroDir=$HOME/jet_analysis/STARJet_Pythia

PTHARD=11pThard15
XSECTION=2.21e-3
#export INPUTDIR=$HOME/jet_analysis/data/Pythia6426/$PTHARD/00
#export OUTPUTDIR=$HOME/jet_analysis/data/Pythia6426/$PTHARD/00
export INPUTDIR="$HOME/jet_analysis/PYTHIA_Fuqiang/Pythia64xx/data/$PTHARD/00"
export OUTPUTDIR="$HOME/jet_analysis/PYTHIA_Fuqiang/Pythia64xx/data/$PTHARD/00"

export CHARGED=1 #0,1,2(parton)
export MAXRAP=1
export RPARAM=0.6
export ACUT=0.6 #jet area cut | 0.09 | 0.2 | 0.4

#NEVENTS=100
NEVENTS=10

#mkdir -p $OUTPUTDIR

#cd $MacroDir
echo root -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\)
root4star -q -b -l run_starjet_pythia.C\($NEVENTS,$XSECTION\) #>& $OUTPUTDIR/run_starjet_pythia.log
