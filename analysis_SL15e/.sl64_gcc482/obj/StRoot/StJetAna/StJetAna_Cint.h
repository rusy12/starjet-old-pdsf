/********************************************************************
* .sl64_gcc482/obj/StRoot/StJetAna/StJetAna_Cint.h
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************************/
#ifdef __CINT__
#error .sl64_gcc482/obj/StRoot/StJetAna/StJetAna_Cint.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#define G__PRIVATE_GVALUE
#include "G__ci.h"
#include "FastAllocString.h"
extern "C" {
extern void G__cpp_setup_tagtableStJetAna_Cint();
extern void G__cpp_setup_inheritanceStJetAna_Cint();
extern void G__cpp_setup_typetableStJetAna_Cint();
extern void G__cpp_setup_memvarStJetAna_Cint();
extern void G__cpp_setup_globalStJetAna_Cint();
extern void G__cpp_setup_memfuncStJetAna_Cint();
extern void G__cpp_setup_funcStJetAna_Cint();
extern void G__set_cpp_environmentStJetAna_Cint();
}


#include "TObject.h"
#include "TMemberInspector.h"
#include "StJetAna.h"
#include "StJetBuildResponseMatrix.h"
#include "StJetBuildResponseMatrixROO.h"
#include <algorithm>
namespace std { }
using namespace std;

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__StJetAna_CintLN_TClass;
extern G__linked_taginfo G__StJetAna_CintLN_TBuffer;
extern G__linked_taginfo G__StJetAna_CintLN_TMemberInspector;
extern G__linked_taginfo G__StJetAna_CintLN_TObject;
extern G__linked_taginfo G__StJetAna_CintLN_TNamed;
extern G__linked_taginfo G__StJetAna_CintLN_TString;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEunsignedsPintcOallocatorlEunsignedsPintgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEdoublecOallocatorlEdoublegRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_StPicoDstMaker;
extern G__linked_taginfo G__StJetAna_CintLN_iteratorlEbidirectional_iterator_tagcOTObjectmUcOlongcOconstsPTObjectmUmUcOconstsPTObjectmUaNgR;
extern G__linked_taginfo G__StJetAna_CintLN_StPicoDst;
extern G__linked_taginfo G__StJetAna_CintLN_TDataSet;
extern G__linked_taginfo G__StJetAna_CintLN_pairlEdoublecOintgR;
extern G__linked_taginfo G__StJetAna_CintLN_maplEstringcOTObjArraymUcOlesslEstringgRcOallocatorlEpairlEconstsPstringcOTObjArraymUgRsPgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_TVectorTlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TVectorTlEdoublegR;
extern G__linked_taginfo G__StJetAna_CintLN_TF1;
extern G__linked_taginfo G__StJetAna_CintLN_TH1D;
extern G__linked_taginfo G__StJetAna_CintLN_TFile;
extern G__linked_taginfo G__StJetAna_CintLN_TTree;
extern G__linked_taginfo G__StJetAna_CintLN_StMemStat;
extern G__linked_taginfo G__StJetAna_CintLN_StMaker;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEintcOallocatorlEintgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEintcOallocatorlEintgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_multimaplEpairlEdoublecOintgRcOintcOlesslEpairlEdoublecOintgRsPgRcOallocatorlEpairlEconstsPpairlEdoublecOintgRcOintgRsPgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTBaselEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTBaselEdoublegR;
extern G__linked_taginfo G__StJetAna_CintLN_TElementActionTlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TElementPosActionTlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTRow_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTRowlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTDiag_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTColumn_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTFlat_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSub_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSparseRow_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSparseDiag_constlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTColumnlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTDiaglEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTFlatlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSublEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSparseRowlEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TMatrixTSparseDiaglEfloatgR;
extern G__linked_taginfo G__StJetAna_CintLN_TH2D;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlETStringcOallocatorlETStringgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlETStringcOallocatorlETStringgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_TPythia6;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEfastjetcLcLPseudoJetcOallocatorlEfastjetcLcLPseudoJetgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEfastjetcLcLPseudoJetcOallocatorlEfastjetcLcLPseudoJetgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_pairlEstringcOunsignedsPintgR;
extern G__linked_taginfo G__StJetAna_CintLN_listlEpairlEstringcOunsignedsPintgRcOallocatorlEpairlEstringcOunsignedsPintgRsPgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEconstsPfastjetcLcLPseudoJetmUcOallocatorlEconstsPfastjetcLcLPseudoJetmUgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEconstsPfastjetcLcLPseudoJetmUcOallocatorlEconstsPfastjetcLcLPseudoJetmUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEfastjetcLcLClusterSequencecLcLhistory_elementcOallocatorlEfastjetcLcLClusterSequencecLcLhistory_elementgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEfastjetcLcLClusterSequencecLcLhistory_elementcOallocatorlEfastjetcLcLClusterSequencecLcLhistory_elementgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_setlEconstsPfastjetcLcLClusterSequencecLcLhistory_elementmUcOlesslEconstsPfastjetcLcLClusterSequencecLcLhistory_elementmUgRcOallocatorlEconstsPfastjetcLcLClusterSequencecLcLhistory_elementmUgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_multimaplEdoublecOpairlEintcOintgRcOlesslEdoublegRcOallocatorlEpairlEconstsPdoublecOpairlEintcOintgRsPgRsPgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEfastjetcLcLClusterSequencecLcLTilecOallocatorlEfastjetcLcLClusterSequencecLcLTilegRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEfastjetcLcLClusterSequencecLcLTilecOallocatorlEfastjetcLcLClusterSequencecLcLTilegRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_vectorlEfastjetcLcLClusterSequenceActiveAreacLcLGhostJetcOallocatorlEfastjetcLcLClusterSequenceActiveAreacLcLGhostJetgRsPgR;
extern G__linked_taginfo G__StJetAna_CintLN_reverse_iteratorlEvectorlEfastjetcLcLClusterSequenceActiveAreacLcLGhostJetcOallocatorlEfastjetcLcLClusterSequenceActiveAreacLcLGhostJetgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__StJetAna_CintLN_StJetAna;
extern G__linked_taginfo G__StJetAna_CintLN_StJetBuildResponseMatrix;
extern G__linked_taginfo G__StJetAna_CintLN_StJetBuildResponseMatrixROO;

/* STUB derived class for protected member access */
