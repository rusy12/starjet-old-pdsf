/* This was generated for version 'SL15e' */
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#ifdef __CINT__
#pragma link C++ class Pico+;
#pragma link C++ class StPicoCut+;
#pragma link C++ class StPicoDst+;
#pragma link C++ class StPicoDstMaker+;
#pragma link C++ class StPicoEvent+;
#pragma link C++ class StPicoTrack+;
#pragma link C++ class StPicoV0+;
#endif
