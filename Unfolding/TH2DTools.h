#ifndef __TH2DTools__hh
#define __TH2DTools__hh

class TH2D;

class TH2DTools
{
 public:
  TH2DTools() {;}
  ~TH2DTools() {;}

  void Divide(TH2D *numerator, TH2D *denominator, TH2D *hratio);
  void DiffOverSum(TH2D *numerator, TH2D *denominator, TH2D *hratio);
  void Difference(TH2D *h1, TH2D *h2, TH2D *result);
};

#endif
