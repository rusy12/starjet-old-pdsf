#ifndef __UnfoldSVD__hh
#define __UnfoldSVD__hh
 
#include "Rtypes.h"
#include "TString.h"

class TSVDUnfold;
class TH1D;
class TH2D;
class TFile;

class UnfoldSVD
{
 public:
  UnfoldSVD(Int_t kterm, TString outfile);
  ~UnfoldSVD();

  void Unfold();
  void SetHistograms(TH1D *measured, TH1D *prior, TH2D *response_matrix);
  void SmearPrior(int statistics, double binmean);

 private:

 protected:	
  TFile *fout;
  Int_t fKterm;		//cut-off term

const  TH1D *HPrior;
const  TH1D *HMeasured;
  TH1D *HUnfolded;
const  TH2D *HResponseE;
const  TH2D *HResponseP;
const  TH1D *HSmearedPrior;
  TH1D *HDvector;
  TH2D *HCov;

};

#endif
