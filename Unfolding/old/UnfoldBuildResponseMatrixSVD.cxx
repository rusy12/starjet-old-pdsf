#include "TF1.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TDirectoryFile.h"
 
#include "UnfoldBuildResponseMatrixSVD.h"

//=============================================================================
UnfoldBuildResponseMatrixSVD::UnfoldBuildResponseMatrixSVD(TString path, Float_t R, Float_t pTthresh, Int_t priorNo)
{
  nbins = 800;
  nevts = 1E9;

  pTmin = 0.0;
  pTmax = 100.;

  hResponse = 0x0;
  
  finput = new TFile(Form("%s/histos_dpTarea_R%.1lf_pTcut0.2.root", path.Data(), R), "OPEN");
  TString true_path=Form("/home/rusnak/jet_analysis/toymodel/DataOut/sp/jetonly/charged_R%.1lf",R);
  TString prior_path="/home/rusnak/jet_analysis/STARJet/out/MB/prior";
  TString prior_type[]={"pp_scaled","flat","pythia","powlaw5","powlaw6","powlaw4","powlaw3"};

  Float_t pTemb[] = {0.1, 1.0, 2.0, 3.0, 5.0, 10.0, 15.0};
  //Float_t pTemb[] =  {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0};
  for(Int_t idist = 0; idist < nEmb; idist++)
    {
      TString name = Form("hdpT_Acut_pTemb%.1lf", pTemb[idist]);
      hdpT[idist] = (TH1D*)finput->Get(name.Data());

      // removing 1 +/- 1
      for(Int_t bin = 1; bin <= 2000; bin++)
	if(hdpT[idist]->GetBinContent(bin) == hdpT[idist]->GetBinError(bin))
	  {
	    hdpT[idist]->SetBinContent(bin, 0);
	    hdpT[idist]->SetBinError(bin, 0);
	  }
    }

  fout = new TFile(Form("%s/response_matrix_SVD_R%.1lf_pTlead%.1lf_prior%i.root", path.Data(), R, pTthresh, priorNo), "RECREATE");

//seting up prior distribution
if(priorNo==0){
  str = Form("%s/histos_jets_R%.1lf_pTcut0.2.root", true_path.Data(),R);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  hprior = hPtRecpTleadingPrior->ProjectionY("hSignalSpectrum", firstbin, lastbin);
  }
else if (priorNo==2){
  str = Form("%s/histos_pythiajet_R%.1lf.root", prior_path.Data(),R);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPrior2d = (TH2D*)fprior->Get("hpT_pTlead");
  Int_t firstbin = hPrior2d->GetYaxis()->FindBin(pTthresh);
//cout<<"firstbin "<<firstbin<<endl;
  Int_t lastbin = hPrior2d->GetNbinsY();
//cout<<"lastbin "<<lastbin<<endl;
  hprior = (TH1D*)hPrior2d->ProjectionX("prior_pythia", firstbin, lastbin);
  }

else{
  str = Form("%s/histos_prior.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  str=Form("hprior_%s",prior_type[priorNo].Data());
  TH2D *hprior2d = (TH2D*)fprior->Get(str.Data());
  Int_t firstbin = hprior2d->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = firstbin;
  TString priorName=Form("prior_%i",priorNo);
  hprior = hprior2d->ProjectionX(priorName,firstbin,lastbin,"e");
  }


}

//==============================================================================
UnfoldBuildResponseMatrixSVD::~UnfoldBuildResponseMatrixSVD()
{
  fout->Close();
  delete fout;
}

//==============================================================================
void UnfoldBuildResponseMatrixSVD::BuildDeltaPtResponseMatrix()
{
  TString name;
  Int_t save_evt = 8;

  hResponse = new TH2D("hResponse", "hResponse;p_{T}^{meas};p_{T}^{true};entries", nbins, -pTmax, +pTmax, nbins, -pTmax, +pTmax);
  hMCtrue = new TH1D("hMCtrue", "hMCtrue;p_{T}^{true};entries", nbins, -pTmax, +pTmax);
  hMCreco = new TH1D("hMCreco", "hMCreco;p_{T}^{reco};entries", nbins, -pTmax, +pTmax);
  
  for(Int_t ievt = 0; ievt < nevts; ievt++)
    {
      Double_t pT = hprior->GetRandom();
      Double_t dpT = SmearWithDeltaPt(pT);
		hMCtrue->Fill(pT);
      hResponse->Fill(pT + dpT, pT);
		hMCreco->Fill(pT+dpT);

      if(ievt != TMath::Power(10, save_evt) - 1) continue;
      fout->cd();
      name = Form("hResponse_1E%d", save_evt);
      hResponse->Write(name.Data());
      name = Form("hMCtrue_1E%d", save_evt);
      hMCtrue->Write(name.Data());
      name = Form("hMCreco_1E%d", save_evt);
      hMCreco->Write(name.Data());
      cout << Form("Event 1E%d saved!", save_evt) << endl;
      save_evt++;
    }
  
  delete hResponse;
  delete hMCtrue;
  delete hMCreco;
}

//==============================================================================
Double_t UnfoldBuildResponseMatrixSVD::SmearWithDeltaPt(Double_t pT)
{
  Float_t fEmbPt[] = {0.1, 1.0, 2.0, 3.0, 5.0, 10.0, 15.0};
  //Float_t fEmbPt[] =  {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0};
   Double_t dpT = 0;
  if (pT <= fEmbPt[0]){
    dpT = hdpT[0]->GetRandom();
   }
  for(int i=0; i<(nEmb-1); i++){
   if(pT>fEmbPt[i] && pT<=fEmbPt[i+1]){
   Double_t rnd=gRandom->Uniform(fEmbPt[i],fEmbPt[i+1]);
   if(pT>rnd) dpT = hdpT[i+1]->GetRandom();
   else dpT = hdpT[i]->GetRandom();
   }
  }
  if(pT>fEmbPt[nEmb-1]) {
   dpT = hdpT[nEmb-1]->GetRandom();
}
  return dpT;

/*
//-------------------------
  Double_t dpT = 0.0;
	  
  if(pT < 1.0)
    dpT = hdpT[0]->GetRandom();
  else
  if(pT < 2.0)
    dpT = hdpT[1]->GetRandom();
  else
  if(pT < 3.0)
    dpT = hdpT[2]->GetRandom();
  else
  if(pT < 5.0)
    dpT = hdpT[3]->GetRandom();
  else
  if(pT < 10.0)
	 dpT = hdpT[4]->GetRandom();
  else
  if(pT < 15.0)
    dpT = hdpT[5]->GetRandom();
  else
    dpT = hdpT[6]->GetRandom();
  return dpT;
  */
}
