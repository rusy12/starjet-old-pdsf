#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TFile.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "Riostream.h"
#include "TStopwatch.h"
#include "TSVDUnfold.h"

#include "UnfoldSVD.h"

//======================================================================
UnfoldSVD::UnfoldSVD(Int_t kterm, TString outfile)
{
	fKterm=kterm;
	fout=new TFile(outfile.Data(), "recreate");
	fout->cd();
}

//======================================================================
UnfoldSVD::~UnfoldSVD()
{
  fout->Close();

  delete fout;
}

//==============================================================================
void UnfoldSVD::SetHistograms(TH1D *measured, TH1D *prior, TH2D *response_matrix/*,double binMean=0.5*//*where to place mean value of the pT bin*/)
{
  //SET/CHANGE HISTOGRAM TITLES
  prior->SetTitle("prior spectrum");
  prior->SetXTitle("p_{T}^{corr}");
  measured->SetTitle("measured data");
  measured->SetXTitle("p_{T}^{corr}");

  HPrior = (TH1D*)prior->Clone("HPrior");
  HMeasured = (TH1D*)measured->Clone("HMeasured");
  //HUnfolded = (TH1D*)prior->Clone("HUnfolded");
  HResponseP = (TH2D*)response_matrix->Clone("HResponseP");
	
  //Recalculate Response Matrix: probabilities->events
  response_matrix->Sumw2();
  Int_t nxbins=response_matrix->GetNbinsX();
  Int_t nybins=response_matrix->GetNbinsY();
  for(Int_t biny = 1; biny <= nybins; biny++){
  for(Int_t binx = 1; binx <= nxbins; binx++){
      Double_t value = response_matrix->GetBinContent(binx, biny)*HPrior->GetBinContent(biny);
      response_matrix->SetBinContent(binx, biny, value);
      response_matrix->SetBinError(binx, biny, TMath::Sqrt(value));
}}
  HResponseE = (TH2D*)response_matrix->Clone("HResponseE");
  //cout<<"smearing prior"<<endl;
  //SmearPrior((int)1E6,binMean); //create smeared prior distribution

  fout->cd();
  cout<<"writing output"<<endl;
  HPrior->Write("hprior");
  HResponseE->Write("hresponse_event");
  HResponseP->Write("hresponse_probab");
  HMeasured->Write("hmeasured");

}

//==============================================================================
void UnfoldSVD::Unfold()
{
cout<<"starting unfolding"<<endl;
TSVDUnfold * unfolding = new TSVDUnfold(HMeasured, HSmearedPrior, HPrior, HResponseE);
cout<<"unfolding"<<endl;
HUnfolded = unfolding->Unfold(fKterm);	
cout<<"D vector"<<endl;
HDvector = unfolding->GetD();
cout<<"unfolding done"<<endl;
cout<<"making covariance matrix"<<endl;
HCov = unfolding->GetAdetCovMatrix(5);


cout<<"writing output"<<endl;
fout->cd();
HDvector->Write("hdvector");
HUnfolded->Write("hunfolded");
HCov->Write("hcovariance");
}
//==============================================================================
void UnfoldSVD::SmearPrior(int statistics, double binmean=0.5/*where to place mean value of the pT bin*/)
{
	Double_t norm=HPrior->Integral();
	Int_t nBins=HPrior->GetNbinsX();
   //Double_t binWidth=HPrior->GetBinWidth(1);
   //Double_t pTmin=HPrior->GetBinCenter(1)-(binWidth/2);
   //Double_t pTmax=HPrior->GetBinCenter(nBins)+(binWidth/2);
   TH1D* hsmeared=(TH1D*) HMeasured->Clone("hsmeared");
	hsmeared->Reset("MICE");
   TH1D* hpriort=(TH1D*) HPrior->Clone("hpriort"); //just a temp copy of the prior histogram which is not const so it is easier to work with it
	for(int binNo=1;binNo<=nBins;binNo++){
   	Double_t pT = hpriort->GetBinLowEdge(binNo)+(hpriort->GetBinWidth(binNo))*binmean;
      //Int_t binNo = hpriort->FindBin(pT);
	   Double_t yield = hpriort->GetBinContent(binNo);
	   if(yield==0)continue;
	   Int_t ybin=HResponseP->GetYaxis()->FindBin(pT);
	   TH1D *hsmear = HResponseP->ProjectionX("hsmear", ybin, ybin);
	   //Double_t norm=hsmear->Integral();
	   //hsmear->Scale(1/norm);
	   for(int i=0;i<statistics;i++){
   		Double_t pTsmear = hsmear->GetRandom();
		   hsmeared->Fill(pTsmear, yield/statistics);
	   }
   	delete hsmear;
	}
	hsmeared->Scale(1/norm);
   HSmearedPrior=(TH1D*) hsmeared->Clone("HSmearedPrior");
  	delete hsmeared;
   delete hpriort;

  cout<<"writing smeared prior"<<endl;
  fout->cd();
  HSmearedPrior->Write("hsmeredprior");
}

