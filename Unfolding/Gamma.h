#ifndef __Gamma__hh
#define __Gamma__hh

class Gamma
{
 public:
  Gamma(int npar); 
  ~Gamma();

  double operator() (double *x, double *p);

  int fnpar;
};

#endif
