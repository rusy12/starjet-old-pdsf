#include "Gamma.h"
#include "TMath.h"

Gamma::Gamma(int npar)
{
  fnpar = npar;
}

Gamma::~Gamma()
{
}

double Gamma::operator() (double *x, double *p)
{
  // f_gamma(pT; b, np, a) = A*b / Gamma(np) * [b(pT - a) + (np - 1)]^{np - 1} * exp{-b(pT - a) - (np - 1)}

  // log(f) = log(A) + log(b) - log(Gamma(np)) + (np - 1)*log[b(pT - a) + (np - 1)] - b(pT - a) - (np - 1)

  // f1 = log(b) - log(Gamma(np))
  // f2 = (np - 1)*log[b(pT - a) + (np - 1)]
  // f3 = - b(pT - a) - (np - 1)

  // par[0] = b
  // par[1] = np 
  // par[2] = a
  // par[3] = A

  double pT = *x;
  double par[fnpar];

  for(int i = 0; i < fnpar; i++)
    par[i] = p[i];

  double f1 = TMath::Log(par[0]) - TMath::LnGamma(par[1]);
  
  double f2 = (par[1] - 1) * TMath::Log(par[0]*(pT - par[2]) + (par[1] - 1));
  
  double f3 = -par[0]*(pT - par[2]) - (par[1] - 1);

  return par[3]*TMath::Exp(f1 + f2 + f3);
}
