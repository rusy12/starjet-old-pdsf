#include "TH2D.h"
#include "Riostream.h"

#include "TH2DTools.h"

//________________________________________________________________________________
void TH2DTools::Divide(TH2D *probe, TH2D *reference, TH2D *hratio)
{
  probe->Reset("MICES");

  Int_t nbinsXden = reference->GetNbinsX();
  Int_t nbinsYden = reference->GetNbinsY();
  
  for(Int_t binx = 1; binx <= nbinsXden; binx++)
    for(Int_t biny = 1; biny <= nbinsYden; biny++)
      {
	Double_t yield_reference = reference->GetBinContent(binx, biny);
	
	if(!yield_reference) continue;
	
	Double_t yield_probe = probe->GetBinContent(binx, biny);
	
	Double_t ratio = yield_probe / yield_reference;

	hratio->SetBinContent(binx, biny, ratio);
      }
}

//________________________________________________________________________________
void TH2DTools::DiffOverSum(TH2D *probe, TH2D *reference, TH2D *hratio)
{
  Int_t nbinsXden = reference->GetNbinsX();
  Int_t nbinsYden = reference->GetNbinsY();
  
  for(Int_t binx = 1; binx <= nbinsXden; binx++)
    for(Int_t biny = 1; biny <= nbinsYden; biny++)
      {
	Double_t yield_reference = reference->GetBinContent(binx, biny);
	Double_t yield_probe = probe->GetBinContent(binx, biny);

	Double_t ratio = -2.;

 	if(yield_reference && yield_probe )
	  ratio = (yield_reference - yield_probe) / (yield_reference + yield_probe);

	hratio->SetBinContent(binx, biny, ratio);
      }
}

//________________________________________________________________________________
void TH2DTools::Difference(TH2D *h1, TH2D *h2, TH2D *result)
{
  // Plot h1 - h2
  result->Reset("MICE");
  Int_t nbinsx = result->GetNbinsX();
  Int_t nbinsy = result->GetNbinsY();
  
  for(Int_t binx = 1; binx <= nbinsx; binx++)
    for(Int_t biny = 1; biny <= nbinsy; biny++)
      {
	Double_t yield_h1 = h1->GetBinContent(binx, biny);
	Double_t yield_h2 = h2->GetBinContent(binx, biny);
	Double_t difference = yield_h1 - yield_h2;

	result->SetBinContent(binx, biny, difference);
      }
}
