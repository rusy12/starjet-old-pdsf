void unfold_roounfold()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;
  TString prior_type[]={"pp_scaled","flat","pythia","powlaw5","powlaw6","powlaw4","powlaw3"};

  Int_t priorNo= atoi(gSystem->Getenv("PRIOR"));
  TString data_path = gSystem->Getenv("DATA_PATH");
  TString prior_path = gSystem->Getenv("PRIOR_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");
  TString rtype=gSystem->Getenv("RMATRIX_TYPE");
  TString type=gSystem->Getenv("TYPE");

  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
  Double_t R = atof(gSystem->Getenv("RPARAM"));
  Int_t nbins = atoi(gSystem->Getenv("NBINS"));
  Int_t niter = atoi(gSystem->Getenv("NITER"));
  Int_t efficorr= atoi(gSystem->Getenv("EFFICORR"));

  //I/O files
  str =Form("%s/Unfolded_R%.1lf_Bayes_%ibins_%s/%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", data_path.Data(), R, nbins, type.Data(), prior_type[priorNo].Data(),R, pTthresh); 
  TFile *fout = new TFile(str.Data(), "RECREATE");

  TFile *finput; 
  TH1D *hSignalSpectrum;
if(rtype!="dete"){
  str = Form("%s/histos_inclusivejet_R%.1lf.root", data_path.Data(), R);
  finput= new TFile(str.Data(), "OPEN");
  
  TH2D *hDSpTleading = (TH2D*)finput->Get("hpT_pTlead_nobadsecedge1R");
  Int_t firstbin = hDSpTleading->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsY();
  hSignalSpectrum = hDSpTleading->ProjectionX("hSignalSpectrum", firstbin, lastbin);
  }
else{
/*  str =Form("%s/Unfolded_R%.1lf_Bayes_%ibins_BG_sp/%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", data_path.Data(), R, nbins, prior_type[priorNo].Data(),R, pTthresh); 
finput= new TFile(str.Data(), "OPEN");
TDirectoryFile* dir = (TDirectoryFile*)finput->Get("iter5");
hSignalSpectrum=(TH1D*)dir->Get("hunfolded"); 
*/

str =Form("/eliza17/star/pwg/starjetc/rusnak/jets/run11_AuAu/Pythia/pythia_charged_R%.1lf/histos_pythiajet_R%.1lf_eff.root",R,R);
finput= new TFile(str.Data(), "OPEN");
TH2D *hDSpTleading = (TH2D*)finput->Get("hpT_pTlead");
  Int_t firstbin = hDSpTleading->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsY();
  hSignalSpectrum = hDSpTleading->ProjectionX("hSignalSpectrum", firstbin, lastbin);

}
  hSignalSpectrum->Sumw2();
  Double_t int_signal=hSignalSpectrum->Integral();

//RESPONSE MATRIX, PRIOR
  str = Form("%s/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", rmatrix_path.Data(),rtype.Data(), R,pTthresh,priorNo);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  
  TH1D *hMCtrue = (TH1D*)frmatrix->Get("hMCtrue_1E9"); //MC input spectrum
  TH2D *rmatrix;
  TH1D *hMCreco;
  if(efficorr){
  rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9_eff"); //response matrix
  hMCreco = (TH1D*)frmatrix->Get("hMCreco_1E9_eff"); //MC measured spectrum
  }
  else{
  rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9"); //response matrix
  hMCreco = (TH1D*)frmatrix->Get("hMCreco_1E9"); //MC measured spectrum
  }

  // MAKE SURE DIM = nbins
  // for time constrain purpose
  Int_t nbinsx;
  nbinsx = hSignalSpectrum->GetNbinsX();
  if(nbinsx != nbins) hSignalSpectrum->Rebin(nbinsx / nbins);
  nbinsx = hMCreco->GetNbinsX();
  if(nbinsx != nbins) 
  {
	hMCreco->Rebin(nbinsx / nbins);
   hMCtrue->Rebin(nbinsx / nbins);
  }

  // MAKE SURE DIM = nbins x nbins
  Int_t nbinsy;
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);


  fout->mkdir("input");
  fout->cd("input");

  hMCtrue->Write("hprior");
  rmatrix->Write("hresponse");
  //rmatrix->Write("PEC"); //for backward compatibility
  hSignalSpectrum->Write("hmeasured");

//UNFOLDING with RooUnfold
RooUnfoldResponse response (hMCreco,hMCtrue,rmatrix);
for(Int_t iteration=0; iteration<niter; iteration++)
{
cout<<"BAYESIAN UNFOLDING, iteration: "<<iteration<<endl;
RooUnfoldBayes   unfold (&response, hSignalSpectrum, iteration+1);
cout<<"WRITING OUTPUT"<<endl;

  //cout<<"integral"<<hReco->Integral()<<endl; 
  //unfold.PrintTable (cout, hSignalSpectrum); 
 
  TH1D* hReco= (TH1D*) unfold.Hreco(); 

  //Get the covariance matrix
cout<<"saving covariance matrix"<<endl;
  TMatrixD CovM = unfold.Ereco();
  TH2D* hcovariance=rmatrix->Clone("hcovariance");
  hcovariance->Reset("MICE");
  for(Int_t i = 0; i < nbins; i++){
    for(Int_t j = i; j <nbins; j++)
      {
   Double_t covij_mult = CovM(i, j);
   hcovariance->SetBinContent(i+1, j+1, covij_mult);
   hcovariance->SetBinContent(j+1, i+1, covij_mult);
   }
   }


cout<<"writing output"<<endl;
  fout->mkdir(Form("iter%d", iteration));
  fout->cd(Form("iter%d", iteration));

  hReco->Write("hunfolded"); 
  hcovariance->Write("hcovariance"); 

}//iterations

  fout->Close(); 
  delete fout; 
 
 
  timer.Stop(); 
  timer.Print();
}

