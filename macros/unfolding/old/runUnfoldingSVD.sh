#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw7 powlaw6_1 powlaw6_2 powlaw6_3 powlaw6_1m powlaw6_2m powlaw6_3m)
COLLIDER="RHIC" # "LHC" # 
#export PTCUT=0.2
export RPARAM=0.4
WRKDIR="/home/rusnak/jet_analysis/STARJet/out/$TRG"
export DATA_PATH="$WRKDIR/inclusive"
export RMATRIX_PATH="$WRKDIR/embedding"
export TRUE_PATH="$TOYMODELPATH/DataOut/sp/jetonly/10M_charged_R$RPARAM"
export PRIOR_PATH="$WRKDIR/prior"

#cd ~/jet_analysis/STARJet/macros
for PRIOR in 4 #2 4 6 7 8 9 10 11 #true, flat, pythia, powerlaw -5, -6, -7, (pT+1)^-6, (pT+2)^-6 (pT+3)^-6, (pT-1)^-6, (pT-2)^-6 (pT-3)^-6
	do
	export PRIOR
	OUT_DIR=$DATA_PATH"/Unfolded_SVD/"${prior_type[$PRIOR]}
	echo "creating directory: $OUT_DIR"
	mkdir -p $OUT_DIR
		for PTTHRESH in 5.0 #3.0 5.0 6.0 #15.0 20.0 25.0 #
		  do
		  export PTTHRESH
		  root -q -b unfold_SVD.C
		done
done
