#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE"
    exit 1
}

TRG=$1 #HT, MB
export RMATRIX_TYPE=$2 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "TRIGGER: $TRG"
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

export NBINS=200
#export NBINS=62
export EFFICORR=1 # do efficiency correction

for RPARAM in 0.4 #0.3
do
export RPARAM

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw4 powlaw3)

WRKDIR="$HOME/jet_analysis/STARJet/out/$TRG"
export DATA_PATH="$WRKDIR/inclusive_R${RPARAM}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding_R${RPARAM}/rmatrix"

for PRIOR in 2 #5 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-4) 6:pT^(-3)
do
	export PRIOR
	#OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${NBINS}bins/"${prior_type[$PRIOR]}
	OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_SVD_${NBINS}bins_${RMATRIX_TYPE}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   mkdir -p $OUT_DIR

	for PTTHRESH in 5 #4 5 6 7 # 10.0 15.0 20.0 25.0 #
	do
	  export PTTHRESH
		for KTERM in 1 #2 3 4 5 6 7 8 9 10
		do
			export KTERM
 		   root -b -l -q unfold_SVD.C
		   #root -l -q -b unfold_SVD_uneqbin.C
		done
	done
done
done
