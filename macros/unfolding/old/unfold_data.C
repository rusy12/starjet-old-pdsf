void unfold_data()
{
  TStopwatch timer;
  timer.Start();
	
  Int_t priorNo= atoi(gSystem->Getenv("PRIOR"));	 
  TString prior_type[]={/*0*/"pp_scaled",/*1*/"flat",/*2*/"pythia",/*3*/"powlaw5",/*4*/"powlaw6",/*5*/"powlaw7",/*6*/"powlaw6_1",/*7*/"powlaw6_2",/*8*/"powlaw6_3",/*9*/"powlaw6_1m",/*10*/"powlaw6_2m",/*11*/"powlaw6_3m"};
  TString str;

  Int_t niterations = 6;

//  Float_t priorScale = 1; //scale the prior distribution nevents to the input distribution number of events

  Int_t doEfficorr =  atof(gSystem->Getenv("EFFI_CORR"));
  TString rmatrix_type = gSystem->Getenv("RMATRIX_TYPE");
  TString data_path = gSystem->Getenv("DATA_PATH");
  TString out_dir = gSystem->Getenv("OUT_DIR");
  TString prior_path = gSystem->Getenv("PRIOR_PATH");
  TString true_path = gSystem->Getenv("TRUE_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");
  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
  Double_t r = atof(gSystem->Getenv("RPARAM"));
 
  Int_t nbins = 400;

  str = Form("%s/histos_inclusivejet_R%.1lf.root", data_path.Data(), r);
  TFile *finput = new TFile(str.Data(), "OPEN");
  
  TH2D *hDSpTleading = (TH2D*)finput->Get("hpT_pTlead_nobadsecedge1R");
  //hDSpTleading->Sumw2();
  Int_t firstbin = hDSpTleading->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsY();
  TH1D *hSignalSpectrum = hDSpTleading->ProjectionX("hSignalSpectrum", firstbin, lastbin);
  hSignalSpectrum->Sumw2();
  Double_t int_signal=hSignalSpectrum->Integral();

  //TH1D *hFullSpectrum = hDSpTleading->ProjectionX("hFullSpectrum", 1, lastbin);
  //hFullSpectrum->Sumw2();
  
  str = Form("%s/response_matrix_deltapT_%s_R%.1lf_pTlead%0.lf.root", rmatrix_path.Data(),rmatrix_type.Data(),r,pTthresh);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9");

if(doEfficorr){
str = Form("%s/epsilon_R%.1lf_pTlead%0.lf.root", rmatrix_path.Data(),r,pTthresh);
  TFile *fepsilon = new TFile(str.Data(), "OPEN");
  TH1D *hepsilon = (TH1D*)fepsilon->Get("hepsilon");
}
  /*str = Form("%s/histos_embeddedjet_R%.1lf.root",rmatrix_path.Data(),r);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  str= Form("delta_pt_%s",rmatrix_type.Data());
  TH2D *rmatrix = (TH2D*)frmatrix->Get(str);
*/
  // MAKE SURE DIM = nbins
  // for time constrain purpose
  Int_t nbinsx;
  //nbinsx = hFullSpectrum->GetNbinsX();
  //if(nbinsx != nbins) hFullSpectrum->Rebin(nbinsx / nbins);

  nbinsx = hSignalSpectrum->GetNbinsX();
  if(nbinsx != nbins) hSignalSpectrum->Rebin(nbinsx / nbins);


  // MAKE SURE DIM = nbins x nbins
  Int_t nbinsy;
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);
  // TRANSPOSING R-MATRIX 
  /*
  TH2D *htemp = (TH2D*)rmatrix->Clone("htemp");
  rmatrix->Reset("MICE");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetYaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  for(Int_t binx = 1; binx <= nbins; binx++)
    for(Int_t biny = 1; biny <= nbins; biny++)
      rmatrix->SetBinContent(biny, binx, htemp->GetBinContent(binx, biny));
  */

  // PRIOR
  TH1D *hprior = (TH1D*)hSignalSpectrum->Clone("hprior");
  hprior->SetName("hprior");
  hprior->Reset("MICE");

if(priorNo==0){
  str = Form("%s/histos_jets_R%.1lf_pTcut0.2.root", true_path.Data(),r);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  TH1D *hPtReco = (TH1D*)hPtRecpTleadingPrior->ProjectionY("prior_0", firstbin, lastbin);
  }
else if (priorNo==2){
  str = Form("%s/histos_pythiajet_R%.1lf.root", prior_path.Data(),r);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPrior2d = (TH2D*)fprior->Get("hpT_pTlead");
  Int_t firstbin = hPrior2d->GetYaxis()->FindBin(pTthresh);
//cout<<"firstbin "<<firstbin<<endl;
  Int_t lastbin = hPrior2d->GetNbinsY();
//cout<<"lastbin "<<lastbin<<endl;
  TH1D *hPtReco = (TH1D*)hPrior2d->ProjectionX("prior_pythia", firstbin, lastbin);
  }
else{
  str = Form("%s/histos_prior.root", prior_path.Data());
  TFile *finput2 = new TFile(str.Data(), "OPEN");
  str=Form("hprior_%s",prior_type[priorNo].Data());
  TH2D *hprior2d = (TH2D*)finput2->Get(str.Data());
  firstbin = hprior2d->GetYaxis()->FindBin(pTthresh);
cout<<"pT thresh: "<<pTthresh<<"bin number:"<<firstbin<<endl;
  lastbin = firstbin;
  TString priorName=Form("prior_%i",priorNo);
  TH1D *hPtReco = (TH1D*)hprior2d->ProjectionX(priorName,firstbin,lastbin,"e");
  }
  hPtReco->Sumw2();
  //Double_t int_prior=hPtReco->Integral();

  for(Int_t bin = 1; bin <= hPtReco->GetNbinsX(); bin++)
    {
      Double_t pT = hPtReco->GetBinCenter(bin);
      Double_t yield = hPtReco->GetBinContent(bin);
      Double_t error = hPtReco->GetBinError(bin);

		//if(pT>10)
      hprior->Fill(pT, yield);
		//else
      //hprior->Fill(pT, yield*TMath::Exp((-(pT-10)*(pT-10))/4));
    }
   Double_t int_prior=hprior->Integral();
	hprior->Scale(int_signal/int_prior);
	
	hprior->Sumw2();

  /*str = Form("%s/histos_prior.root", data_path.Data());
  TFile *finput2 = new TFile(str.Data(), "OPEN");
  str=Form("hprior_%s",prior_type[priorNo].Data());
  TH2D *hprior2d = (TH2D*)finput2->Get(str.Data());
  firstbin = hprior2d->GetYaxis()->FindBin(pTthresh);
  lastbin = firstbin+1; 
  TString priorName=Form("prior_%i",priorNo);
  TH1D *hprior = hprior2d->ProjectionX(priorName,firstbin,lastbin,"e");
  //TH1D *hprior = (TH1D*)finput->Get("hprior_gauss");
  hprior->SetName("hprior");
//hprior->Draw();

  hprior->Reset("MICE");
  str = Form("%s/prior.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("Pt_PtLead_AkTR04_pt02");
  //hPtRecpTleadingPrior->Sumw2();
  Int_t firstbin = hPtRecpTleadingPrior->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsY();
  TH1D *hPtReco = hPtRecpTleadingPrior->ProjectionX("hPtReco", firstbin, lastbin);
  hPtReco->Sumw2();

  for(Int_t bin = 1; bin <= hPtReco->GetNbinsX(); bin++)
    {
      Double_t pT = hPtReco->GetBinCenter(bin);
      Double_t yield = hPtReco->GetBinContent(bin);
      Double_t error = hPtReco->GetBinError(bin);
//if(bin%20==0)cout<<"yield: "<<yield<<endl;

      hprior->Fill(pT, yield*priorScale);
    }
  */
  str = Form("%s", out_dir.Data());
  
  UnfoldBayes *bayes = new UnfoldBayes(niterations, Form("%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", str.Data(), r, pTthresh));
 if(doEfficorr)
  bayes->SetHistograms(hSignalSpectrum, hprior, rmatrix, hepsilon);
 else
  bayes->SetHistograms(hSignalSpectrum, hprior, rmatrix);
  bayes->Unfold();
  delete bayes;

//hSignalSpectrum->Draw();
//hprior->Draw();
//rmatrix->Draw("COLZ");

  timer.Stop();
  timer.Print();
}

