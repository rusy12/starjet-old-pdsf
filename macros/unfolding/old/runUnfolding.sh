#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE"
    exit 1
}

TRG=$1 #HT, MB
export RMATRIX_TYPE=$2 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "TRIGGER: $TRG"
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw7 powlaw6_1 powlaw6_2 powlaw6_3 powlaw6_1m powlaw6_2m powlaw6_3m)
COLLIDER="RHIC" # "LHC" # 
#export PTCUT=0.2
export RPARAM=0.4
TOYMODELPATH="/home/rusnak/jet_analysis/toymodel"
WRKDIR="/home/rusnak/jet_analysis/STARJet/out/$TRG"
export DATA_PATH="$WRKDIR/inclusive"
export TRUE_PATH="$TOYMODELPATH/DataOut/sp/jetonly/10M_charged_R$RPARAM"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding"
export EFFI_CORR=0 #do jet reconstruction efficiency correction

#cd ~/jet_analysis/STARJet/macros
for PRIOR in 2 3 4 #5 #6 7 8 9 10 11 #true, flat, pythia, powerlaw -5, -6, -7, (pT+1)^-6, (pT+2)^-6 (pT+3)^-6, (pT-1)^-6, (pT-2)^-6 (pT-3)^-6
	do
	export PRIOR
	#export OUT_DIR=$DATA_PATH"/Unfolded/"$RMATRIX_TYPE"_corr/"${prior_type[$PRIOR]}
	export OUT_DIR=$DATA_PATH"/Unfolded/"$RMATRIX_TYPE"/"${prior_type[$PRIOR]}
	echo "creating directory: $OUT_DIR"
	mkdir -p $OUT_DIR
		for PTTHRESH in 4.0 5.0 6.0 #15.0 20.0 25.0 #
		  do
		  export PTTHRESH
		  root -q -b unfold_data.C
		done
done
