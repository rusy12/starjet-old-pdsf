#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE"
    exit 1
}

TRG=$1 #HT, MB
export RMATRIX_TYPE=$2 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "TRIGGER: $TRG"
echo "RMATRIX_TYPE: $RMATRIX_TYPE"
#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

#*******************************************************************

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw4 powlaw3)
export TYPE="${RMATRIX_TYPE}_effi_pTsmear"
export NBINS=200
#export NBINS=62
export NITER=10 #number of iterations
export EFFICORR=1 # do efficiency correction

# FOR PRIOR DISTRIBUTION
for RPARAM in 0.3 #0.3 0.4
do
export RPARAM

WRKDIR="$HOME/jet_analysis/STARJet/out/$TRG"
export DATA_PATH="$WRKDIR/inclusive_R${RPARAM}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding_R${RPARAM}/rmatrix"

for PRIOR in 2 #5 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-4) 6:(pT)^(-3)
do
	export PRIOR
	#OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${NBINS}bins/"${prior_type[$PRIOR]}
	OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_Bayes_${NBINS}bins_${TYPE}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   rm $OUT_DIR/*.root
   mkdir -p $OUT_DIR

	for PTTHRESH in 5.0 7.0 # 10.0 15.0 20.0 25.0 #
	do
	  export PTTHRESH
	  root -b -l -q unfold_roounfold.C
	  #root -b -l -q unfold_roounfold_uneqbin.C
	done
done
done
