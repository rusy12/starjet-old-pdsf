#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE" 
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"

RMATRIX_TYPE=$2  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

export RPARAM=0.4
export RMATRIX_TYPE
#export RMATRIX_TYPE="BG_sp" #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects  
export PATH_TO_DELTA_PT_HISTOGRAMS="/home/rusnak/jet_analysis/STARJet/out/${TRG}/embedding"
for PTTHRESH in 4 5 6 #15.0 20.0 25.0 #
   do
   export PTTHRESH
   root -l make_epsilon.C -q
   root -l buildResponseM.C -q
done

