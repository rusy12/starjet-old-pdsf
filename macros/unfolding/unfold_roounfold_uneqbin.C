void unfold_roounfold_uneqbin()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;
  TString prior_type[]={"truth","flat","pythia","powlaw3","powlaw45","powlaw5","powlaw55","levy","levy_alex"};

  Int_t priorNo= atoi(gSystem->Getenv("PRIOR"));
  TString data_path = gSystem->Getenv("DATA_PATH");
  TString out_dir = gSystem->Getenv("OUT_DIR");
  TString true_path = gSystem->Getenv("TRUE_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");
  TString rmatrix_type = gSystem->Getenv("RMATRIX_TYPE");
  Double_t pTcut = atof(gSystem->Getenv("PTCUT"));
  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
  Double_t R = atof(gSystem->Getenv("RPARAM"));
  //Int_t nbins = atoi(gSystem->Getenv("NBINS"));
  Int_t niter = atoi(gSystem->Getenv("NITER")); //number of iterations/kterms
  Int_t efficorr= atoi(gSystem->Getenv("EFFICORR"));//correct the result for jet reconstruction efficiency
  TString epsilon_path = gSystem->Getenv("EPSILON_PATH"); //path to efficiency files
  Int_t doSVD=atoi(gSystem->Getenv("SVD")); //SVD unfolding instead of Bayesian
  Int_t binChoice=atoi(gSystem->Getenv("BININGCH")); //which binning use
  bool smooth =0;
	if(!doSVD)smooth= atoi(gSystem->Getenv("SMOOTH"));//smooth unfolded solutions in between iterations

	//set of binning arrays
	const int nbinsarr0=30; //number of bins created from the array bellow 
	float binarr0[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,1,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,40,50,60,70,80,100};

	const int nbinsarr1a=33;
	float binarr1a[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,35,40,50,60,70,80,100};
	const int nbinsarr1b=18;
	float binarr1b[]={0,1,3,5,7,8,10,12,14,16,20,25,30,40,50,60,70,80,100};

	const int nbinsarr2a=33;
	float binarr2a[]={-100,-80,-60,-40,-20,-10,-5,-3,-1,0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,35,40,50,60,70,80,100};
	const int nbinsarr2b=15;
	float binarr2b[]={0,1,3,5,7,10,13,18,23,28,36,50,60,70,80,100};


	const int nbinsarr3a=13;
	float binarr3a[]={0,1,3,5,7,9,11,14,18,24,30,40,50,70,100};
	const int nbinsarr3b=13;
	float binarr3b[]={0,1,3,5,7,9,11,14,18,24,30,40,50,70,100};
   
	const int nbinsarr4a=13;
	float binarr4a[]={0,1,3,5,7,9,11,14,18,24,30,40,50,70,100};
	const int nbinsarr4b=12;
	float binarr4b[]={0,1,3,5,7,11,13,17,22,27,37,50,70,100};
 

 
//VARIABLE BINNING - use some of above definned arrays
  float pTrange=100; //pTrange in input histograms (has to be same in all histos!)
  int tmp_nbins;
  int tmp_nbins2;
  float* tmp_array=NULL;
  float* tmp_array2=NULL;
	if(binChoice==0)
   {
		tmp_nbins=nbinsarr0;
		tmp_nbins2=nbinsarr0;
		tmp_array=binarr0;
		tmp_array2=binarr0;
   }
   else if(binChoice==1)
   {
		tmp_nbins=nbinsarr1a;
		tmp_nbins2=nbinsarr1b;
		tmp_array=binarr1a;
		tmp_array2=binarr1b;
   }
	else if(binChoice==2)
   {
		tmp_nbins=nbinsarr2a;
		tmp_nbins2=nbinsarr2b;
		tmp_array=binarr2a;
		tmp_array2=binarr2b;
   }
	else if(binChoice==3)
   {
		tmp_nbins=nbinsarr3a;
		tmp_nbins2=nbinsarr3b;
		tmp_array=binarr3a;
		tmp_array2=binarr3b;
   }

	else if(binChoice==4)
   {
		tmp_nbins=nbinsarr4a;
		tmp_nbins2=nbinsarr4b;
		tmp_array=binarr4a;
		tmp_array2=binarr4b;
   }

  const Int_t newbins=tmp_nbins;
  float *pTbinArray=tmp_array;
	
  const Int_t newbins2=tmp_nbins2;
  float *pTbinArray2=tmp_array2;


/*const Int_t newbins=30;
  Double_t pTbinArray[newbins+1]; //-100,-80,-60,-40,-20,-10,-5,-3,-1,1,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,40,50,60,70,80,100
  pTbinArray[0]=-pTrange;
  Double_t width=20;
  for(int i=1; i<=newbins; i++){
	if(pTbinArray[i-1]==-20) width=10;
	else if(pTbinArray[i-1]==-10) width=5;
	else if(pTbinArray[i-1]==-5) width=2;
	else if(pTbinArray[i-1]==3) width=1;
	else if(pTbinArray[i-1]==10) width=2;
	else if(pTbinArray[i-1]==20) width=5;
	else if(pTbinArray[i-1]==30) width=10;
	else if(pTbinArray[i-1]==50) width=10;
	else if(pTbinArray[i-1]==80) width=20;
	pTbinArray[i]=pTbinArray[i-1]+width;
//if(pTbinArray[i]==pTrange)cout<<"lastbin:"<<i<<endl;
}*/
  

  /*const Int_t newbins2=18;
  Double_t pTbinArray2[newbins2+1]={0,1,3,5,7,8,10,12,14,16,20,25,30,40,50,60,70,80,100};*/
  

cout<<"binning choice:"<<binChoice<<endl;
for(int i=0; i<=newbins; i++){
cout<<pTbinArray[i]<<",";
}cout<<endl;
for(int i=0; i<=newbins2; i++){
cout<<pTbinArray2[i]<<",";
}
cout<<endl;
//return;


TString effsuf="";
if(efficorr)effsuf="_eff";

TString utype="Bayes";
if(doSVD) utype="SVD";
 
//I/O FILES
  str = Form("%s/histos_inclusivejet_R%.1lf.root", data_path.Data(),R);
  TFile *finput = new TFile(str.Data(), "OPEN");

str =Form("%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", out_dir.Data(), R, pTthresh); 
  TFile *fout = new TFile(str.Data(), "RECREATE");

//vectors for QA variables
const int vsize=niter;
TVectorD chi2change(vsize);
TVectorD chi2backf(vsize);
TVectorD chi2backf_trunk(vsize);


//MEASURED SPECTRUM    
  TH2D *h2 = (TH2D*)finput->Get("hpT_pTlead");
  Int_t firstbin = h2->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = h2->GetNbinsY();
  TH1D *htemp= h2->ProjectionX("htemp", firstbin, lastbin);
  Double_t binWidth=htemp->GetBinWidth(1);
  TH1D *hSignalSpectrum = new TH1D("hSignalSpectrum","measured data",newbins,pTbinArray); 
  hSignalSpectrum->Sumw2();
  for(Int_t binx = 1; binx <= htemp->GetNbinsX(); binx++){
	Double_t pT = htemp->GetBinCenter(binx);
	Double_t yield = htemp->GetBinContent(binx);
   Int_t newBin=hSignalSpectrum->FindBin(pT);
   Double_t newWidth=hSignalSpectrum->GetBinWidth(newBin);
	hSignalSpectrum->Fill(pT,yield);
  }
  delete htemp;
//error calculation
  for(Int_t binx = 1; binx <= hSignalSpectrum->GetNbinsX(); binx++){
  	  Double_t yield = hSignalSpectrum->GetBinContent(binx);
	  Double_t error = TMath::Sqrt(yield);
	  hSignalSpectrum->SetBinError(binx, error);
//cout<<"bin: "<<hSignalSpectrum->GetBinLowEdge(binx)<<" width: "<<hSignalSpectrum->GetBinWidth(binx)<<" yield: "<<yield<<endl;
  }
  Double_t int_signal=hSignalSpectrum->Integral();

//return;
//Efficiency correction  
TH1D* hepsilon;
if(efficorr){
str = Form("%s/epsilon_R%.1lf_pTlead%0.lf.root",epsilon_path.Data(),R,pTthresh);
TFile *fepsilon= new TFile(str.Data(), "OPEN");
hepsilon=(TH1D*) fepsilon->Get("hepsilon_unfolded");
//hepsilon=(TH1D*) fepsilon->Get("hepsilon_transposed");
}

  //RESPONSE MATRIX, PRIOR
  str = Form("%s/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", rmatrix_path.Data(),rmatrix_type.Data(),R,pTthresh,priorNo);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = new TH2D("hresponse","hresponse",newbins,pTbinArray,newbins2,pTbinArray2);
  TH2D *rmatrix_tmp = (TH2D*)frmatrix->Get("hResponse_1E9");
  TH1D *hMCreco = new TH1D("hmcreco","hmcreco",newbins,pTbinArray);
  TH1D *hMCreco_tmp = (TH1D*)frmatrix->Get("hMCreco_1E9"); //MC measured spectrum
  TH1D *hMCtrue = new TH1D("hmctrue","hmctrue",newbins2,pTbinArray2);
  TH1D *hMCtrue_tmp = (TH1D*)frmatrix->Get("hMCtrue_1E9"); //MC input spectrum

   rmatrix->Sumw2();
	hMCreco->Sumw2();
	hMCtrue->Sumw2();


 // rmatrix->Reset("MICE");
 // hMCreco->Reset("MICE");    
 // hMCtrue->Reset("MICE"); 

  Int_t nbinsx;
  Int_t nbinsy;
  nbinsx = rmatrix_tmp->GetNbinsX();
  nbinsy = rmatrix_tmp->GetNbinsY();

  rmatrix->GetYaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  Double_t binWidthx=rmatrix_tmp->GetXaxis()->GetBinWidth(1); 
  Double_t binWidthy=rmatrix_tmp->GetYaxis()->GetBinWidth(1); 
  for(Int_t binx = 1; binx <= nbinsx; binx++){
    for(Int_t biny = 1; biny <= nbinsy; biny++){
		Double_t yield = rmatrix_tmp->GetBinContent(binx, biny);
		Double_t pTx = rmatrix_tmp->GetBinCenter(binx);
		Double_t pTy = rmatrix_tmp->GetBinCenter(biny);
      Int_t newBinx=rmatrix->GetXaxis()->FindBin(pTx); 
      Int_t newBiny=rmatrix->GetYaxis()->FindBin(pTy);
		Double_t newWidthx=rmatrix->GetXaxis()->GetBinWidth(newBinx); 
		Double_t newWidthy=rmatrix->GetYaxis()->GetBinWidth(newBiny); 
		//yield=yield*(binWidthx/newWidthx)*(binWidthy/newWidthy);
		yield=yield;
      rmatrix->Fill(pTx,pTy,yield);
	}}
  delete rmatrix_tmp; 
//error calculation
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  for(Int_t binx = 1; binx <= nbinsx; binx++){ 
  	for(Int_t biny = 1; biny <= nbinsy; biny++){ 
		Double_t yield = rmatrix->GetBinContent(binx, biny); 
		Double_t error = TMath::Sqrt(yield);
		rmatrix->SetBinError(binx, biny, error);
	}}
  // PRIOR
  binWidth=hMCtrue_tmp->GetBinWidth(1);   
  for(Int_t bin = 1; bin <= hMCreco_tmp->GetNbinsX(); bin++)
    {
      Double_t pT = hMCtrue_tmp->GetBinCenter(bin);
      Double_t yield = hMCtrue_tmp->GetBinContent(bin);
		Int_t newBin=hMCtrue->FindBin(pT);  
		Double_t newWidth=hMCtrue->GetBinWidth(newBin);  
      hMCtrue->Fill(pT, yield);
    }
   delete hMCtrue_tmp;
//error calculation
	for(Int_t bin = 1; bin <= hMCtrue->GetNbinsX(); bin++) {
		Double_t yield = hMCtrue->GetBinContent(bin);
		Double_t errorp = TMath::Sqrt(yield);
   	hMCtrue->SetBinError(bin,errorp);
   }
	//hMCtrue->Sumw2();

  //SMEARED PRIOR
  binWidth=hMCreco_tmp->GetBinWidth(1);   
  for(Int_t bin = 1; bin <= hMCreco_tmp->GetNbinsX(); bin++)
    {
      Double_t pT = hMCreco_tmp->GetBinCenter(bin);
      Double_t yield = hMCreco_tmp->GetBinContent(bin);
		Int_t newBin=hMCreco->FindBin(pT);  
		Double_t newWidth=hMCreco->GetBinWidth(newBin);  
      hMCreco->Fill(pT, yield);
    }
   delete hMCreco_tmp;
//error calculation
	for(Int_t bin = 1; bin <= hMCreco->GetNbinsX(); bin++) {
		Double_t yield = hMCreco->GetBinContent(bin);
		Double_t errorp = TMath::Sqrt(yield);
   	hMCreco->SetBinError(bin,errorp);
   }
	//hMCreco->Sumw2();

//SAVE INPUT HISTOGRAMS
  fout->mkdir("input");
  fout->cd("input");

  hMCtrue->Write("hprior");
  rmatrix->Write("hresponse");
  //rmatrix->Write("PEC"); //for backward compatibility
  hSignalSpectrum->Write("hmeasured");

//UNFOLDING with RooUnfold
RooUnfoldResponse response (hMCreco,hMCtrue,rmatrix);
for(Int_t iteration=0; iteration<niter; iteration++)
{
cout<<"UNFOLDING, iteration/kterm: "<<iteration<<endl;
if(doSVD){RooUnfoldSvd unfold (&response, hSignalSpectrum, iteration+1);}
else{RooUnfoldBayes   unfoldb (&response, hSignalSpectrum, iteration+1, smooth);}
cout<<"WRITING OUTPUT"<<endl;

  //cout<<"integral"<<hReco->Integral()<<endl; 
  //unfold.PrintTable (cout, hSignalSpectrum); 
 
  //TH1D* hReco= (TH1D*) unfold.Hreco(kCovariance); //kCovariance specifies the error calculation method
     TH1D* hReco;
	if(doSVD) hReco= (TH1D*) unfold.Hreco(3); 
	else hReco= (TH1D*) unfoldb.Hreco(3); 

	if(doSVD) TH1D* hDvec=(TH1D*) unfold.GetDvec();
	//get Covariance Matrix
	if(!doSVD)TMatrixD covM=(TMatrixD) unfoldb.Ereco();
	else TMatrixD covM=(TMatrixD) unfold.Ereco();

	//TH2D* hCov=(TH2D*) rmatrix->Clone("hCov");
	//hCov->Reset("MICE");
	TH2D* hCov=new TH2D("hCov","hCov",newbins2,pTbinArray2,newbins2,pTbinArray2);
	for(Int_t i=0; i<newbins2; i++){
   for(Int_t j=0; j<newbins2; j++){
		hCov->SetBinContent(i+1, j+1, covM(i,j));
	}
	}

  fout->mkdir(Form("iter%d", iteration));
  fout->cd(Form("iter%d", iteration));

//chi2 of ratio of successive iterations
float chi2ch=0;
if(!doSVD)
   chi2ch=unfoldb.GetChi2Change();
chi2change[iteration]=chi2ch;

//BACKFOLDING
TH1D* hbackfold=(TH1D*) hSignalSpectrum->Clone(Form("hbackfold%i",iteration));
hbackfold->Reset("MICE");
hbackfold->SetTitle("backfolded distribution");
hbackfold->Sumw2();
for(int bin=1; bin<=hReco->GetNbinsX(); bin++)
{
	Double_t val=hReco->GetBinContent(bin);
	TH1D* prob=(TH1D*)rmatrix->ProjectionX("prob",bin,bin); //rmatrix has the same binning as hReco
	if(!prob->Integral()>0)continue;
	prob->Scale(1./prob->Integral());
	for(int ev=0; ev<val; ev++)
	{
		Double_t pTnew=prob->GetRandom();
		hbackfold->Fill(pTnew);
	}
/*	for(int binb=1; binb<=hReco->GetNbinsX(); binb++)
	{
		double prb=prob->GetBinContent(binb);
		//cout<<"prob: "<<prb<<" val: "<<val<<" pT: "<<hbackfold->GetBinCenter(binb)<<endl;
		hbackfold->Fill(hbackfold->GetBinCenter(binb),prb*val);	
	}*/
delete prob;
}
hbackfold->Write("hbackfolded");
TH1D* hbfmratio=(TH1D*) hbackfold->Clone(Form("hbfmratio%i",iteration));
hbfmratio->Divide(hSignalSpectrum);
hbfmratio->SetTitle("backfolded/measured");
hbfmratio->Write("hbfmratio");

//chi2/NDF of ratio backfolded/measured 
//------------------------------------------------
   Int_t ndf=0;
   Int_t ndf_trunk=0; 
   for(Int_t bin = 1; bin <= hbfmratio->GetNbinsX(); bin++)
   {
      if(hbfmratio->GetBinCenter(bin)>40)continue;
      Double_t yld = hbfmratio->GetBinContent(bin);
      Double_t error=hbfmratio->GetBinError(bin);
     
      if(yld>0){
         chi2backf[iteration]+=(yld-1.0)*(yld-1.0)/(error*error);
         ndf++;

         if(hbfmratio->GetBinCenter(bin)<10) continue;
         chi2backf_trunk[iteration]+=(yld-1.0)*(yld-1.0)/(error*error);
         ndf_trunk++; 
      } 
   }
   chi2backf[iteration]=chi2backf[iteration]/ndf;
   chi2backf_trunk[iteration]=chi2backf_trunk[iteration]/ndf_trunk;
//------------------------------------------------



	//EFFICIENCY CORRECTION
	if(efficorr){
	for(int bin=1; bin<=hReco->GetNbinsX(); bin++)
	{
		Double_t oldv=hReco->GetBinContent(bin);
		Int_t efbin=hepsilon->FindBin(hReco->GetBinCenter(bin));
		Double_t eff=hepsilon->GetBinContent(efbin);
		Double_t newv=oldv;
		if(eff>0)newv=oldv/eff;
		hReco->SetBinContent(bin,newv);
	}
	}
  hReco->Write("hunfolded"); 
  hCov->Write("hcovariance"); 
  if(doSVD) hDvec->Write("hdvec");
}//iterations

//save QA vectors
fout->mkdir("QA");
fout->cd("QA");
chi2change.Write("chi2change");
chi2backf.Write("chi2backf");
chi2backf_trunk.Write("chi2backf_trunk");

  fout->Close(); 
  delete fout; 
 
 
  timer.Stop();
  timer.Print();
}

