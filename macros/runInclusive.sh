#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage


#export CENTRALITY="60-80"
export CENTRALITY="0-10"

export OUTPUTDIR="../out_test"
export INPUTDIR="../out_test"
export TRIGGER=$TRG"11" # YEAR11: HT11, MB11
export TRIGGER="All"
export RPARAM=0.4
export ACUT=0.4 #jet area cut
export MAXRAP=1.0
#export REFMULTMIN=10 # YEAR 11 DATA, 60-80%
#export REFMULTMAX=31 # YEAR 11 DATA, 60-80%
export REFMULTMIN=396 # YEAR 11 DATA, 0-10%
export REFMULTMAX=10000 # YEAR 11 DATA, 0-10%
export ZVERTEX=30
export DOEFFICORR=0 #?
export CHARGED=1
export NJETSREMOVE=2
export NFILES=1
ALEXPICO=0 #Alex's or my picodst

NEVENTS=1000 #number of events
N=1 #take every Nth batch
MAX=1 #number of input files

if [ $ALEXPICO -eq 1 ]; then
   FILELISTFILE="$HOME/jet_analysis/run11/alex_${CENTRALITY}.list"
   SCRIPTNAME="runInclusiveAlex.C"
else
   FILELISTFILE="$HOME/jet_analysis/run11/xrootd_${TRG}.list"
   SCRIPTNAME="runInclusive.C"
fi
export FILELISTFILE
#LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
#let MAX=`expr $LINES + 1`
#echo "MAX set to: $MAX"
let ADD=$NFILES*$N 
for ((FIRST=0; FIRST < MAX; FIRST += $ADD))
  do
  export FIRST
export FIRST=0
export NSKIPFILES=10
#export OUTPUTDIR="${BASEDIR}/${OUTDIR}/inclusive_${TRIGGER}_year11_${TYPE}/${FIRST}"
#export INPUTDIR="${BASEDIR}/${OUTDIR}/inclusive_${TRIGGER}_year11_${TYPE}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
	mkdir -p $OUTPUTDIR
  fi

root -l ${SCRIPTNAME}\($NEVENTS\) -q
#root -l runAnaInclusive.C -q

done
