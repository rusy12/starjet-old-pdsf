#!/bin/bash
_usage() {
    echo "Usage: $0 [HT,MB] [local|xrootd]"
    exit 1
}

TRIG=$1
STYPE=$2
# Check args
[ -n "$TRIG" ] || _usage
[ -n "$STYPE" ] || _usage

export OUTPUTDIR="../out_test/$STYPE"
export TRIGGER=$TRIG"11" #run10 central: All | run11: MB11, HT11

if [ "$STYPE" = "local" ]; then
	export FILELISTFILE="$HOME/jet_analysis/run11/checklists/"$TRIG"_run11_tmp.list"
else
	export FILELISTFILE="$HOME/xrootd_scripts/xrootd_"$TRIG"_tmp.list"
fi

LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"

export NFILES=$MAX
export NSKIPFILES=0

mkdir -p $OUTPUTDIR

root -l 'runEventCounter.C' -q
