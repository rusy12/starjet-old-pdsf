#!/bin/bash

export OUTPUTDIR="../out_test"
export INPUTDIR="../out_test"
export TRIGGER="MB11" #run10 central: All | run11: MB11, HT11
export FILELISTFILE="$HOME/jet_analysis/run11/xrootd_MB.list"
export REFMULT=366
export NFILES=1
export NSKIPFILES=10

mkdir -p $OUTPUTDIR

#root -l 'runQA.C(100)' -q
root -l 'runQA.C' -q
