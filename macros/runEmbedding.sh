#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

export OUTPUTDIR="../out_test"
export INPUTDIR="../out_test"
export TRIGGER=$TRG"11" # YEAR11: HT11, MB11
export RPARAM=0.2
export ACUT=0.09 #jet area cut
export MAXRAP=1.0
export REFMULTMIN=10 # YEAR 11 DATA, 50-80%
export REFMULTMAX=76 # YEAR 11 DATA, 50-80%
export FILELISTFILE="$HOME/jet_analysis/run11/xrootd_"$TRG".list"
#export FILELISTFILE="$HOME/jet_analysis/run10/run10_filelists/project_area_tmp.list"
export NFILES=1
export NSKIPFILES=50
export PTEMBFIXED=1 # 1 - probe pT fixed, 0 - probe pT in an interval
export CHARGED=1

mkdir -p $OUTPUTDIR

#root -l -q 'runEmbedding.C(1000)'
root -l -q runAnaEmbedding.C 
