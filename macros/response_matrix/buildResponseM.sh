#!/bin/bash
TRG="MB"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=0 # correct delta pT for event plane bias
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #path to correction histograms
export CENTRAL=1

if [ $CENTRAL -eq 1 ]; then
SUFF=""
else
SUFF="_peripheral"
fi

for RPARAM in 0.2 0.3 0.4
do
	export RPARAM
   export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/$TRG/embedding_R${RPARAM}$SUFF"
for PTLEAD in 5 7
do
   export PTLEAD
	root -l buildResponseM.C -q -b
done
done
