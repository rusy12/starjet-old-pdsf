void multiply_matrix()
{
Float_t pTlead = atof(gSystem->Getenv("PTTHRESH"));
Float_t R=atof(gSystem->Getenv("RPARAM"));
Int_t reverse=atoi(gSystem->Getenv("REVERSE"));
TString jettype=gSystem->Getenv("JETTYPE");
TString suffix=gSystem->Getenv("CSUFFIX");

cout<<"R: "<<R<<endl;
cout<<"pTlead: "<<pTlead<<endl;

TString str;
TString data_path = Form("/global/homes/r/rusnak/jet_analysis/STARJet/out/MB/embedding_R%.1lf%s",R,suffix.Data());
TString outdir=data_path;
str = Form("%s/rmatrix/response_matrix_BG_sp_R%.1lf_pTlead%.0lf.root", data_path.Data(),R,pTlead);
cout<<"input1: "<<str<<endl;
TFile *finput1 = new TFile(str.Data(), "OPEN");
TH2D* hmatrix1=(TH2D*) finput1->Get("hResponse_1E9");
//TH2D* hmatrix1=(TH2D*) finput->Get(Form("hdpT2D_%s",type.Data()));
TString data_path2 = Form("/global/homes/r/rusnak/jet_analysis/toymodel/DataOut/%s/jetonly/pyEmb_R%.1lf%s",jettype.Data(),R,suffix.Data());
str = Form("%s/pythia_emb_R%.1lf.root", data_path2.Data(),R);
cout<<"input2: "<<str<<endl;
//str = Form("%s/histos_deltapT_R%.1lf.root", data_path.Data(),R);
TFile *finput2 = new TFile(str.Data(), "OPEN");
TH2D* hmatrix2=(TH2D*)finput2->Get(Form("hresponse_pTl%.0lf",pTlead));
//TH2D* hmatrix2=(TH2D*) finput2->Get("hdpT2D_dete");


TString outfile=Form("%s/rmatrix/response_matrix_BGD_R%.1lf_pTlead%.0lf.root",outdir.Data(),R,pTlead);
TFile* fout = new TFile(outfile.Data(), "recreate");

TH2D *hmatrix_out=(TH2D*) hmatrix2->Clone("hmatrix_out");
hmatrix_out->Reset("MICE");
Int_t nbins=hmatrix1->GetNbinsX();

TMatrixD matrix1(nbins,nbins);
TMatrixD matrix2(nbins,nbins);
TMatrixD matrixo(nbins,nbins);

cout<<"filling vectors"<<endl;
for(Int_t i=0; i<nbins; i++){
for(Int_t j=0; j<nbins; j++){
 matrix1(i,j)=hmatrix1->GetBinContent(i + 1, j + 1);
 matrix2(i,j)=hmatrix2->GetBinContent(i + 1, j + 1);
}
}

if(!reverse)
{
	cout<<"multiplying BGxdete"<<endl;
	matrixo.Mult(matrix1,matrix2);
}
else 
{
	cout<<"multiplying detexBG"<<endl;
	matrixo.Mult(matrix2,matrix1);
}
for(Int_t i=0; i<nbins; i++){
for(Int_t j=0; j<nbins; j++){
hmatrix_out->SetBinContent(i+1, j+1, matrixo(i,j));
}
}

cout<<"writing output"<<endl;
hmatrix_out->Write("hResponse_1E9");
//hmatrix_out->Write("hdpT2D_BG_dete");

finput1->Close();
finput2->Close();
fout->Close();
}

