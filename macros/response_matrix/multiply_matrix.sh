#!/bin/bash
export JETTYPE="pythia"
export REVERSE=0 #0: BGxDete 1: DetexBG
CENTRAL=1 #central or peripheral collisions

if [ $CENTRAL -eq 1 ]; then
CSUFFIX=""
PTLEADCUTS="5 7"
else
CSUFFIX="_peripheral"
PTLEADCUTS="3 4 5"
fi
export CSUFFIX

for RPARAM in 0.2 0.3 0.4
do
export RPARAM
for PTTHRESH in `echo $PTLEADCUTS` 
   do
   export PTTHRESH
   root -l multiply_matrix.C -q
done
done
