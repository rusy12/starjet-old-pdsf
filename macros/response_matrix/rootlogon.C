{
  Int_t ierr = 0;

  gSystem->Load("$ROOTUTILS/libROOTUTIL.so");
  gSystem->AddIncludePath("-I$HOME/jet_analysis/STARJet/Unfolding");
  gSystem->Load("../../Unfolding/libUnfold.so");

  char *clibs[] =
    {
      "$ROOTSYS/lib/libPhysics.so",
      "$ROOTSYS/lib/libRIO.so",
      "$ROOTSYS/lib/libHist.so",
      "$ROOTSYS/lib/libEG.so",
      "$ROOTSYS/lib/libTree.so",
      
      "$PYTHIA6/libPythia6.so",
      "$ROOTSYS/lib/libEGPythia6.so",

      //"$CGAL_LIB_DIR/libCGAL.so", // no CGAL on PDSF
      "$FASTJETDIR/lib/libfastjet.so",
      //"$FASTJETDIR/lib/libsiscone.so",
      //"$FASTJETDIR/lib/libSISConePlugin.so",
      //"$PYTHIA8DIR/lib/libpythia8.so",
      //"$PYTHIA8GEN/libTPythia8.so",
      "$JETPICODIR/libTStarJetPico.so",

      "$FJWRAPPER/libFJWRAPPER.so",
      "$STARJETBASEDIR/analysis/libSJetRun.so",
      0
    };

  TUtils::LoadLibs(clibs, kFALSE);

  char *cmacros[] = 
    {
      0
    };

  Int_t i = 0;
  while (cmacros[i++] != 0)
    {
      TString cmacro;
      cmacro += cmacros[i-1];
      ierr = gROOT->LoadMacro(cmacro.Data());
      if (ierr != 0)
	{
	  cerr <<  "Unable to load macro " << cmacro.Data() << endl;
	}
    }


  //gSystem->Load("libEG");
    //gSystem->Load("$ROOTSYS/../pythia6/libPythia6"); //change to your setup

  //gStyle->SetFrameFillColor(10);
  //gStyle->SetTitleColor(1);
  //gStyle->SetAxisColor(1, "X");
  //gStyle->SetAxisColor(1, "Y");
  //gStyle->SetAxisColor(1, "Z");
  
  //gStyle->SetTitleTextColor(1);

  gStyle->SetPalette(1);
  //gStyle->SetPadColor(10);
  //gStyle->SetFillColor(10);
  gStyle->SetFrameFillColor(10);
  gStyle->SetTitleFillColor(10);
  gStyle->SetStatColor(10);
}
