{
Float_t R=atof(gSystem->Getenv("RPARAM"));
Float_t pTlead=atof(gSystem->Getenv("PTTHRESH"));
Float_t prior=atoi(gSystem->Getenv("PRIOR"));
TString rmatrix_type = gSystem->Getenv("RMATRIX_TYPE");
TString path = gSystem->Getenv("PATH_TO_DELTA_PT_HISTOGRAMS");

UnfoldBuildResponseMatrixSVD *rmatrix = new UnfoldBuildResponseMatrixSVD(path, R, pTlead, prior, rmatrix_type);
rmatrix->BuildDeltaPtResponseMatrix();
}
