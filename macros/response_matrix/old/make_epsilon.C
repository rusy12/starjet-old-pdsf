void make_epsilon(){
float pTlead = atof(gSystem->Getenv("PTTHRESH"));
Float_t R=atof(gSystem->Getenv("RPARAM"));

cout<<"R: "<<R<<endl;
cout<<"pTlead: "<<pTlead<<endl;

Double_t pTmin=0;
Double_t pTmax=100;

const Int_t nemb =16;
Double_t fEmbPt[] ={0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 8.0, 10.0, 15.0, 20.0, 30.0, 40.0, 50.0, 70.0, 90.0};


//TString data_path=Form("/eliza17/star/pwg/starjetc/rusnak/jets/run11_AuAu/MB/embedding_R%.1lf",R);
TString data_path=Form("/eliza17/star/pwg/starjetc/rusnak/jets/run11_AuAu/Pythia/pythia_charged_R%.1lf",R);

TString out_path=Form("/eliza17/star/pwg/starjetc/rusnak/jets/run11_AuAu/MB/embedding_R%.1lf/rmatrix",R);
TString outdir=out_path;

//TString infile=Form("histos_embeddedjet_R%.1lf.root",R);
TString infile=Form("histos_pythiajet_R%.1lf.root",R);
TString str=Form("%s/%s",data_path.Data(),infile.Data());
TFile *fepsilon = new TFile(str.Data(), "OPEN");
//TH1D* inhisto=fepsilon->Get(Form("heffi_BG_dete_%0.lf",pTlead));
TH1D* inhisto=fepsilon->Get(Form("heffi_pTl%.0lf",pTlead));

TString outfile=Form("%s/epsilon_R%.1lf_pTlead%0.lf.root",outdir.Data(),R,pTlead);
//TString outfile=Form("%s/epsilon_R%.1lf.root",outdir.Data(),R);
TFile* fout = new TFile(outfile.Data(), "recreate");
TH1D* outhisto=new TH1D("outhisto","efficiency",400,pTmin,pTmax);

Int_t nbins=inhisto->GetNbinsX();
Int_t bin0=inhisto->FindBin(pTlead);
for(Int_t bin=bin0; bin<nbins; bin++){
Double_t pT = inhisto->GetBinCenter(bin);
Double_t con1 = inhisto->GetBinContent(bin);
Int_t bin2=inhisto->FindBin(-pT);
Double_t con2 = inhisto->GetBinContent(bin2);
Double_t epsilon=0;
if(con2>0) epsilon=con1/con2;
//epsilon=epsilon+0.05;
//if(epsilon>1) epsilon=1;
//if(pT>70)epsilon=0.95;
outhisto->Fill(pT,epsilon); 
//cout<<epsilon<<endl;
}//bin loop

nbins=outhisto->GetNbinsX();
bin0=outhisto->FindBin(65);
Double_t aver=0;
for(Int_t bin=bin0-4; bin<bin0; bin++){
aver+=outhisto->GetBinContent(bin);
}
aver=aver/4.0;

for(Int_t bin=bin0; bin<nbins; bin++){
outhisto->SetBinContent(bin,aver);
}

fout->cd();
outhisto->Write("hepsilon");

fepsilon->Close();
fout->Close();
}
