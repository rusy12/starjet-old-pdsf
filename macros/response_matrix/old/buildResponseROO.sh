#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE (BG_sp | BG_pyt | dete | BG_dete)" 
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"

RMATRIX_TYPE=$2  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

export RPARAM=0.4
export RMATRIX_TYPE

export PATH_TO_DELTA_PT_HISTOGRAMS="/home/rusnak/jet_analysis/STARJet/out/${TRG}/embedding_R${RPARAM}"

for PTTHRESH in 5 #7
do
   export PTTHRESH
	#root -l make_epsilon.C -q -b
	for PRIOR in 2 #5 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-7) 6:(pT-2)^(-6)
	do
		export PRIOR
		root -l buildResponseROO.C -q -b
	done
done
