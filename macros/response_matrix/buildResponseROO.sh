#!/bin/bash
TRG="MB"
export RPARAM=0.3
export PTTHRESH=5
export RMATRIX_TYPE="BGD" #deltapT distribution: BG_sp|inplane|outplane
BASEDIR="$HOME/jet_analysis/STARJet"
export RM_PATH="$BASEDIR/out/${TRG}/embedding_R${RPARAM}/rmatrix"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}"
export PRIOR_PATH="$HOME/jet_analysis/STARJet/out/prior"
export PRIOR=2


	root -l buildResponseROO.C -q -b
