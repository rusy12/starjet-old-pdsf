#!/bin/bash

for PTHARD in  "30pThard35" #"3pThard4" "4pThard5" "5pThard7" "7pThard9" "9pThard11" "11pThard15" "15pThard20" "20pThard25" "25pThard30" "30pThard35" "35pThard40" "40pThard50" "50pThard60" "60pThard-1"
do
SET=1
export OUTPUTDIR="$HOME/jet_analysis/STARJet/out/test"
export INPUTDIR="$HOME/jet_analysis/pythia/"${PTHARD[$PTBIN]}"/"${SET}
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency"
export RPARAM=0.3
export ACUT=0.2 #jet area cut | 0.09 | 0.2 | 0.4
export MAXRAP=1.0
export CHARGED=1
export DETE_JETS_ONLY=1 #save only detector level jets 
export EFFICORR=1 #apply efficiency correction (for detector level jets)
export PTSMEAR=1 #simulate track pT smearing (due to detector resolution) (for detector level jets)

export DPTSMEAR=0 #smear jets with delta-pT
export V2CORR=0 #use v2-corrected delta-pT histograms
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #pah to v2-correction coefficients
export CENTRAL=0 #central|peripheral events (for delta-pT selection)
export PTLEAD=3 #pTleading cut (for delta-pT selection)

if [ $CENTRAL -eq 1 ]; then
SUFF=""
else
SUFF="_peripheral"
fi
export DPTPATH="$HOME/jet_analysis/STARJet/out/MB/embedding_R${RPARAM}$SUFF" #path to delta-pT histograms

NEVENTS=10000

#mkdir -p $OUTPUTDIR

root -q -b -l runPythia.C\($NEVENTS\) 
#root -q -b -l runPythia.C
#root -l runAnaPythia.C -q
done
