#ifndef __ana_embedding__hh
#define __ana_embedding__hh

bool is_inside(Double_t eta1, Double_t eta2, Double_t phi1, Double_t phi2, Double_t R);
void ana_embedding(Long64_t nev = -1);

#endif
