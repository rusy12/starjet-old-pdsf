#include "PySimSimulationHeader.h"

ClassImp(PySimSimulationHeader)

//_____________________________________________________________________________
PySimSimulationHeader::PySimSimulationHeader()
{
  // DEFAULT CONSTRUCTOR
}

//_____________________________________________________________________________
PySimSimulationHeader::PySimSimulationHeader(Int_t nevents, Int_t ntrials, Double_t xsection)
{
  fnevents = nevents;
  fntrials = ntrials;
  fxsection = xsection;
}

//_____________________________________________________________________________
PySimSimulationHeader::~PySimSimulationHeader()
{
}
