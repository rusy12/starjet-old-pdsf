#include <TSystem.h>
#include <TFile.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TChain.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "StMemStat.h"
#include "jet.h"
#include "fjwrapper.h"
#include "utils.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

//=============================================================================
int DeltaPtIndex(Double_t pT)
{
  Float_t fEmbPt[] = {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 8.0, 10.0, 15.0, 20.0, 30.0, 40.0, 50.0, 70.0, 90.0};
	const int nEmb=16;
   int index = 0;
  if (pT <= fEmbPt[0])
    index=0;
  else if(pT>fEmbPt[nEmb-1]) 
 	 index=nEmb-1; 
  else 
  for(int i=0; i<(nEmb-1); i++){
   if(pT>fEmbPt[i] && pT<=fEmbPt[i+1]){
   Double_t rnd=gRandom->Uniform(fEmbPt[i],fEmbPt[i+1]);
   if(pT>rnd) index=i+1;
   else index=i;
   }
  }
	
  return index;
}

//=============================================================================
void pythia(Long64_t nev)
{
//load parameters----------------------------------
TString dataDir = gSystem->Getenv("INPUTDIR");
TString outDir = gSystem->Getenv("OUTPUTDIR");
Float_t r = atof(gSystem->Getenv("RPARAM"));
Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
Float_t fpTcut=0.2;
Int_t dete_jets_only=atoi(gSystem->Getenv("DETE_JETS_ONLY")); //save only detector level jets
Int_t efficorr=atoi(gSystem->Getenv("EFFICORR")); //apply efficiency correction cuts (for detector level jets)
TString eff_path = gSystem->Getenv("EFFPATH");
Int_t pTsmear=atoi(gSystem->Getenv("PTSMEAR")); //apply track pT smearing (for detector level)

bool dpTsmear=atoi(gSystem->Getenv("DPTSMEAR")); //smear jets with delta-pT
TString dpTpath=gSystem->Getenv("DPTPATH"); //path to delta-pT histograms
bool kv2corr=atoi(gSystem->Getenv("V2CORR")); //use v2-corrected delta-pT histograms
TString v2path=gSystem->Getenv("V2PATH"); //pah to v2-correction coefficients
bool kCentral=atoi(gSystem->Getenv("CENTRAL"));//central|peripheral events (for delta-pT selection)
Float_t pTleading=atof(gSystem->Getenv("PTLEAD"));//pTleading cut (for delta-pT selection)
//-------------------------------------------------
//cout << Form("[i] Output=%s",outDir.Data()) << endl;
//cout << Form("[i] R=%1.1f", r) << endl;
//-------------------------------------------------

Double32_t rho = 0;     //[0, 0, 16]
Double32_t sigma = 0;   //[0, 0, 16]
TClonesArray *pythiaarr = new TClonesArray("jet", 10);
TClonesArray *fpartarr=new TClonesArray("TParticle",1000);

StMemStat memstat;
memstat.Start();

cout<<"Memory used: "<<memstat.Used()<<endl;

//OUTPUT
TString str = "null";
str = Form("%s/pythia_R%.1lf.root", outDir.Data(), r);
if(dete_jets_only)
str = Form("%s/pythia_R%.1lf_eff.root", outDir.Data(), r);
TFile *foutput = new TFile(str.Data(), "RECREATE");
TTree* ftreejets = new TTree("PythiaJets","Pythia Jets");
TBranch *br_rho = ftreejets->Branch("rho", &rho);
TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
TBranch *br_pythia = ftreejets->Branch("akt_pythia", &pythiaarr);

//INPUT
str = Form("%s/pythia.root", dataDir.Data());
cout<<"[i] Input file: "<<str<<endl;
TFile *finput = new TFile(str.Data(), "OPEN");
TTree *ftreepyt = (TTree*)finput->Get("event");
ftreepyt->SetBranchAddress("particles", &fpartarr);

//Tracking efficiency
/*
TFile* efffile = new TFile(Form("%s/eff_pp.root",eff_path.Data()),"OPEN");
TF1* effL = (TF1*)efffile->Get("effhL");
TF1* effH = (TF1*)efffile->Get("effhH");
*/
//control histograms
TH2D* hNconstPtl0=new TH2D("hNconstPtl0", "# of jet const. vs jet pT, p_{T}^{lead}>0; n constituents; p_{T} GeV",20,0.5,20.5,100,0,50); 
TH2D* hNconstPtl5=new TH2D("hNconstPtl5", "# of jet const. vs jet pT, p_{T}^{lead}>5; n constituents; p_{T} GeV",20,0.5,20.5,100,0,50); 
TH2D* hpTlFracPtl0=new TH2D("hpTlFracPtl0", "p_{T}^{lead}/p_{T}^{jet} vs p_{T}^{jet}, p_{T}^{lead}>0; p_{T}^{lead}/p_{T}^{jet}; p_{T} GeV",20,0.1,1.1,100,0,50);
TH2D* hpTlFracPtl5=new TH2D("hpTlFracPtl5", "p_{T}^{lead}/p_{T}^{jet} vs p_{T}^{jet}, p_{T}^{lead}>5; p_{T}^{lead}/p_{T}^{jet}; p_{T} GeV",20,0.1,1.1,100,0,50);
TH1D* htrackpT=new TH1D("htrackpT", "htrackpT",50,0,50);

TH1D* hNjets=new TH1D("hnJets","# of jets per pT",250,0.0,50.0);
TH2D* hFF=new TH2D("hz_pT","jet FF vs jet pT",51,0,1.02, 250,0.0,50.0);

//Load delta-pT histograms for dpT-smearing
//--------------------------------------------------------------
	TH1D* hdpT[16];
	TH2D* hv2;
if(dpTsmear){
	str=Form("%s/histos_embeddedjet_R%.1lf.root", dpTpath.Data(),r);
  	finput = new TFile(str.Data(), "OPEN");

  if(kv2corr){
  str=Form("%s/v2corr.root", v2path.Data()); //file with v2 correction factors for delta-pT
  TFile* fv2=new TFile(str.Data(), "OPEN");
  str=Form("dpt_v2corrVSuncorr_R0%.0lf",r*10);
  if(kCentral) str+="_central";
   else str+="_peripheral";
   hv2=(TH2D*)fv2->Get(str.Data());
	}

  Float_t pTemb[] =  {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 8.0, 10.0, 15.0, 20.0, 30.0, 40.0, 50.0, 70.0, 90.0};
int nEmb=16;
  for(Int_t idist = 0; idist < nEmb; idist++)
    {
      TString name = Form("delta_pt_BG_sp_%.0lf", pTleading);
      TH2D* htmp= (TH2D*)finput->Get(name.Data());

      name=Form("dpt_pTl%.0lf_emb%i",pTleading,idist);
      Int_t firstbin=htmp->GetXaxis()->FindBin(pTemb[idist]);
      Int_t lastbin=firstbin;
      hdpT[idist] = (TH1D*)htmp->ProjectionY(name,firstbin,lastbin);
      delete htmp;
      // removing 1 +/- 1
      for(Int_t bin = 1; bin <= 2000; bin++)
	   if(hdpT[idist]->GetBinContent(bin) == hdpT[idist]->GetBinError(bin))
     {
       hdpT[idist]->SetBinContent(bin, 0);
       hdpT[idist]->SetBinError(bin, 0);
     }

//v2 correction from Alex
      if(kv2corr){
      for(Int_t bin = 1; bin <= 2000; bin++)
      {
         Float_t pTreco=hdpT[idist]->GetBinCenter(bin);
         Float_t prob=hdpT[idist]->GetBinContent(bin);
         if(!prob>0)continue;
         Float_t prob_err=hdpT[idist]->GetBinError(bin);
         Int_t binx=hv2->GetXaxis()->FindBin(pTemb[idist]);
         Int_t biny=hv2->GetYaxis()->FindBin(pTreco);
         Float_t scaler=hv2->GetBinContent(binx,biny);
         if(!scaler>0)scaler=1;
         hdpT[idist]->SetBinContent(bin,prob*scaler);
         hdpT[idist]->SetBinError(bin,prob_err*scaler);
      }
      }//v2 corr
    }//pTemb loop
}//load delta-pT histograms
//--------------------------------------------------------------

cout<<"Memory used II: "<<memstat.Used()<<endl;

//EVENT LOOP 
//Int_t evt = 0;
//Int_t njets=0;
Int_t nevts = ftreepyt->GetEntries();
if(nevts>nev && nev>0)nevts=nev;
cout<<"Run set for "<<nevts<<" events"<<endl;
for(Int_t ievt = 0; ievt < nevts; ievt++)
{
if(ievt%1000==0){
	cout<<"filling event "<<ievt<<endl;
	cout<<"Memory used: "<<memstat.Used()<<endl;}
ftreepyt->GetEntry(ievt);

std::vector<fastjet::PseudoJet> input_vector;
std::vector<fastjet::PseudoJet> input_vector_dete;
Int_t nparticles = fpartarr->GetEntries();


//PARTICLE LOOP
//Double_t part=0;
//Double_t part_eff=0;

TLorentzVector partlv;
for(Int_t ipart = 0; ipart < nparticles; ipart++)
{
TParticle *particle = (TParticle*)fpartarr->At(ipart);
Int_t pdg = TMath::Abs(particle->GetPdgCode());

if(pdg == 12 || pdg == 14 || pdg == 16 || pdg == 18 ) continue; //undetectable particles
//pdg == 2112 || pdg == 130 

if(charged){
 Double_t chrg = particle->GetPDG()->Charge();
              if(!chrg) continue;
			//if(pdg==22) continue; //neutral particles
		}
      particle->Momentum(partlv);

      if(partlv.Pt() < fpTcut) continue;

      if(TMath::Abs(partlv.Eta()) > max_rap) continue;
		
		Double_t px,py,pz,E;
		px=partlv.Px();
		py=partlv.Py();
		pz=partlv.Pz();
		E=partlv.Energy();

      fastjet::PseudoJet inp_particle(px,py,pz,E); 
//	cout<<"Memory used 2: "<<memstat.Used()<<endl;

	   
		htrackpT->Fill(partlv.Pt());
		input_vector.push_back(inp_particle);

//apply detector effects
if(dete_jets_only && pTsmear) //momentum smearing - but only in case we want to save only detector level jet, otherwise particle level jet will be saved and this step will be skiped now and done later
		{
				Double_t p=TMath::Sqrt(px*px+py*py+pz*pz);
				Double_t gsigma = 0.01*p*p;
				Double_t pnew = gRandom->Gaus(p,gsigma);
				//cout<<"old "<<ptp<<" new "<<ptn<<endl;

				Double_t scl=1;
				if(p>0)scl=pnew/p;

				px=px*scl;
				py=py*scl;
				pz=pz*scl;
				E=TMath::Sqrt(E*E-(1-scl*scl)*(px*px+py*py+pz*pz));
				partlv.SetPxPyPzE(px,py,pz,E);
		}



		if(dete_jets_only && efficorr){ //applying tracking efficiency - but only in case we want to save only detector level jet, otherwise particle level jet will be saved and this step will be skiped now and done later
				  //Double_t epsilon=efficiency11(partlv.Pt(), effL, effH); //efficiency function from utils.h
				  Double_t epsilon=efficiencyAlex(partlv.Pt(),0, kCentral); //efficiency function from utils.h
				  Double_t rnd=gRandom->Uniform(0,1);
				  //cout<<"rnd "<<rnd<<"epsilon "<<epsilon<<endl;
				  if(rnd>epsilon)continue;
		}
		input_vector_dete.push_back(inp_particle);

//		delete particle;
    }
	//end of particel loop
      fpartarr->Delete();
      pythiaarr->Delete();
      
      if (input_vector.size() <= 0) continue;

	//cout<<"Memory used 3: "<<memstat.Used()<<endl;
      FJWrapper kt_data;
      kt_data.r = r;
      kt_data.maxrap = max_rap;
      kt_data.algor = fastjet::kt_algorithm;
      kt_data.input_particles = input_vector;
      kt_data.Run();

      kt_data.GetMedianAndSigma(rho, sigma);
      
	//cout<<"Memory used 4: "<<memstat.Used()<<endl;
      FJWrapper akt_data;
      akt_data.r = r;
      akt_data.maxrap = max_rap;
      akt_data.algor = fastjet::antikt_algorithm;
      akt_data.input_particles = input_vector;
      akt_data.Run();


      // saving jets
      Int_t goodjet = 0;
      //TLorentzVector jetlv(0, 0, 0, 0);

	//cout<<"Memory used 5: "<<memstat.Used()<<endl;
      std::vector<fastjet::PseudoJet> jets = akt_data.inclusive_jets;
      for(Int_t ijet = 0; ijet < (Int_t)jets.size(); ijet++)
      //std::vector<fastjet::PseudoJet> jets = sorted_by_pt(akt_data.inclusive_jets);
      //for(Int_t ijet = 0; ijet < 1; ijet++)//take only highest pT jets
	    {
		  Double_t jphi = jets[ijet].phi();
		  Double_t jeta = jets[ijet].eta();
		  Double_t jpT = jets[ijet].perp();
	 	  Double_t jM = jets[ijet].m();
		  Double_t area = akt_data.clust_seq->area(jets[ijet]);

	  if(TMath::Abs(jeta) > akt_data.maxrap - r) continue;

	  Int_t N = akt_data.clust_seq->constituents(jets[ijet]).size();
	      
	  std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[ijet]));

	  new ((*pythiaarr)[goodjet]) jet();
	  jet *pythiajet = (jet*)pythiaarr->At(goodjet);

	  pythiajet->jet_fv.SetPtEtaPhiM(jpT, jeta, jphi, jM);
	  pythiajet->Nconst = N;
	  pythiajet->area = area;
	  pythiajet->pTleading = constituents[0].perp();

		//fill histos
		hNconstPtl0->Fill(N,jpT);
		hpTlFracPtl0->Fill(constituents[0].perp()/jpT,jpT);
		if(constituents[0].perp()>5)
		{
			hNconstPtl5->Fill(N,jpT);
	      hpTlFracPtl5->Fill(constituents[0].perp()/jpT,jpT);
		}
  
//			if(constituents[0].perp()>5 && jpT>5.0 && jpT<5.25)
//njets++;

      //calculate jet pT and pTleading after efficiency corrections as well
		TLorentzVector jetlv;
		Double_t pTlead_eff=0;

		hNjets->Fill(jpT);	

		//Loop over jet constituents
		for(int con=0; con<N; con++){
			fastjet::PseudoJet part=constituents[con];
			Double_t ptp = part.perp();

			//fill fragmentation function histo
			hFF->Fill(ptp/jpT,jpT,ptp/jpT);			
			

			//if(constituents[0].perp()>5 && jpT>5.0 && jpT<5.25)
//cout<<"particle: "<<con+1<<"/"<<N<<" pT: "<<ptp<<endl;
		   if(pTsmear)
			{
				Double_t gsigma = 0.01*ptp*ptp;
				Double_t ptn = gRandom->Gaus(ptp,gsigma);
				//cout<<"old "<<ptp<<" new "<<ptn<<endl;
				ptp=ptn;
			}
       	//Double_t epsilon=efficiency11(ptp, effL, effH); //efficiency function from utils.h
			Double_t epsilon=efficiencyAlex(partlv.Pt(), 0, kCentral); //efficiency function from utils.h
			//epsilon=0.80;
			Double_t rnd=gRandom->Uniform(0,1);
			//cout<<"rnd "<<rnd<<"epsilon "<<epsilon<<endl;
			if(efficorr && rnd>epsilon)continue;
			
		   TLorentzVector partlv;
			partlv.SetPtEtaPhiM(ptp,part.eta(),part.phi(),0);		
			if(partlv.Pt()>pTlead_eff)
				pTlead_eff=partlv.Pt();
			jetlv+=partlv;
		}//constituents loop

		//deltapT smearing
		Double_t pTdete=jetlv.Pt();
		if(dpTsmear)
		{
			int indx=DeltaPtIndex(pTdete);
			Double_t dpT=hdpT[indx]->GetRandom();
			pTdete=pTdete+dpT;
			//if(jetlv.Pt()>0)pTlead_eff=pTlead_eff*(pTdete/jetlv.Pt());
		//cout<<"pTold: "<<jetlv.Pt()<<" index: "<<indx<<" pT new: "<<pTdete<<endl;	
		}
		pythiajet->pTDete=pTdete;
		pythiajet->phiDete=jetlv.Phi();	  
		if(jetlv.Pt()>0)
			pythiajet->etaDete=jetlv.Eta();	  
		else
			pythiajet->etaDete=jeta;
      pythiajet->pTleadingDete=pTlead_eff; 
	  goodjet++;
	}//jet loop
	//cout<<"Memory used 6: "<<memstat.Used()<<endl;
      
      foutput->cd();
      ftreejets->Fill();

    }
//end of event loop

  foutput->cd();
  ftreejets->Write();
		hNconstPtl5->Write();
      hpTlFracPtl5->Write();
		hNconstPtl0->Write();
      hpTlFracPtl0->Write();
		htrackpT->Write();
		hFF->Write("hz_pT");
		hNjets->Write("hnJets");
  foutput->Close();
memstat.Stop();

}
