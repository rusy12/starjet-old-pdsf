#ifndef __prior_maker_hh
#define __prior_maker_hh

#include "TObject.h"
#include "Rtypes.h"
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

class prior_maker : public TObject
 {

	public:
	prior_maker();
	virtual ~prior_maker(); 
	void init(const TString input,const TString output);
	Double_t epsilon(const Float_t  pT,const Float_t rad, const Int_t pTthresh);
	void prior_histos(const Int_t njets=1E5);

	private:
	static const Int_t npriors_py=3;
	static const Int_t nLeadCuts=11;
	Double_t R[3];
 	TFile *fpyt[3];
 	TFile *foutput;
	TH1D* hprior_py[3][11];
	TH1D* hprior_pyde[3][11];
	TH1D* hdiv_py[3][11];
	TH1D* hdiv_pyde[3][11];
   TH2D *hprior_pythia[3];
   TH2D *hprior_pythiadete[3];

	TString str;

	static const Int_t nptbins=800;
	static const Float_t ptminbin=-100;
	static const Float_t ptmaxbin=100;
	static const Int_t npows=4;
	static const Int_t firstpow=3;

   ClassDef(prior_maker, 1)
};

#endif
