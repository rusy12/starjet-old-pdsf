#ifndef __pythia__hh
#define __pythia__hh
#include <TH2D.h> 

int DeltaPtIndex(Double_t pT);
void pythia(Long64_t nev = -1);


#endif
