#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
//#include <TRandom1.h>
//#include <TDatime.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TPythia6.h>
#include <TParticle.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "StMemStat.h"
#include "embedding.h"
#include "find_embedded_jet.h"
#include "fjwrapper.h"
#include "fourvector.h"
#include "utils.h"
#include "run_pythia.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void data_embedding(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  double r = atof(gSystem->Getenv("RPARAM"));
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcut = atoi(gSystem->Getenv("REFMULT"));
  //Int_t doEffiCorr = atoi(gSystem->Getenv("DOEFFICORR"));
  Int_t pTembFixed = atoi(gSystem->Getenv("PTEMBFIXED"));
//-------------------------------------------------

  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Trigger=%s", r, strigger.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
//SETTING RANDOM SEED
/*  TDatime dt;
  UInt_t curtime = dt.Get();
  UInt_t procid = gSystem->GetPid();
  UInt_t seed = curtime - procid;
TRandom1 *rndNum = new TRandom1(seed);
cout<<"setting seed: "<<seed<<endl;*/
//-------------------------------------------------
  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, central, pp, ppHT, ppJP
  //cout << "[i] Setting refmult cut for AuAu 10\% central" << endl;
  evCuts->SetRefMultCut(refmultcut);
  evCuts->SetVertexZCut(30.); 

  readerdata.GetTrackCuts()->SetDCACut(1.);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(20);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy
  const Double_t pTcut=0.2; //minimal pT of tracks

  const Int_t nemb =1; //how many times to use one particular event for jet embedding 
  //Double_t fEmbPt[] = {0.2, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 10.0, 15.0, 30.0};
  const Double_t pTembMin=0.2; //maximal pT of embedded jets
  const Double_t pTembMax=50; //maximal pT of embedded jets
  TString str = "null";
  str = Form("%s/embedding_R%.1lf.root", outDir.Data(), r);

  StMemStat memstat;
  memstat.Start();

// SETTING RANDOM SEED
  TDatime dt;
  UInt_t curtime = dt.Get();
  UInt_t procid = gSystem->GetPid();
  UInt_t seed = curtime - procid;
  gRandom->SetSeed(seed);
cout<<"[i] Random seed set to "<<seed<<endl;

 // PYTHIA
  TPythia6 * fpythia = new TPythia6();
  fpythia->SetMRPY(1, seed);
//cout<<"Pythia initialized"<<endl;

  int runid = 0;
  int refmult = 0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  //Double32_t rpangle = 0; //[0, 0, 16]
  TClonesArray *embedding_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_part_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_rec_arr = new TClonesArray("embedding", nemb);

  TClonesArray *simarr = new TClonesArray("fourvector", 1000);

  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TTree* ftreejets = new TTree("Embedding","Embedding");
  TBranch *br_runid = ftreejets->Branch("runid", &runid);
  TBranch *br_embedding = ftreejets->Branch("embedding", &embedding_arr); 
  TBranch *br_emb_pythia_part = ftreejets->Branch("emb_pythia_part", &emb_pythia_part_arr); 
  TBranch *br_emb_pythia_dete = ftreejets->Branch("emb_pythia_dete", &emb_pythia_dete_arr); 
  TBranch *br_emb_pythia_dete_rec = ftreejets->Branch("emb_pythia_dete_rec", &emb_pythia_dete_rec_arr); 
  TBranch *br_refmult = ftreejets->Branch("refmult", &refmult);
  TBranch *br_rho = ftreejets->Branch("rho", &rho);
  TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
//  TBranch *br_rpangle = ftreejets->Branch("rpangle", &rpangle);

  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
cout<<"Initializing picoreader"<<endl;
  readerdata.Init(nev);
    
  Int_t evt = 0;
	cout<<"doing event 0"<<endl;
  std::vector<fastjet::PseudoJet> input_vector;

  while (readerdata.NextEvent() == kTRUE)
    {
		//cout<<"Memory used: "<<memstat.Used()<<endl;
      embedding_arr->Delete();
      emb_pythia_part_arr->Delete();
      emb_pythia_dete_arr->Delete();
      emb_pythia_dete_rec_arr->Delete();
      simarr->Delete();
      TStarJetVectorContainer<TStarJetVector>* containerdata = readerdata.GetOutputContainer();
      
   if(charged){
		//if(doEffiCorr)
		//input_vector = make_efficorrected_charged_input_vector(containerdata, 0.2);
		//else
		input_vector = make_charged_input_vector(containerdata, pTcut);
		}    
   else
	input_vector = make_input_vector(containerdata, pTcut);
      
      if (input_vector.size() <= 0) continue;
      
      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      //rpangle = header->GetReactionPlaneAngle();
      refmult = header->GetReferenceMultiplicity();
      runid = header->GetRunId();

      std::vector<fastjet::PseudoJet> input_vector_data = input_vector;

      FJWrapper kt_data;
      kt_data.r = r;
      kt_data.maxrap = max_rap;
      kt_data.algor = fastjet::kt_algorithm;
      kt_data.input_particles = input_vector_data;
      kt_data.Run();

      kt_data.GetMedianAndSigma(rho, sigma);
 
   //for (Int_t iemb = 0; iemb < nemb; iemb++){
   for (Int_t iemb = 0; iemb < nemb; iemb++){

	  //set embedded pT, eta, phi
	  Double_t pT_emb=gRandom->Uniform(pTembMin,pTembMax);
	  Double_t eta_rnd = gRandom->Uniform(-max_rap + r, max_rap - r);
	  Double_t phi_rnd = gRandom->Uniform(0, 2.*TMath::Pi());
/*
	  if(pTembFixed)pT_emb = fEmbPt[iemb]; //pT probe fixed
	  else { //pT probe in an interval
		if(iemb==(nemb-1)) pT_emb =gRandom->Uniform(fEmbPt[iemb],50);
		else pT_emb =gRandom->Uniform(fEmbPt[iemb],fEmbPt[iemb+1]);
	  }
*/
	  
	  std::vector<fastjet::PseudoJet> input_vector_sp = input_vector; //single particle jet embedded into event
	  std::vector<fastjet::PseudoJet> input_vector_pythia_dete_rec = input_vector; //detector level pythia jet embedded into event
	  std::vector<fastjet::PseudoJet> input_vector_pythia_part; //particle level pythia jet
	  std::vector<fastjet::PseudoJet> input_vector_pythia_dete; //detector level pythia jet

	  //SINGLE PARTICLE JETS
	  TLorentzVector v;
	  v.SetPtEtaPhiM(pT_emb, eta_rnd, phi_rnd, 0);
	  fastjet::PseudoJet embededParticle(v.Px(), v.Py(), v.Pz(), v.E());
	  embededParticle.set_user_index(99999);
	  input_vector_sp.push_back(embededParticle);

     //PYTHIA JETS
     Double_t jetPt = MakeCone(fpythia, simarr, pT_emb, eta_rnd, phi_rnd, r, pTcut, charged);
//cout<<"pTemb="<<pT_emb<<" jetpT="<<jetPt<<endl;
	  Int_t Nparticles = simarr->GetEntries();
	  //TLorentzVector corr_jetlv;
	  for(Int_t ipart = 0; ipart < Nparticles; ipart++)
	   {
	  		fourvector *fv = (fourvector*)simarr->At(ipart);
	      TLorentzVector lv = fv->GetTLorentzVector();
//cout<<"particle "<<ipart<<" pT="<<lv.Pt()<<endl;
			fastjet::PseudoJet embededParticle(lv.Px(), lv.Py(), lv.Pz(), lv.E());
	      embededParticle.set_user_index(99999);
         input_vector_pythia_part.push_back(embededParticle);

			//apply tracking efficiency
	      Double_t epsilon=efficiency(fv->GetEta(), fv->GetPt()); //efficiency function from utils.h
//cout<<"efficiency="<<epsilon<<endl;
	      Double_t rand = gRandom->Uniform(0,1);
			if(rand>epsilon)continue;
//cout<<"PASSED"<<endl;
			//corr_jetlv+=lv;
			input_vector_pythia_dete.push_back(embededParticle);
			input_vector_pythia_dete_rec.push_back(embededParticle);
		}
     simarr->Clear("C");     
 
	  FJWrapper akt_data_emb;
	  akt_data_emb.r = r;
	  akt_data_emb.maxrap = max_rap;
	  akt_data_emb.algor = fastjet::antikt_algorithm;
	  akt_data_emb.input_particles = input_vector_sp;
	  akt_data_emb.Run();

     //single aprticle jets
	  new ((*embedding_arr)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb, (embedding*)embedding_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, r, rho);

	  FJWrapper akt_data_emb2;
	  akt_data_emb2.r = r;
	  akt_data_emb2.maxrap = max_rap;
	  akt_data_emb2.algor = fastjet::antikt_algorithm;

	  //pythia jets - particle level
	  akt_data_emb2.input_particles = input_vector_pythia_part;
	  akt_data_emb2.Run();
	  new ((*emb_pythia_part_arr)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb2, (embedding*)emb_pythia_part_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, r, rho);
//cout<<"pT eta phi"<<endl;
/*
cout<<"gen: "<<pT_emb<<" "<<eta_rnd<<" "<<phi_rnd<<endl; 
double recpT=((embedding*)emb_pythia_part_arr->At(iemb))->fPtEmbReco;
double receta=((embedding*)emb_pythia_part_arr->At(iemb))->fetaEmbReco;
double recphi=((embedding*)emb_pythia_part_arr->At(iemb))->fphiEmbReco;
cout<<"rec: "<<recpT<<" "<<receta<<" "<<recphi<<endl; */
	  FJWrapper akt_data_emb3;
	  akt_data_emb3.r = r;
	  akt_data_emb3.maxrap = max_rap;
	  akt_data_emb3.algor = fastjet::antikt_algorithm;

//recalculate eta_rnd and phi_rnd for the jet after efficiency correction
//Double_t eta_rnd_corr=corr_jetlv.PseudoRapidity();
//Double_t phi_rnd_corr=corr_jetlv.Phi();

 //pythia track. eff. corrected jets
	  akt_data_emb3.input_particles = input_vector_pythia_dete;
	  akt_data_emb3.Run();
	  new ((*emb_pythia_dete_arr)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb3, (embedding*)emb_pythia_dete_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, r, rho);
/*
 recpT=((embedding*)emb_pythia_dete_arr->At(iemb))->fPtEmbReco;
 receta=((embedding*)emb_pythia_dete_arr->At(iemb))->fetaEmbReco;
 recphi=((embedding*)emb_pythia_dete_arr->At(iemb))->fphiEmbReco;
cout<<"rec corr: "<<recpT<<" "<<receta<<" "<<recphi<<endl; */

		//cout<<"Memory used1: "<<memstat.Used()<<endl;

 //pythia track. eff. corrected jets embedded into real events
	  FJWrapper akt_data_emb4;
	  akt_data_emb4.r = r;
	  akt_data_emb4.maxrap = max_rap;
	  akt_data_emb4.algor = fastjet::antikt_algorithm;

	  akt_data_emb4.input_particles = input_vector_pythia_dete_rec;
	  akt_data_emb4.Run();
	  new ((*emb_pythia_dete_rec_arr)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb4, (embedding*)emb_pythia_dete_rec_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, r, rho);

		//cout<<"Memory used2: "<<memstat.Used()<<endl;

   }//embedding_pT   
      foutput->cd();
      ftreejets->Fill();
      readerdata.PrintStatus(60); //every 1min 

      evt++;
    }

  foutput->cd();
  ftreejets->Write();
  foutput->Close();


  readerdata.PrintStatus();
  memstat.Stop();
}
