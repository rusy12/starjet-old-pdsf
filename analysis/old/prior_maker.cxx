#include <TSystem.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TF1.h>
#include <TRandom.h>
#include <TMath.h>

#include <iostream>
#include "prior_maker.h"

ClassImp(prior_maker)
using namespace std;

Double_t levy(Double_t *x, Double_t *par)
{
	Double_t pT=x[0];
	Double_t mu=par[0];
	Double_t c=par[1];
	Double_t pow=par[2];
	Double_t x0=par[3];
	Double_t y=TMath::Exp(-(c/(2*(pT-mu))))/TMath::Power((pT-mu),pow);
   //y = (1.0/(1.0+TMath::Exp(-(pT-x0))))*y; //suppress low-pT part
	return y;
}

Double_t LevyFitFunc(Double_t* x_val, Double_t* par)
{
    // Fit function for d2N/(2pi*pT dpT dy)
    // taken from here: http://sampa.if.usp.br/~suaide/blog/files/papers/PLB6372006.pdf
    Double_t pT, y, B, T, n, m0, mu, x0;
    B    = par[0];
    T    = par[1];
    n    = par[2];
    m0   = par[3];
    mu   = par[4];
	 x0   = par[5];
    pT   = x_val[0];
    Double_t mT = TMath::Sqrt((pT-mu)*(pT-mu)+m0*m0);
    y = B/TMath::Power(1.0+(mT-m0)/(n*T),n);
   // y = (1.0/(1.0+TMath::Exp(-(pT-x0))))*y; //suppress low-pT part
    return y;
}

Double_t powlaw(Double_t* x_val, Double_t* par)
{
	Double_t y, pT, pow, x0;
	pT=x_val[0];
	pow=par[0];
   x0=par[1];
	y=(1.0/TMath::Power(pT,pow));
	//y=y*TMath::Power((1.0/(1.0+TMath::Exp(-(pT-x0)))),(10.0+pow)/10.0); //suppress low-pT part
   return y;
}



//___________________________________________________________________
//default constructor
prior_maker::prior_maker()
{
	const TString inDir = gSystem->Getenv("INPUTDIR");
   const TString outDir = gSystem->Getenv("OUTPUTDIR");
	init(inDir,outDir);
}

//___________________________________________________________________
//default destructor
prior_maker::~prior_maker()
{
}

//___________________________________________________________________
void prior_maker::init(const TString input,const TString output)
{
	R[0]=0.2;
	R[1]=0.3;
	R[2]=0.4;

	for(int i=0;i<npriors_py;i++){
		str= Form("%s/histos_pythiajet_R%.1lf.root",input.Data(),R[i]);
		fpyt[i]= new TFile(str.Data(), "OPEN");
		TH2D *hPrior2d = (TH2D*)fpyt[i]->Get("hpT_pTlead");

		for(int j=0;j<nLeadCuts;j++){
			Int_t firstbin = hPrior2d->GetYaxis()->FindBin(j);
			Int_t lastbin = hPrior2d->GetNbinsY();
			hprior_py[i][j] = (TH1D*)hPrior2d->ProjectionX(Form("prior_pythia_%i_%i",i,j), firstbin, lastbin);
			if(j<1)continue;
			Int_t bini1=hprior_py[i][j]->FindBin(35);
			Int_t bini2=hprior_py[i][j]->GetNbinsX();
			hprior_py[i][j]->Scale(hprior_py[i][0]->Integral(bini1,bini2,"width")/hprior_py[i][j]->Integral(bini1,bini2,"width"));
			hdiv_py[i][j]=(TH1D*)hprior_py[i][j]->Clone(Form("hdiv_py_%i_%i",i,j));
			hdiv_py[i][j]->Divide(hprior_py[i][0]);
		}

		hPrior2d = (TH2D*)fpyt[i]->Get("hpT_pTlead_dete");
		for(int j=0;j<nLeadCuts;j++){
			Int_t firstbin = hPrior2d->GetYaxis()->FindBin(j);
			Int_t lastbin = hPrior2d->GetNbinsY();
			hprior_pyde[i][j] = (TH1D*)hPrior2d->ProjectionX(Form("prior_pythia_det_%i_%i",i,j), firstbin, lastbin);
			if(j<1)continue;
			Int_t bini1=hprior_pyde[i][j]->FindBin(35);
			Int_t bini2=hprior_pyde[i][j]->GetNbinsX();
			hprior_pyde[i][j]->Scale(hprior_pyde[i][0]->Integral(bini1,bini2,"width")/hprior_pyde[i][j]->Integral(bini1,bini2,"width"));
			hdiv_pyde[i][j]=(TH1D*)hprior_pyde[i][j]->Clone(Form("hdiv_pyde_%i_%i",i,j));
			hdiv_pyde[i][j]->Divide(hprior_pyde[i][0]);
		}
		delete hPrior2d;
	}
/*
hdiv_py[2][5]->Draw();
hprior_py[2][0]->Draw();
hprior_py[2][5]->SetLineColor(kRed);
hprior_py[2][5]->Draw("same");
*/


  str = Form("%s/histos_prior.root", output.Data());
  foutput = new TFile(str.Data(), "RECREATE");

return;
}

//___________________________________________________________________
Double_t prior_maker::epsilon(const Float_t pT,const Float_t rad,const Int_t pTthresh)
{
	Int_t r=0;
	if(rad>0.2)r=1;
	else if(rad>0.3)r=2;
	Double_t eps;
	if(pT<pTthresh) eps=1;
	else eps=hdiv_py[r][pTthresh]->GetBinContent(hdiv_py[r][pTthresh]->FindBin(pT));
return eps;
}

//___________________________________________________________________
void prior_maker::prior_histos(const Int_t njets)
{
  TH2::SetDefaultSumw2();
  
    Double_t pTmax=ptmaxbin;

//-----------------------
//histogram declarations
//-----------------------

  TH2D *hprior_flat = new TH2D("hprior_flat", "flat pT prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
  TH2D *hprior_gauss = new TH2D("hprior_gauss", "Gaussian pT prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
  TH2D *hprior_levy = new TH2D("hprior_levy", "levy I function prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
  TH2D *hprior_levy_alex = new TH2D("hprior_levy_alex", "levy II function prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	

for(int rad=0; rad<npriors_py;rad++){
str=Form("hprior_pythia_R%.1lf",R[rad]);
hprior_pythia[rad] = new TH2D(str, "PYTHIA prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
str=Form("hprior_pythiadete_R%.1lf",R[rad]);
hprior_pythiadete[rad] = new TH2D(str, "PYTHIA prior distribution; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
}

Int_t lastpow=firstpow+npows-1;
TH2D *hprior_powlaw[npows];
for(int pwr=firstpow; pwr<lastpow+1; pwr++)
{	
  str=Form("hprior_powlaw%i",pwr);
  hprior_powlaw[pwr-firstpow] = new TH2D(str, Form("PowLaw pT prior distribution1/pT^%i; p_{T} [GeV/c]",pwr), nptbins, ptminbin, ptmaxbin,nLeadCuts,-0.5,nLeadCuts-0.5); 	
}
//-----------------
//TF1 declarations
//-----------------
TF1 *myLevy = new TF1("myLevy",levy,0,ptmaxbin,4);
     myLevy->SetParameters(0.0,5.0,5.0,4.0);
     myLevy->SetParNames("mu","c","power","x0");
	  myLevy->SetNpx(1000);

TF1* LevyFit_pT = new TF1("LevyFit_pT",LevyFitFunc,0.0,ptmaxbin,6);
 	  LevyFit_pT->SetParameter(0,1); // B, changes the amplitude, 0.1
	  LevyFit_pT->SetParameter(1,0.4); // T, changes slope, 0.4
	  LevyFit_pT->SetParameter(2,4.4); // n, changes how fast spectrum drops, 5.8
	  LevyFit_pT->SetParameter(3,0.0001); // m0, changes the width, 0.0001
	  LevyFit_pT->SetParameter(4,0.0); // mu, changes the x-axis shift, 0.0
	  LevyFit_pT->SetParameter(5,4.0); // x0, position of the low-pT knee
	  LevyFit_pT->SetNpx(1000);

TF1 *fplaw = new TF1("fplaw",powlaw,0,ptmaxbin,2);
     fplaw->SetParameters(5,4);
     fplaw->SetParNames("power","x0");

//Filling histos
for(Int_t leadCut=0; leadCut<nLeadCuts;leadCut++){
	if(leadCut!=5 && leadCut!=7)continue;
	cout<<"pTleading cut: "<<leadCut<<endl;
	Double_t pTleadCut;
	if(leadCut==0) pTleadCut=0.2;
	else pTleadCut=(Double_t)leadCut;

	for(Int_t jet=0; jet<njets;jet++){
		if((jet%1000000)==0)cout<<"#jets filled: "<<jet/1000000<<"M"<<endl;
		Double_t pT=gRandom->Uniform(pTleadCut, pTmax);
		Double_t pTgauss=gRandom->Gaus(pTleadCut, 10);

		hprior_flat->Fill(pT,leadCut);
		if(pTgauss>leadCut) hprior_gauss->Fill(pTgauss,leadCut);

		Double_t w;
		for(int pwr=firstpow; pwr<lastpow+1; pwr++){
			fplaw->SetParameter(0,pwr);
			w=fplaw->Eval(pT);
			hprior_powlaw[pwr-firstpow]->Fill(pT,leadCut,w);			
		}

		w=myLevy->Eval(pT);
		hprior_levy->Fill(pT,leadCut,w);
		w=LevyFit_pT->Eval(pT);
		hprior_levy_alex->Fill(pT,leadCut,w);
		
		for(int rad=0; rad<npriors_py;rad++)
		{
			pT=hprior_py[rad][leadCut]->GetRandom();
			hprior_pythia[rad]->Fill(pT,leadCut);
			pT=hprior_pyde[rad][leadCut]->GetRandom();
			hprior_pythiadete[rad]->Fill(pT,leadCut);
		}	
		}//pT loop
}//pTleading loop

//********************************
//low-pT part suppression
//********************************
for(int ybin=1; ybin<=nLeadCuts; ybin++)
{
	if(ybin!=6 && ybin!=8)continue;
	Float_t ptc;
	Double_t yld;
	Double_t eps;
	Double_t rat;
	Double_t mult;
for(int xbin=1; xbin<=nptbins; xbin++)
{
	ptc=hprior_powlaw[5-firstpow]->GetXaxis()->GetBinCenter(xbin);
	yld=hprior_powlaw[5-firstpow]->GetBinContent(xbin,ybin);
	eps=epsilon(ptc,0.4,ybin-1);
	hprior_powlaw[5-firstpow]->SetBinContent(xbin,ybin,yld*eps*eps);

}
	Double_t num=hprior_powlaw[5-firstpow]->GetBinContent(hprior_powlaw[5-firstpow]->GetXaxis()->FindBin(ybin-1),ybin);
cout<<"numerator: "<<num<<endl;

for(int xbin=1; xbin<=nptbins; xbin++)
{
//if(xbin%40!=0)continue;
//cout<<"xbin"<<xbin<<endl;
	for(int pwr=firstpow; pwr<lastpow+1; pwr++){
		if(pwr==5)continue;
		ptc=hprior_powlaw[pwr-firstpow]->GetXaxis()->GetBinCenter(xbin);
		yld=hprior_powlaw[pwr-firstpow]->GetBinContent(xbin,ybin);
		eps=epsilon(ptc,0.4,ybin-1);
		   rat=num/hprior_powlaw[pwr-firstpow]->GetBinContent(hprior_powlaw[pwr-firstpow]->GetXaxis()->FindBin(ybin-1),ybin);
			mult=rat/TMath::Power(epsilon(ybin-1+0.1,0.4,ybin-1),2);
//cout<<"prior pT^-"<<pwr<<" multiplicator: "<<mult<<" ratio: "<<rat<<endl;
		hprior_powlaw[pwr-firstpow]->SetBinContent(xbin,ybin,yld*eps*eps*mult);
	}
	ptc=hprior_levy->GetXaxis()->GetBinCenter(xbin);
	eps=epsilon(ptc,0.4,ybin-1);
	yld=hprior_levy->GetBinContent(xbin,ybin);
	   rat=num/hprior_levy->GetBinContent(hprior_levy->GetXaxis()->FindBin(ybin-1),ybin);
		mult=rat/TMath::Power(epsilon(ybin-1+0.1,0.4,ybin-1),2);
//cout<<"epsilon: "<<eps<<endl;
//cout<<"prior: levy"<<" multiplicator: "<<mult<<" ratio: "<<rat<<endl;
	hprior_levy->SetBinContent(xbin,ybin,yld*eps*eps*mult);
	Double_t yld2=hprior_levy->GetBinContent(xbin,ybin);
//cout<<"yield1: "<<yld<<"yield2: "<<yld2<<endl;

	yld=hprior_levy_alex->GetBinContent(xbin,ybin);
	   rat=num/hprior_levy_alex->GetBinContent(hprior_levy_alex->GetXaxis()->FindBin(ybin-1),ybin);
		mult=rat/TMath::Power(epsilon(ybin-1+0.1,0.4,ybin-1),1);
//cout<<"prior: levy II"<<" multiplicator: "<<mult<<" ratio: "<<rat<<endl;
	hprior_levy_alex->SetBinContent(xbin,ybin,yld*eps*mult);
	yld2=hprior_levy_alex->GetBinContent(xbin,ybin);
//cout<<"yield1: "<<yld<<"yield2: "<<yld2<<endl;

	for(int rad=0; rad<npriors_py;rad++)
	{
		yld=hprior_pythia[rad]->GetBinContent(xbin,ybin); 
	   	rat=num/hprior_pythia[rad]->GetBinContent(hprior_pythia[rad]->GetXaxis()->FindBin(ybin-1),ybin);
			mult=rat/TMath::Power(epsilon(ybin-1+0.1,0.4,ybin-1),1);
//cout<<"prior: PYTHIA R="<<R[rad]<<" multiplicator: "<<mult<<" ratio: "<<rat<<endl;
		hprior_pythia[rad]->SetBinContent(xbin,ybin,yld*eps*mult);
		yld2=hprior_pythia[rad]->GetBinContent(xbin,ybin); 
//cout<<"yield1: "<<yld<<"yield2: "<<yld2<<endl;
		yld=hprior_pythiadete[rad]->GetBinContent(xbin,ybin); 
	   	rat=num/hprior_pythiadete[rad]->GetBinContent(hprior_pythiadete[rad]->GetXaxis()->FindBin(ybin-1),ybin);
			mult=rat/TMath::Power(epsilon(ybin-1+0.1,0.4,ybin-1),1);
//cout<<"prior: PYTHIA dete R="<<R[rad]<<" multiplicator: "<<mult<<" ratio: "<<rat<<endl;
		hprior_pythiadete[rad]->SetBinContent(xbin,ybin,yld*eps*mult);
		yld2=hprior_pythiadete[rad]->GetBinContent(xbin,ybin); 
//cout<<"yield1: "<<yld<<"yield2: "<<yld2<<endl;
	}

}//x-bin
}//y-bin
foutput->cd();
foutput->Write();
foutput->Close();
delete foutput;

}
