#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TH2D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TStopwatch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "embedding.h"
#include "fjwrapper.h"
#include "utils.h"
#include "checker.h"

#include <iostream>
using namespace std;

void ana_embedding(Long64_t nev)
{
  TStopwatch timer;
  timer.Start();
  
  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  TString dataDir = gSystem->Getenv("INPUTDIR");

  double r = atof(gSystem->Getenv("RPARAM"));
  double acut = atof(gSystem->Getenv("ACUT"));
  int refcut = atof(gSystem->Getenv("REFMULT"));
  int year=11;
  const Int_t nemb = 11;
  Double_t fEmbPt[] = {0.2, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 10.0, 15.0, 30.0};

  cout << Form("[i] WRKDIR=%s", dataDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Acut=%.1lf", r, acut) << endl;

  int runid = 0;
  int refmult = 0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  //Double32_t rpangle = 0; //[0, 0, 16]
  TClonesArray *embedding_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_part_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_rec_arr = new TClonesArray("embedding", nemb);

  TString str = "null";
  str = Form("%s/embedding_R%.1lf.root", dataDir.Data(), r);

  TFile *finput = new TFile(str.Data(), "OPEN");
  TTree* ftreejets = (TTree*)finput->Get("Embedding");
  ftreejets->SetBranchAddress("runid", &runid);
  ftreejets->SetBranchAddress("refmult", &refmult);
  ftreejets->SetBranchAddress("rho", &rho);
  ftreejets->SetBranchAddress("sigma", &sigma);
  ftreejets->SetBranchAddress("embedding", &embedding_arr);
  ftreejets->SetBranchAddress("emb_pythia_part", &emb_pythia_part_arr);
  ftreejets->SetBranchAddress("emb_pythia_dete", &emb_pythia_dete_arr);
  ftreejets->SetBranchAddress("emb_pythia_dete_rec", &emb_pythia_dete_rec_arr);
  //ftreejets->SetBranchAddress("rpangle", &rpangle);
  
  str = Form("%s/histos_embeddedjet_R%.1lf_refmult%i.root", dataDir.Data(), r, refcut);
  
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  //here we define the histograms
  //------------------------------

  Int_t nptbins=400;
  Float_t ptminbin=-100;
  Float_t ptmaxbin=100;

  TString hname;
  TString bname;

  TH1I *hevts = new TH1I("hevts", "hevts", 2, 0, 2);
  TH2D* delta_pt_BG_sp = new TH2D("delta_pt_BG_sp","delta pT for BG corrections, using sp probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  TH2D* delta_pt_BG_pyt = new TH2D("delta_pt_BG_pyt","delta pT for BG corrections, using pythia probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  TH2D* delta_pt_dete = new TH2D("delta_pt_dete","delta pT for detector corrections", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  TH2D* delta_pt_BG_dete = new TH2D("delta_pt_BG_dete","delta pT for BG and detector corrections", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  TH1D *heffi_dete=new TH1D("heffi_dete", "jet reco. efficiency after inst. corrections",nptbins, ptminbin, ptmaxbin);
  TH1D *heffi_BG_dete=new TH1D("heffi_BG_dete", "jet reco. efficiency after inst. and BG corrections",nptbins, ptminbin, ptmaxbin);
  TH2D *hdR_pTfrac=new TH2D("hdR_pTfrac","dR of matched detector level jet vs. ratio pTjet over pTemb",40,0,0.4,50,0,1);
  TH2D *eta_phi_emb=new TH2D("eta_phi_emb","eta vs phi of embedded jets",40,-1,1,48,0,2*TMath::Pi());
/*
  TH2D *hdpT_area[nemb];
  TH2D *hdpT_pTlead[nemb];
  TH2D *hdpT_refmult[nemb];
  TH2D *hdpT_rho[nemb];
  TH2D *hdpT_day[nemb];
  TH2D *hdpT_eta[nemb];
  TH2D *hdpT_phi[nemb];
  TH1D *hdpT_cut0[nemb];
  TH1D *hdpT_cut1[nemb];
  TH1D *hdpT_cut2[nemb];
  TH1D *hdpT_cut3[nemb];
  TH1D *hdpT_pyth_cut3[nemb];
  TH1D *hdpT_pythCorr_cut3[nemb];
  TH1D *hdpT_pyth_pythCorr[nemb];
  TH1D *hdpT_sp_pyth[nemb];
  TH1D *heffi;

  for(Int_t iemb=0; iemb<nemb;iemb++){
		bname=Form("hdpT%.1lf",fEmbPt[iemb]);
		hname=Form("%s_area",bname.Data());
		hdpT_area[iemb] = new TH2D(hname,"deltapT_vs_area;#deltap_{T};area (sr)",nptbins,ptminbin,ptmaxbin,20,0,2);
		hname=Form("%s_pTlead",bname.Data());
		hdpT_pTlead[iemb] = new TH2D(hname,"deltapT_vs_pTlead;#deltap_{T};p_{T}^{leading} (sr)",nptbins,ptminbin,ptmaxbin,100,0,50);
		hname=Form("%s_refmult",bname.Data());
		hdpT_refmult[iemb] = new TH2D(hname,"deltapT_vs_refmult;#deltap_{T}; refmult",nptbins,ptminbin,ptmaxbin,60,200,800);
		hname=Form("%s_rho",bname.Data());
		hdpT_rho[iemb] = new TH2D(hname,"deltapT_vs_rho;#deltap_{T}; #rho",nptbins,ptminbin,ptmaxbin,70,0,70);
		hname=Form("%s_day",bname.Data());
		hdpT_day[iemb] = new TH2D(hname,"deltapT_vs_day;#deltap_{T}; day",nptbins,ptminbin,ptmaxbin,90,0.5,90.5);
		hname=Form("%s_eta",bname.Data());
		hdpT_eta[iemb] = new TH2D(hname,"deltapT_vs_eta;#deltap_{T}; #eta",nptbins,ptminbin,ptmaxbin,100,-1,1);
		hname=Form("%s_phi",bname.Data());
		hdpT_phi[iemb] = new TH2D(hname,"deltapT_vs_phi;#deltap_{T}; #phi",nptbins,ptminbin,ptmaxbin,60, -1*TMath::Pi(), TMath::Pi());
		hname=Form("%s_cut0",bname.Data());
		hdpT_cut0[iemb] = new TH1D(hname,"deltapT;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut1",bname.Data());
		hdpT_cut1[iemb] = new TH1D(hname,"deltapT without bad sector;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut2",bname.Data());
		hdpT_cut2[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut3",bname.Data());
		hdpT_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pyth_cut3",bname.Data());
		hdpT_pyth_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R (pythia);#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pythCorr_cut3",bname.Data());
		hdpT_pythCorr_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R (eff. corr. pythia);#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pyth_pythCorr",bname.Data());
		hdpT_pyth_pythCorr[iemb] = new TH1D(hname,"pythia - pythiacorr;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_sp_pyth",bname.Data());
		hdpT_sp_pyth[iemb] = new TH1D(hname,"sp - pythia;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname="heffi";
		heffi = new TH1D(hname,"jet reco efficiency;#p_{T};efficiency",200,-100,100);
		}      
*/
  //------------------------------

	//define bad TPC sectors, bad days, etc.
	checker * eventCheck = new checker();
   eventCheck->addBadSector(-1,1,-0.78,-0.26);
  //------------------------------	
   Int_t njets=0; //total number of embedded pythia jets

  for(Int_t ievt = 0; ievt < ftreejets->GetEntries(); ievt++)
    {
		if(ievt%1000==0)cout<<"Processing event "<<ievt<<endl;
      ftreejets->GetEntry(ievt);
      
      if(refmult < refcut) continue;
		Int_t day=(runid/1000)-((year+1)*1000);

      bool badDay=eventCheck->isBadDay(day);
		if(badDay) continue;

      hevts->Fill(1);

      for(Int_t ejet = 0; ejet < embedding_arr->GetEntries(); ejet++) //loop over cycles of embedding (usually only 1)
		{
	   embedding *embeddedjet = (embedding*)embedding_arr->At(ejet);
	   embedding *embjet_pyth_part = (embedding*)emb_pythia_part_arr->At(ejet);
	   embedding *embjet_pyth_dete = (embedding*)emb_pythia_dete_arr->At(ejet);
	   embedding *embjet_pyth_dete_rec = (embedding*)emb_pythia_dete_rec_arr->At(ejet);

      //was the jet found in this event?
      bool found_jet_sp=embeddedjet->ffoundJet;
      bool found_jet_pp=embjet_pyth_part->ffoundJet;
      bool found_jet_pd=embjet_pyth_dete->ffoundJet;
      bool found_jet_pdr=embjet_pyth_dete_rec->ffoundJet;

      //pT, eta, phi of the embedded jet (should be same for sp, pp, pd, pdr jets)
      Double_t pTemb=embeddedjet->fPtEmb;
      Double_t etaemb=embeddedjet->fetaEmb;
      Double_t phiemb=embeddedjet->fphiEmb;
      
		//area of the reconstructed jets
		Double_t area_sp=embeddedjet->fAreaEmb;
		Double_t area_pp=embjet_pyth_part->fAreaEmb;
		Double_t area_pd=embjet_pyth_dete->fAreaEmb;
		Double_t area_pdr=embjet_pyth_dete_rec->fAreaEmb;

		//pTjet
      Double_t pT_sp=embeddedjet->fPtEmbReco;
      Double_t pT_pp=embjet_pyth_part->fPtEmbReco;
      Double_t pT_pd=embjet_pyth_dete->fPtEmbReco;
      Double_t pT_pdr=embjet_pyth_dete_rec->fPtEmbReco;

		//pTjet-rho*A
      Double_t pT_corr_sp=embeddedjet->fpTEmbCorr;
      //Double_t pT_corr_pp=embjet_pyth_part->fpTEmbCorr; //these are jets w/o BG -> no corrected pT needed
      //Double_t pT_corr_pd=embjet_pyth_dete->fpTEmbCorr; //these are jets w/o BG -> no corrected pT needed
      Double_t pT_corr_pdr=embjet_pyth_dete_rec->fpTEmbCorr;

		//dR=Sqrt((etaRec-etaEmb)^2+(phiRec-phiEmb)^2)
		Double_t dR_sp=embeddedjet->fdR;
		Double_t dR_pp=embjet_pyth_part->fdR;
		Double_t dR_pd=embjet_pyth_dete->fdR;
		Double_t dR_pdr=embjet_pyth_dete_rec->fdR;

/*
		Double_t area=embeddedjet->fAreaEmb;
		Double_t delta_pT=embeddedjet->fdeltapT;
			Double_t delta_pT_pyth=embjet_pyth_part->fdeltapT;
			Double_t delta_pT_pyth_dete=embjet_pyth_dete->fdeltapT;
		Double_t probe_pT=embeddedjet->fPtEmb;
		Double_t probe_eta=embeddedjet->fetaEmb;
		Double_t probe_phi=embeddedjet->fphiEmb;
			if(probe_phi>TMath::Pi())probe_phi=probe_phi-2*TMath::Pi();
		Double_t jet_eta=embeddedjet->fetaEmbReco;
		Double_t jet_phi=embeddedjet->fphiEmbReco;
		Double_t jet_pT=embeddedjet->fPtEmbReco;
			Double_t jet_pT_pyth=embjet_pyth_part->fPtEmbReco;
			Double_t jet_pT_pyth_dete=embjet_pyth_dete->fPtEmbReco;
		Double_t pT_leading=embeddedjet->fPtLeading; 
		
		hdpT_area[ejet]->Fill(delta_pT,area);
*/		
		
      //bool badSector=eventCheck->isBadSector(probe_eta,probe_phi);
		//if(badSector) continue;

		//badSector=eventCheck->isNearBadSector(probe_eta,probe_phi, 0.5*r); //remove jets bordering with the bad sector
		//if(badSector) continue;

		bool badSector=eventCheck->isNearBadSector(etaemb,phiemb, 1*r); //remove jets bordering with the bad sector
		if(badSector) continue;

		//FILL HISTOS
		Double_t weight=TMath::Power(pTemb,-6);
		if(found_jet_sp && area_sp>acut)
		delta_pt_BG_sp->Fill(pTemb,pT_corr_sp-pTemb,weight);
		if(found_jet_pd && found_jet_pp)
		delta_pt_dete->Fill(pTemb,pT_pd-pT_pp,weight);
		if(found_jet_pdr && found_jet_pd && area_pdr>acut){ 
		delta_pt_BG_pyt->Fill(pTemb,pT_corr_pdr-pT_pd,weight);
		delta_pt_BG_dete->Fill(pTemb,pT_corr_pdr-pTemb,weight);
      }
		//if(found_jet_pp)
			heffi_dete->Fill(-pTemb);
			heffi_BG_dete->Fill(-pTemb);
		if(found_jet_pd)
			heffi_dete->Fill(pTemb);
		if(found_jet_pdr)
			heffi_BG_dete->Fill(pTemb);

		if(pT_pp>0 && found_jet_pd)
		hdR_pTfrac->Fill(dR_pd, pT_pd/pT_pp);
	   eta_phi_emb->Fill(etaemb,phiemb);		
/*
		hdpT_cut3[ejet]->Fill(delta_pT);
		if(jet_pT_pyth>0){
		hdpT_pyth_cut3[ejet]->Fill(delta_pT_pyth);
		hdpT_sp_pyth[ejet]->Fill(jet_pT-jet_pT_pyth);
		}
		if(jet_pT_pyth_dete>0){
		hdpT_pyth_dete_cut3[ejet]->Fill(delta_pT_pyth_dete);
		hdpT_pyth_pyth_dete[ejet]->Fill(jet_pT_pyth-jet_pT_pyth_dete);
		}
		hdpT_pTlead[ejet]->Fill(delta_pT,pT_leading);
		hdpT_refmult[ejet]->Fill(delta_pT,refmult);
		hdpT_rho[ejet]->Fill(delta_pT,rho);
		hdpT_day[ejet]->Fill(delta_pT,day);
		hdpT_eta[ejet]->Fill(delta_pT,probe_eta);
		hdpT_phi[ejet]->Fill(delta_pT,probe_phi);

		if(jet_pT_pyth>0){
			if(jet_pT_pyth_dete>0) heffi->Fill(jet_pT_pyth);
			else heffi->Fill(-jet_pT_pyth);
		}
*/
    }//jet loop
}//event loop

  foutput->cd();
  foutput->Write();

  foutput->Close();
  delete foutput;

  finput->Close();
  delete finput;

  timer.Stop();
  timer.Print();
}
