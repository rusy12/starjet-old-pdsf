#ifndef __PySimSimulationHeader__hh
#define __PySimSimulationHeader__hh

#include "Rtypes.h"
#include "TObject.h"

class PySimSimulationHeader : public TObject
{
 public:
  PySimSimulationHeader();
  PySimSimulationHeader(Int_t nevents, Int_t ntrials, Double_t xsection);
  ~PySimSimulationHeader();
  
  Int_t GetNevents() {return fnevents;}
  Int_t GetNtrials() {return fntrials;}
  Double_t GetXsection() {return fxsection;}

 private:
  Int_t fnevents;
  Int_t fntrials;

  Double32_t fxsection; //[0, 0, 16]

  ClassDef(PySimSimulationHeader, 1);
};

#endif

