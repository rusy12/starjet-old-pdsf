#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TStopwatch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "embedding.h"
#include "fjwrapper.h"
#include "utils.h"
#include "checker.h"

#include <iostream>
using namespace std;

void ana_embedding(Long64_t nev)
{
  TStopwatch timer;
  timer.Start();
  
  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  TString dataDir = gSystem->Getenv("INPUTDIR");

  double r = atof(gSystem->Getenv("RPARAM"));
  double acut = atof(gSystem->Getenv("ACUT"));
  int year=11;
  const Int_t nemb = 16; //number of embedded probes in data_embedding.cxx
  const int npTlead=8; //maximum value of pTleading cut 
  //Double_t fEmbPt[] = {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 10.0, 15.0, 20.0, 30.0, 50.0 };
  cout << Form("[i] WRKDIR=%s", dataDir.Data()) << endl;
  cout << Form("[i] R=%.1lf Acut=%.2lf", r, acut) << endl;

  int runid = 0;
  int refmult = 0;
  Double_t weight = 1.0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  TClonesArray *embedding_arr = new TClonesArray("embedding", nemb);
/*  TClonesArray *emb_pythia_part_arr = new TClonesArray("embedding", nemb);
  //TClonesArray *emb_pythia_dete_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_arr2 = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_rec_arr = new TClonesArray("embedding", nemb);
  TClonesArray *emb_pythia_dete_rec_arr2 = new TClonesArray("embedding", nemb);*/

  TString str = "null";
  str = Form("%s/embedding_R%.1lf.root", dataDir.Data(), r);

  TFile *finput = new TFile(str.Data(), "OPEN");
  TTree* ftreejets = (TTree*)finput->Get("Embedding");
  ftreejets->SetBranchAddress("runid", &runid);
  ftreejets->SetBranchAddress("weight", &weight);
  ftreejets->SetBranchAddress("refmult", &refmult);
  ftreejets->SetBranchAddress("rho", &rho);
  ftreejets->SetBranchAddress("sigma", &sigma);
  ftreejets->SetBranchAddress("embedding", &embedding_arr);
/*  ftreejets->SetBranchAddress("emb_pythia_part", &emb_pythia_part_arr);
  //ftreejets->SetBranchAddress("emb_pythia_dete", &emb_pythia_dete_arr);
  ftreejets->SetBranchAddress("emb_pythia_dete2", &emb_pythia_dete_arr2);
  ftreejets->SetBranchAddress("emb_pythia_dete_rec", &emb_pythia_dete_rec_arr);
  ftreejets->SetBranchAddress("emb_pythia_dete_rec2", &emb_pythia_dete_rec_arr2);*/
  ftreejets->SetBranchAddress("rpangle", &rpangle);
  
  //str = Form("%s/histos_embeddedjet_R%.1lf_refmult%i.root", dataDir.Data(), r, refcut);
  str = Form("%s/histos_embeddedjet_R%.1lf.root", dataDir.Data(), r);
  
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  //here we define the histograms
  //------------------------------

  Int_t nptbins=800;
  Float_t ptminbin=-100;
  Float_t ptmaxbin=100;

  TString hname;
  TString bname;

  TH1I *hevts = new TH1I("hevts", "hevts", 2, 0, 2);
  TH2D *hdR_pTfrac=new TH2D("hdR_pTfrac","dR of matched detector level jet vs. ratio pTembpart over pTemb",40,0,0.4,11,0,1.1);
  TH2D *eta_phi_emb=new TH2D("eta_phi_emb","eta vs phi of embedded jets",40,-1,1,48,0,2*TMath::Pi());
  TH1D *pTpart_sp_BG=new TH1D("pTpart_sp_BG", "praction of jet pT carried by embedded particles",21,0,1.05);
  //TH1D *pTpart_pyt_part=new TH1D("pTpart_pyt_part", "praction of jet pT carried by embedded particles",21,0,1.05);
  //TH1D *pTpart_pyt_dete=new TH1D("pTpart_pyt_dete", "praction of jet pT carried by embedded particles",21,0,1.05);
  //TH1D *pTpart_pyt_dete_BG=new TH1D("pTpart_pyt_dete_BG", "praction of jet pT carried by embedded particles",21,0,1.05);
  TH2D* delta_pt_BG_sp[npTlead];
  //TH2D* delta_pt_BG_pyt[npTlead];
  //TH2D* delta_pt_dete[npTlead];
  //TH2D* delta_pt_BG_dete[npTlead];
  //TH2D* delta_pt_inplane[npTlead];
  //TH2D* delta_pt_outplane[npTlead];
  //TH1D* heffi_BG_dete[npTlead];

for(Int_t pTlcut=0; pTlcut<npTlead; pTlcut++){
  hname=Form("delta_pt_BG_sp_%i", pTlcut);
  delta_pt_BG_sp[pTlcut] = new TH2D(hname,"delta pT for BG corrections, using sp probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  hname=Form("dpTarea_%i", pTlcut);
 /*
  hname=Form("delta_pt_BG_pyt_%i", pTlcut);
  delta_pt_BG_pyt[pTlcut] = new TH2D(hname,"delta pT for BG corrections, using pythia probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  hname=Form("delta_pt_dete_%i", pTlcut);
  delta_pt_dete[pTlcut] = new TH2D(hname,"delta pT for detector corrections", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  hname=Form("delta_pt_BG_dete_%i", pTlcut);
  delta_pt_BG_dete[pTlcut] = new TH2D(hname,"delta pT for BG and detector corrections", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  hname=Form("heffi_BG_dete_%i", pTlcut);
  heffi_BG_dete[pTlcut]=new TH1D(hname, "jet reco. efficiency after detector and BG corrections",nptbins, ptminbin, ptmaxbin);
*/
  //hname=Form("delta_pt_inplane_%i", pTlcut);
  //delta_pt_inplane[pTlcut] = new TH2D(hname,"delta pT in event plane, using sp probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
  //hname=Form("delta_pt_outplane_%i", pTlcut);
  //delta_pt_outplane[pTlcut] = new TH2D(hname,"delta pT out of event plane, using sp probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
}

	TH1D *hjetarea = new TH1D("hjetarea","jet area",100,0,1);
	TH2D *hjetpTarea = new TH2D("hjetpTarea","jet pTmeasured vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
	TH2D *hjetpTcorrArea = new TH2D("hjetpTcorrArea","jet pTreco vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
	TH2D *hjetpTembArea = new TH2D("hjetpTembArea","jet pTemb vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
	TH1D *hrho = new TH1D("hrho","rho",50,0,50);
   TH3D *hdpTarea= new TH3D(hname,"delta pT vs area",nptbins, ptminbin, ptmaxbin,100,0,1,nptbins, ptminbin, ptmaxbin);

/*
  TH2D *hdpT_area[nemb];
  TH2D *hdpT_pTlead[nemb];
  TH2D *hdpT_refmult[nemb];
  TH2D *hdpT_rho[nemb];
  TH2D *hdpT_day[nemb];
  TH2D *hdpT_eta[nemb];
  TH2D *hdpT_phi[nemb];
  TH1D *hdpT_cut0[nemb];
  TH1D *hdpT_cut1[nemb];
  TH1D *hdpT_cut2[nemb];
  TH1D *hdpT_cut3[nemb];
  TH1D *hdpT_pyth_cut3[nemb];
  TH1D *hdpT_pythCorr_cut3[nemb];
  TH1D *hdpT_pyth_pythCorr[nemb];
  TH1D *hdpT_sp_pyth[nemb];
  TH1D *heffi;

  for(Int_t iemb=0; iemb<nemb;iemb++){
		bname=Form("hdpT%.1lf",fEmbPt[iemb]);
		hname=Form("%s_area",bname.Data());
		hdpT_area[iemb] = new TH2D(hname,"deltapT_vs_area;#deltap_{T};area (sr)",nptbins,ptminbin,ptmaxbin,20,0,2);
		hname=Form("%s_pTlead",bname.Data());
		hdpT_pTlead[iemb] = new TH2D(hname,"deltapT_vs_pTlead;#deltap_{T};p_{T}^{leading} (sr)",nptbins,ptminbin,ptmaxbin,100,0,50);
		hname=Form("%s_refmult",bname.Data());
		hdpT_refmult[iemb] = new TH2D(hname,"deltapT_vs_refmult;#deltap_{T}; refmult",nptbins,ptminbin,ptmaxbin,60,200,800);
		hname=Form("%s_rho",bname.Data());
		hdpT_rho[iemb] = new TH2D(hname,"deltapT_vs_rho;#deltap_{T}; #rho",nptbins,ptminbin,ptmaxbin,70,0,70);
		hname=Form("%s_day",bname.Data());
		hdpT_day[iemb] = new TH2D(hname,"deltapT_vs_day;#deltap_{T}; day",nptbins,ptminbin,ptmaxbin,90,0.5,90.5);
		hname=Form("%s_eta",bname.Data());
		hdpT_eta[iemb] = new TH2D(hname,"deltapT_vs_eta;#deltap_{T}; #eta",nptbins,ptminbin,ptmaxbin,100,-1,1);
		hname=Form("%s_phi",bname.Data());
		hdpT_phi[iemb] = new TH2D(hname,"deltapT_vs_phi;#deltap_{T}; #phi",nptbins,ptminbin,ptmaxbin,60, -1*TMath::Pi(), TMath::Pi());
		hname=Form("%s_cut0",bname.Data());
		hdpT_cut0[iemb] = new TH1D(hname,"deltapT;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut1",bname.Data());
		hdpT_cut1[iemb] = new TH1D(hname,"deltapT without bad sector;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut2",bname.Data());
		hdpT_cut2[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_cut3",bname.Data());
		hdpT_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pyth_cut3",bname.Data());
		hdpT_pyth_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R (pythia);#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pythCorr_cut3",bname.Data());
		hdpT_pythCorr_cut3[iemb] = new TH1D(hname,"deltapT w/o bad sector+edge1R (eff. corr. pythia);#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_pyth_pythCorr",bname.Data());
		hdpT_pyth_pythCorr[iemb] = new TH1D(hname,"pythia - pythiacorr;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname=Form("%s_sp_pyth",bname.Data());
		hdpT_sp_pyth[iemb] = new TH1D(hname,"sp - pythia;#deltap_{T}",nptbins,ptminbin,ptmaxbin);
		hname="heffi";
		heffi = new TH1D(hname,"jet reco efficiency;#p_{T};efficiency",200,-100,100);
		}      
*/
  //------------------------------

	//define bad TPC sectors, bad days, etc.
	checker * eventCheck = new checker();
   //eventCheck->addBadSector(-1,1,-0.78,-0.26);
  //------------------------------	
   Int_t njets=0; //total number of embedded pythia jets

  for(Int_t ievt = 0; ievt < ftreejets->GetEntries(); ievt++)
    {
		if(ievt%1000==0)cout<<"Processing event "<<ievt<<endl;
      ftreejets->GetEntry(ievt);
      
		Int_t day=(runid/1000)-((year+1)*1000);

      //bool badDay=eventCheck->isBadDay(day);
		//if(badDay) continue;

		Double_t rpanglerad=(Double_t) (rpangle*TMath::Pi())/180.0;

      hevts->Fill(1,weight);

      for(Int_t ejet = 0; ejet < embedding_arr->GetEntries(); ejet++) //loop over cycles of embedding (usually only 1)
		{
	   embedding *embeddedjet = (embedding*)embedding_arr->At(ejet);
/*	   embedding *embjet_pyth_part = (embedding*)emb_pythia_part_arr->At(ejet);
	   //embedding *embjet_pyth_dete = (embedding*)emb_pythia_dete_arr->At(ejet);
	   embedding *embjet_pyth_dete2 = (embedding*)emb_pythia_dete_arr2->At(ejet);
	   embedding *embjet_pyth_dete_rec = (embedding*)emb_pythia_dete_rec_arr->At(ejet);
	   embedding *embjet_pyth_dete_rec2 = (embedding*)emb_pythia_dete_rec_arr2->At(ejet);*/

      //was the jet found in this event?
      bool found_jet_sp=embeddedjet->ffoundJet;
/*      bool found_jet_pp=embjet_pyth_part->ffoundJet;
      //bool found_jet_pd=embjet_pyth_dete->ffoundJet;
      bool found_jet_pd2=embjet_pyth_dete2->ffoundJet;
      bool found_jet_pdr=embjet_pyth_dete_rec->ffoundJet;
      bool found_jet_pdr2=embjet_pyth_dete_rec2->ffoundJet;*/

      //pT, eta, phi of the embedded jet (should be same for sp, pp, pd, pdr jets)
      Double_t pTemb=embeddedjet->fPtEmb;
/*		Double_t pT_pd2_emb=embjet_pyth_dete2->fPtEmb; //should be same as pT_pp = pTemb
		Double_t pT_pdr_emb=embjet_pyth_dete_rec->fPtEmb; //should be same as pT_pd = pTemb
		Double_t pT_pdr2_emb=embjet_pyth_dete_rec2->fPtEmb; //should be same as pT_pd2 != pTemb*/

      Double_t etaemb=embeddedjet->fetaEmb;
      Double_t phiemb=embeddedjet->fphiEmb;
      
		Double_t angle=TMath::Abs(phiemb-rpanglerad);
         if(angle>TMath::Pi())angle=2.*TMath::Pi()-angle;

		//area of the reconstructed jets
		Double_t area_sp=embeddedjet->fAreaEmb;
		Double_t rho_sp=embeddedjet->fRho;
		/*Double_t area_pp=embjet_pyth_part->fAreaEmb;
		Double_t area_pd=embjet_pyth_dete->fAreaEmb;*/
/*		Double_t area_pdr=embjet_pyth_dete_rec->fAreaEmb;
		Double_t area_pdr2=embjet_pyth_dete_rec2->fAreaEmb;
*/
		//pTjet
      Double_t pT_sp=embeddedjet->fPtEmbReco; //single particle jet pT, should be same as pTemb
/*      Double_t pT_pp=embjet_pyth_part->fPtEmbReco; //pythia particle level pT (no BG)
      Double_t pT_pd=pTemb; //pythia detector level jet pT (no BG), scaled so pTjet = pTemb
      Double_t pT_pd2=embjet_pyth_dete2->fPtEmbReco; //pythia detector level jet pT (no BG)
      Double_t pT_pdr=embjet_pyth_dete_rec->fPtEmbReco; //pythia detector level jet pT (with BG), scaled so pTjet = pTemb
      Double_t pT_pdr2=embjet_pyth_dete_rec2->fPtEmbReco; //pythia detector level jet pT (with BG)
*/
		//pTjet-rho*A
      Double_t pT_corr_sp=embeddedjet->fPtEmbCorr;
      //Double_t pT_corr_pp=embjet_pyth_part->fpTEmbCorr; //these are jets w/o BG -> no corrected pT needed
      //Double_t pT_corr_pd=embjet_pyth_dete->fpTEmbCorr; //these are jets w/o BG -> no corrected pT needed
  /*    Double_t pT_corr_pdr=embjet_pyth_dete_rec->fPtEmbCorr;
      Double_t pT_corr_pdr2=embjet_pyth_dete_rec2->fPtEmbCorr;
*/
		//dR=Sqrt((etaRec-etaEmb)^2+(phiRec-phiEmb)^2)
		Double_t dR_sp=embeddedjet->fdR;
		/*Double_t dR_pp=embjet_pyth_part->fdR;
		Double_t dR_pd=embjet_pyth_dete->fdR;*/
/*		Double_t dR_pdr=embjet_pyth_dete_rec->fdR;
		Double_t dR_pdr2=embjet_pyth_dete_rec2->fdR;
*/
		//pT leading 
		Double_t pTlead_sp=embeddedjet->fPtLeading;
/*		Double_t pTlead_pp=embjet_pyth_part->fPtLeading;
		Double_t pTlead_pd2=embjet_pyth_dete2->fPtLeading;
		Double_t pTlead_pdr2=embjet_pyth_dete_rec2->fPtLeading;
		Double_t pTlead_pdr=embjet_pyth_dete_rec->fPtLeading;
*/		

		//pT of embedded particles in the reconstructed jet
      Double_t pT_part_sp=embeddedjet->fPtEmbPart; 
      /*Double_t pT_part_pp=embjet_pyth_part->fPtEmbPart;
      Double_t pT_part_pd=embjet_pyth_dete->fPtEmbPart;*/
  /*    Double_t pT_part_pdr=embjet_pyth_dete_rec->fPtEmbPart;
      Double_t pT_part_pdr2=embjet_pyth_dete_rec2->fPtEmbPart;
*/
		//delta pT
		Double_t dpT_sp=embeddedjet->fdeltapT;
	
		
  		bool badSector=eventCheck->isNearBadSector(etaemb, phiemb, 1*r); //remove jets bordering with the bad sector
//		if(badSector) continue;

		//FILL HISTOS

		if(!found_jet_sp)continue;
		if(ejet==0)	hrho->Fill(rho_sp,weight);

			hjetarea->Fill(area_sp,weight);
			hjetpTarea->Fill(pT_sp,area_sp,weight);
			hjetpTembArea->Fill(pTemb,area_sp,weight);
			hjetpTcorrArea->Fill(pT_corr_sp,area_sp,weight);
			hdpTarea->Fill(dpT_sp,area_sp,pTemb,weight);

	   eta_phi_emb->Fill(etaemb,phiemb,weight);		

	for(Int_t pTlcut=0; pTlcut<npTlead; pTlcut++){
		if(pTlead_sp<pTlcut) continue;
	//fill BG_sp deltapT
		if(area_sp>acut){
			delta_pt_BG_sp[pTlcut]->Fill(pTemb,dpT_sp,weight);
	/*		if((angle<TMath::Pi()/4.) || (angle>TMath::Pi()*3./4.))	
				delta_pt_inplane[pTlcut]->Fill(pTemb,dpT_sp);
			else
				delta_pt_outplane[pTlcut]->Fill(pTemb,dpT_sp);*/
		}
	}//pTlead lop

    }//jet loop
}//event loop

  foutput->cd();
  foutput->Write();

  foutput->Close();
  delete foutput;

  finput->Close();
  delete finput;

  timer.Stop();
  timer.Print();
}
