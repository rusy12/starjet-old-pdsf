#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "stdhistograms.h"
#include "ppauau.h"
#include "randomcones.h"

#include <iostream>
using namespace std;

void hjet(Long64_t nev)
{
  TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");

  TString strigger = gSystem->Getenv("TRIGGER");
  double r = atof(gSystem->Getenv("RPARAM"));
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));

  cout << Form("[i] Input=%s Output=%s", dataDir.Data(), outDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Trigger=%s", r, strigger.Data()) << endl;
  
  TString sInFileList = gSystem->Getenv("FILELISTFILE");

  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));

  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  if (strigger == "MB" || strigger == "HT")
    {
      cout << "[i] RefMultCut set for all centralities" << endl;
      evCuts->SetRefMultCut(0);
    }

  if (strigger.Contains("pp"))
    {
      cout << "[i] Setting z cut to 30." << endl;
      evCuts->SetVertexZCut(30.); // gRefMult: 399 >= := 10% central Au+Au    
      evCuts->SetRefMultCut(0.);
    }

  readerdata.GetTrackCuts()->SetDCACut(1.);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(20);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  readerdata.SetApplyFractionHadronicCorrection(kFALSE);

  TString str = "null";
  str = Form("%s/recoiljet_R%.1lf.root", outDir.Data(), r);

  int runid = 0;
  int refmult = 0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  fourvector *trig4vec = new fourvector();
  TClonesArray *recoilarr = new TClonesArray("jet", 10);

  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TTree* ftreejets = new TTree("RecoilJets","Recoil Jets");
  TBranch *br_runid = ftreejets->Branch("runid", &runid);
  TBranch *br_refmult = ftreejets->Branch("refmult", &refmult);
  TBranch *br_rho = ftreejets->Branch("rho", &rho);
  TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
  TBranch *br_rpangle = ftreejets->Branch("rpangle", &rpangle);
  TBranch *br_recoil = ftreejets->Branch("akt_recoil", &recoilarr);
  TBranch *br_trig4vec = ftreejets->Branch("htrig", &trig4vec);

  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nev);
    
  Int_t evt = 0;

  std::vector<fastjet::PseudoJet> input_vector;

  while (readerdata.NextEvent() == kTRUE)
    {
      recoilarr->Delete();
      trig4vec->SetPtEtaPhiM(0, 0, 0, 0);
      TStarJetVectorContainer<TStarJetVector>* containerdata = readerdata.GetOutputContainer();
      
      if(charged)
	input_vector = make_charged_input_vector(containerdata, 0.2);
      else
	input_vector = make_input_vector(containerdata, 0.2);

      std::vector<fastjet::PseudoJet> trigger_vector = make_HT_trigger_vector(containerdata, 0.2);
      
      if (input_vector.size() <= 0) continue;

      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      runid = header->GetRunId();
      rpangle = header->GetReactionPlaneAngle();
      refmult = header->GetReferenceMultiplicity();


      FJWrapper kt_data;
      kt_data.r = r;
      kt_data.maxrap = 1.;
      kt_data.algor = fastjet::kt_algorithm;
      kt_data.input_particles = input_vector;
      kt_data.Run();

      kt_data.GetMedianAndSigma(rho, sigma);
      
      FJWrapper akt_data;
      akt_data.r = r;
      akt_data.maxrap = 1.;
      akt_data.algor = fastjet::antikt_algorithm;
      akt_data.input_particles = input_vector;
      akt_data.Run();


      // finding trigger hadron
      TLorentzVector htrig(0, 0, 0, 0);
      for(Int_t ipart = 0; ipart < (Int_t)trigger_vector.size(); ipart++)
	{
	  Double_t pT = trigger_vector[ipart].perp();
	  if(pT > htrig.Pt())
	    {
	      Double_t eta = trigger_vector[ipart].eta();
	      Double_t phi = trigger_vector[ipart].phi();
	      Double_t M = trigger_vector[ipart].m();
	      
	      htrig.SetPtEtaPhiM(pT, eta, phi, M);
	    }
	}

      trig4vec->SetPtEtaPhiM(htrig.Pt(), htrig.Eta(), htrig.Phi(), htrig.M());

      // finding recoil jet
      Int_t goodjet = 0;
      TLorentzVector jetlv(0, 0, 0, 0);

      std::vector<fastjet::PseudoJet> jets = akt_data.inclusive_jets;
      for(Int_t ijet = 0; ijet < (Int_t)jets.size(); ijet++)
	{
	  Double_t jphi = jets[ijet].phi();
	  Double_t jeta = jets[ijet].eta();
	  Double_t jpT = jets[ijet].perp();
	  Double_t jM = jets[ijet].m();
	  Double_t area = akt_data.clust_seq->area(jets[ijet]);

	  if(TMath::Abs(jeta) > akt_data.maxrap - r) continue;

	  Int_t N = akt_data.clust_seq->constituents(jets[ijet]).size();
	      
	  std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[ijet]));

	  new ((*recoilarr)[goodjet]) jet();
	  jet *recoiljet = (jet*)recoilarr->At(goodjet);

	  recoiljet->jet_fv.SetPtEtaPhiM(jpT, jeta, jphi, jM);
	  recoiljet->Nconst = N;
	  recoiljet->area = area;
	  recoiljet->pTleading = constituents[0].perp();
	      
	  goodjet++;
	}
      
      foutput->cd();
      ftreejets->Fill();
      readerdata.PrintStatus(60); //every 1min 

      evt++;
    }
  foutput->cd();
  ftreejets->Write();
  foutput->Close();
      

  readerdata.PrintStatus();
}
