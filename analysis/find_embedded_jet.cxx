#include "TMath.h"
#include "TLorentzVector.h"

#include "find_embedded_jet.h"

#include "fjwrapper.h"
#include <fastjet/PseudoJet.hh>

#include <iostream>
using namespace std;

//____________________________________________________________________________
bool find_jet_E(FJWrapper *jetreco, embedding *embedding_data, Double_t pTemb, Double_t etaemb, Double_t phiemb, Double_t rho)
{
//cout<<"jet matching"<<endl;
  Bool_t jetFound = kFALSE;
  if(phiemb<0)phiemb=phiemb+2*TMath::Pi(); 
  std::vector<fastjet::PseudoJet> jets = jetreco->inclusive_jets;
	
  for(Int_t ijet = 0; ijet < (Int_t)jets.size(); ijet++)
    {
      Double_t eta_jet = jets[ijet].eta();
      Double_t phi_jet = jets[ijet].phi();
	   if(phi_jet<0)phi_jet=phi_jet+2*TMath::Pi(); 
      if(TMath::Abs(eta_jet) > jetreco->maxrap - jetreco->r) continue;

	   Double_t deta2=(eta_jet-etaemb)*(eta_jet-etaemb);
      Double_t dphi2=(phi_jet-phiemb)*(phi_jet-phiemb);    

      Int_t N = jetreco->clust_seq->constituents(jets[ijet]).size();
      std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(jetreco->clust_seq->constituents(jets[ijet]));      
	      
      TLorentzVector foundlv(0., 0., 0., 0.);
      TLorentzVector constlv(0., 0., 0., 0.);

      for(Int_t iConst = 0; iConst < N; iConst++)
	if(constituents[iConst].user_index() >= 90000) //embedded particles
	  {
	    Double_t pT = constituents[iConst].perp();
	    Double_t eta = constituents[iConst].eta();
	    Double_t phi = constituents[iConst].phi();
	    if(phi<0)phi=phi+2*TMath::Pi(); 
	    Double_t M = constituents[iConst].m();
	    constlv.SetPtEtaPhiM(pT, eta, phi, M);
	    foundlv += constlv;
	  }
      //cout<<"pT matched:"<<foundlv.Pt()<<" pTemb:"<<pTemb<<endl;
      if(foundlv.Pt() / pTemb > 0.5)
	{
	  embedding_data->fPtEmb = pTemb;
	  embedding_data->fetaEmb = etaemb;
	  embedding_data->fphiEmb = phiemb;
     embedding_data->fetaEmbReco = eta_jet;
     embedding_data->fphiEmbReco = phi_jet;
	  embedding_data->fPtEmbReco = jets[ijet].perp();
     embedding_data->fPtEmbPart=foundlv.Pt();
	  embedding_data->fAreaEmb = jetreco->clust_seq->area(jets[ijet]);
	  embedding_data->fRho=rho;
     embedding_data->fdeltapT = embedding_data->fPtEmbReco - rho * embedding_data->fAreaEmb - embedding_data->fPtEmb;
	  embedding_data->fPtEmbCorr = embedding_data->fPtEmbReco - rho * embedding_data->fAreaEmb; 
     embedding_data->fdR=TMath::Sqrt(deta2+dphi2);
	  embedding_data->ffoundJet = kTRUE;
     jetFound = kTRUE;
	  embedding_data->fPtLeading = constituents[0].perp();
	  break;
	}
    }
return jetFound;
}

//____________________________________________________________________________
bool find_jet_sp(FJWrapper *jetreco, embedding *embedding_data, Double_t pTemb, Double_t etaemb, Double_t phiemb, Double_t rho)
{
  Bool_t jetFound = kFALSE;
  std::vector<fastjet::PseudoJet> jets = jetreco->inclusive_jets;
  if(phiemb<0)phiemb=phiemb+2*TMath::Pi(); 
	
  for(Int_t ijet = 0; ijet < (Int_t)jets.size(); ijet++)
    {
      Double_t eta_jet = jets[ijet].eta();
      Double_t phi_jet = jets[ijet].phi();
	   if(phi_jet<0)phi_jet=phi_jet+2*TMath::Pi(); 
	   if(phiemb<0)phiemb=phiemb+2*TMath::Pi(); 
      if(TMath::Abs(eta_jet) > jetreco->maxrap - jetreco->r) continue;

      Double_t deta2=(eta_jet-etaemb)*(eta_jet-etaemb); 
		Double_t dphi = TMath::Abs(phi_jet-phiemb);
		if(dphi>TMath::Pi())dphi=(2*TMath::Pi())-dphi;
      Double_t dphi2=dphi*dphi;
      //if(TMath::Sqrt(deta2+dphi2)>R)continue; //not necessary for sp probe
//cout<<"dR: "<<TMath::Sqrt(deta2+dphi2)<<endl;
		Int_t N = jetreco->clust_seq->constituents(jets[ijet]).size();
      std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(jetreco->clust_seq->constituents(jets[ijet]));      
		TLorentzVector foundlv(0., 0., 0., 0.);
      TLorentzVector constlv(0., 0., 0., 0.);
		
		bool embedded=false;
      for(Int_t iConst = 0; iConst < N; iConst++)
 		if(constituents[iConst].user_index() >= 90000) //embedded particles
     {
		 embedded=true;
       Double_t pT = constituents[iConst].perp();
       Double_t eta = constituents[iConst].eta();
       Double_t phi = constituents[iConst].phi();
	    if(phi<0)phi=phi+2*TMath::Pi(); 
       Double_t M = constituents[iConst].m();
       constlv.SetPtEtaPhiM(pT, eta, phi, M);
       foundlv += constlv;
     }

	if(embedded){		
//fill the embedding container
     embedding_data->fPtEmb = pTemb;
     embedding_data->fetaEmb = etaemb;
     embedding_data->fphiEmb = phiemb;
     embedding_data->fetaEmbReco = eta_jet;
     embedding_data->fphiEmbReco = phi_jet;
     embedding_data->fPtEmbReco = jets[ijet].perp();
     embedding_data->fPtEmbPart=foundlv.Pt();
     embedding_data->fAreaEmb = jetreco->clust_seq->area(jets[ijet]);
     embedding_data->fdeltapT = embedding_data->fPtEmbReco - rho * embedding_data->fAreaEmb - embedding_data->fPtEmb;
	  embedding_data->fPtEmbCorr = embedding_data->fPtEmbReco - rho * embedding_data->fAreaEmb; 
     embedding_data->fdR=TMath::Sqrt(deta2+dphi2);
     embedding_data->ffoundJet = kTRUE;
     jetFound = kTRUE;
     embedding_data->fPtLeading = constituents[0].perp();
     break;
		}
	}
return jetFound;
}
