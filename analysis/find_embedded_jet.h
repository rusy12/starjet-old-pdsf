#ifndef __find_embedded_jet_hh
#define __find_embedded_jet_hh

#include "embedding.h"

class FJWrapper;

bool find_jet_E(FJWrapper *jetreco, embedding *embedding_data, Double_t pTemb, Double_t etaemb, Double_t phiemb, Double_t rho);

bool find_jet_sp(FJWrapper *jetreco, embedding *embedding_data, Double_t pTemb, Double_t etaemb, Double_t phiemb, Double_t rho);
#endif
