#include "checker.h"
#include <TMath.h>
#include <iostream>
using namespace std;

//_______________________________________
checker::checker()
{
fnSectors=0;
for(Int_t i=0;i<fnSectorsMax;i++)
	{
		fphiMinCut[i]=0;
		fphiMaxCut[i]=0;
		fetaMinCut[i]=0;
		fetaMaxCut[i]=0;
	}
for(Int_t i=0;i<fnBadDaysMax;i++)
	{
		fBadDay[i]=-1;
	}
}

//_______________________________________
checker::~checker()
{
}

//_______________________________________
void checker::addBadSector(Float_t eta1, Float_t eta2, Float_t phi1, Float_t phi2)
{
if(phi1<0)phi1=phi1+2*TMath::Pi();
if(phi2<0)phi2=phi2+2*TMath::Pi();
	fphiMinCut[fnSectors]=phi1;
	fphiMaxCut[fnSectors]=phi2;
	fetaMinCut[fnSectors]=eta1;
	fetaMaxCut[fnSectors]=eta2;
cout<<"[i] added bad sector: "<<fetaMinCut[fnSectors]<<" "<<fetaMaxCut[fnSectors]<<" "<<fphiMinCut[fnSectors]<<" "<<fphiMaxCut[fnSectors]<<endl;
	fnSectors++;
	return;
}

//_______________________________________
bool checker::isBadSector(Float_t eta, Float_t phi)
{
	if(phi<0)phi=phi+2*TMath::Pi();
	bool isBad=false;
	for(int i=0; i<fnSectors; i++)
 		{
		if(phi> fphiMinCut[i] && phi<fphiMaxCut[i] && eta<fetaMaxCut[i] && eta>fetaMinCut[i])
			{
				isBad=true;
   			return isBad;
			}

		}//for loop
	return isBad;
}

//_______________________________________
bool checker::isNearBadSector(Float_t eta, Float_t phi, Float_t band)
{
	if(phi<0)phi=phi+2*TMath::Pi();
   bool isBad=kFALSE;
   for(int i=0; i<fnSectors; i++)
      {
      Double_t phiMin=fphiMinCut[i]-band;
      Double_t phiMax=fphiMaxCut[i]+band;
      Double_t phiMax2=-1; // for underflow/overflow cases
		Double_t phiMin2=99; // for underflow/overflow cases
		if(phiMin<0)phiMin=phiMin+2*TMath::Pi();
		if(phiMax>2*TMath::Pi()){
			phiMax2=phiMax-2*TMath::Pi();
			phiMin2=0; 
		}
		
//cout<<"[bad sector] phiMin "<<phiMin<<"phiMax" <<phiMax<<endl;
      if(((phi>= phiMin && phi<=phiMax) ||  (phi>= phiMin2 && phi<=phiMax2)) && eta<(fetaMaxCut[i]+band) && eta>(fetaMinCut[i]-band))
         {
				//cout<<"[bad sector] hit in eta "<<eta<<" "<<phi<<" exluded"<<endl;
            isBad=kTRUE;
   			return isBad;
         }
//		else 
				//cout<<"[bad sector] hit in eta "<<eta<<" "<<phi<<" accepted"<<endl;
      }//for loop
   return isBad;
}
//_______________________________________
void checker::addBadDay(Int_t day)
{
	fBadDay[fnBadDays]=day;
	fnBadDays++;
	return;
}

//_______________________________________
bool checker::isBadDay(Int_t day)
{
   bool isBad=false;
   for(int i=0; i<fnBadDays; i++)
      {
      if(day==fBadDay[i])
         {
            isBad=true;
   			return isBad;
         }

      }//for loop
   return isBad;
}

//_______________________________________

bool checker::isBadRun(Int_t runid)
{
	const Int_t bad_run_list_200GeV[179] = {12126101,12127003,12127017,12127018,12127032,12128025,12132043,12133018,12134023,12136005,12136006,12136014,12136017,12136022,12136023,12136024,12136025,12136027,12136028,12136029,12136030,12136031,12136034,12136054,12138017,12138021,12138081,12138082,12139006,12139007,12139015,12139016,12139028,12139059,12139075,12139076,12139077,12139078,12139079,12139080,12140010,12140011,12140012,12140013,12140014,12140015,12140016,12140018,12140019,12140020,12140021,12140025,12140026,12140027,12140028,12140029,12140042,12140051,12140052,12140053,12140054,12140055,12140056,12140064,12140066,12140067,12141001,12141002,12141003,12141004,12141005,12141006,12141009,12141014,12141015,12141016,12141017,12141018,12141019,12141026,12141027,12141028,12141029,12141030,12141032,12141033,12141034,12141035,12141036,12141041,12141042,12141043,12141044,12141045,12141046,12141048,12141050,12141051,12141052,12141056,12141059,12141060,12141061,12141062,12141063,12141064,12141065,12141066,12141067,12141071,12141072,12142001,12142002,12142003,12142006,12142013,12142014,12142015,12142016,12142017,12142018,12142019,12142020,12142021,12142022,12142023,12142026,12142027,12142032,12142033,12142034,12142046,12142047,12142048,12142049,12142050,12142051,12142061,12142062,12142063,12142076,12142077,12143016,12143018,12143054,12143075,12144001,12144002,12144013,12144014,12144027,12144028,12157038,12157051,12158040,12158041,12158054,12158056,12158057,12162055,12162056,12162057,12162058,12164037,12164078,12164079,12166002,12166003,12167015,12167024,12167052,12168002,12168009,12168022,12168077,12170044,12170045,12170054,12170056};
   bool isBad=false;
   for(int i=0; i<fnBadRuns; i++)
      {
      if(runid==bad_run_list_200GeV[i])
         {
            isBad=true;
            return isBad;
         }

      }//for loop
   return isBad;
}
