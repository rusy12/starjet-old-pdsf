#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "StRefMultCorr.h"
#include <TStarJetPicoPrimaryTrack.h>


//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void inclusive(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  double r = atof(gSystem->Getenv("RPARAM"));
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcutMin = atoi(gSystem->Getenv("REFMULTMIN"));
  Int_t refmultcutMax = atoi(gSystem->Getenv("REFMULTMAX"));
  Int_t njetsremove=atoi(gSystem->Getenv("NJETSREMOVE"));

	//float rkt=r; //R size for kT jets (for rho calculation)
	float rkt=0.4; //R size for kT jets (for rho calculation)

  //Int_t zetcut = atoi(gSystem->Getenv("ZVERTEX"));
  //Int_t doEffiCorr = atoi(gSystem->Getenv("DOEFFICORR"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Trigger=%s", r, strigger.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
  

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  evCuts->SetVertexZCut(30);//[cm] 
  //evCuts->SetRefMultCut(refmultcut);

  readerdata.GetTrackCuts()->SetDCACut(1.);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(14);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy

  Float_t pTmin=0.2; //TPC track min pT 
  Float_t pTmax=30.0; //TPC track max pT


  TString str = "null";
  str = Form("%s/inclusivejet_R%.1lf.root", outDir.Data(), r);

  int runid = 0;
  int refmult = 0;
  int refmultCor = 0;
  int zdcrate = 0;
  Double_t zvertex = 0;   
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  //fourvector *trig4vec = new fourvector();
  TClonesArray *inclusivearr = new TClonesArray("jet", 10);

  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TTree* ftreejets = new TTree("InclusiveJets","Inclusive Jets");
  TBranch *br_runid = ftreejets->Branch("runid", &runid);
  //TBranch *br_refmult = ftreejets->Branch("refmult", &refmult);
  TBranch *br_refmult = ftreejets->Branch("refmult", &refmultCor);
  TBranch *br_zvertex = ftreejets->Branch("zvertex", &zvertex);
  TBranch *br_rho = ftreejets->Branch("rho", &rho);
  TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
  TBranch *br_rpangle = ftreejets->Branch("rpangle", &rpangle);
  TBranch *br_inclusive = ftreejets->Branch("akt_inclusive", &inclusivearr);

TH2D *htptfrac_jpt= new TH2D("htptfrac_jpt","rltv_trackpT_vs_jetpT",20,0,1,50,0,50);
TH2D *htptfrac_ntr=new TH2D("htptfrac_ntr","rltv_trackpT_vs_ntracks",20,0,1,40,0.5,40.5);
TH2D *hjetpt_ntr=new TH2D("hjetpt_ntr","jetpT_vs_ntracks",50,0,50,40,0.5,40.5);
TH1D *htrackpT=new TH1D("htrackpT", "track pT; pT [GeV/c]",50,0,50);
TH1I *hevents=new TH1I("hevents", "nevents",1,0,2);

TH2D *refmult_day=new TH2D("refmult_day","refMultCorr_vs_day",500,0.5,500.5,200,0.5,200.5);

  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nev);

	StRefMultCorr* refmultCorrUtil  = new StRefMultCorr("refmult");
    
  Int_t evt = 0;

  std::vector<fastjet::PseudoJet> input_vector;

int Ntracks=0;
float MeanpT=0;

  while (readerdata.NextEvent() == kTRUE)
    {
      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      runid = header->GetRunId();
      rpangle = header->GetReactionPlaneAngle();
      refmult = header->GetReferenceMultiplicity();
		zvertex = header->GetPrimaryVertexZ();
		zdcrate = header->GetZdcCoincidenceRate();

		refmultCorrUtil->init(runid);
   	if(refmultCorrUtil->isBadRun(runid)) continue;
      refmultCorrUtil->initEvent(refmult, zvertex, zdcrate) ;
		refmultCor = refmultCorrUtil->getRefMultCorr() ;
		if(refmultCor<=refmultcutMin || refmultCor>refmultcutMax)continue;

		Int_t day = (Int_t)(runid/1000)-12000;
		refmult_day->Fill(refmultCor,day);

hevents->Fill(1);

      inclusivearr->Delete();
      //trig4vec->SetPtEtaPhiM(0, 0, 0, 0);
      TStarJetVectorContainer<TStarJetVector>* containerdata = readerdata.GetOutputContainer();
      
      if(charged){
			/*if(doEffiCorr)
			input_vector = make_efficorrected_charged_input_vector(containerdata, 0.2);
			else*/
			input_vector = make_charged_input_vector(containerdata, pTmin, pTmax);
		}
      else
	input_vector = make_input_vector(containerdata, pTmin, pTmax);

      //std::vector<fastjet::PseudoJet> trigger_vector = make_HT_trigger_vector(containerdata, 0.2);
      
      if (input_vector.size() <= 0) continue;

	   //Int_t eventid=header->GetEventId();
	   //cout<<"runid: "<<runid<<" eventid: "<<eventid<<endl;

//Primary tracks
      TList*  primTracks          = readerdata.GetListOfSelectedTracks();

       TIter nextTrack(primTracks);
       double TrackPx;
       double TrackPy;
       double TrackPz;
       double TrackPt;
       double TrackEta;
       double TrackPhi;
       double EtaDiff;
       double PhiDiff;
       double TracksDCAxy;
       double NFitHits;

       TStarJetPicoPrimaryTrack* primTrack;
int ntracks=0;
float meanpT=0;
       while(primTrack = (TStarJetPicoPrimaryTrack*)nextTrack()  )
       {
         TrackPx      = primTrack->GetPx();
         TrackPy      = primTrack->GetPy();
         TrackPz      = primTrack->GetPz();
         TrackPt      = TMath::Sqrt(TrackPx*TrackPx+TrackPy*TrackPy);
         TrackEta     = primTrack->GetEta();
         TrackPhi     = primTrack->GetPhi();
         TracksDCAxy  = primTrack->GetsDCAxy();
         EtaDiff      = primTrack->GetEtaDiffHitProjected();
         PhiDiff      = primTrack->GetPhiDiffHitProjected();
         NFitHits     = primTrack->GetNOfFittedHits();
			//if(TrackPt<0.22 && TrackPt>0.2)
			//cout<<"pT: "<<TrackPt<<" eta: "<<TrackEta<<" phi: "<<TrackPhi<<" DACxy: "<<TracksDCAxy<<" NFit: "<<NFitHits<<endl;

htrackpT->Fill(TrackPt);
		ntracks++;
		meanpT+=TrackPt;
		}
//cout<<"refmult: "<<refmultCor<<" n tracks: "<<ntracks<<endl;
meanpT=meanpT/ntracks;

MeanpT+=meanpT;
Ntracks+=ntracks;

      FJWrapper kt_data;
      kt_data.r = rkt;
      kt_data.maxrap = max_rap;
      kt_data.algor = fastjet::kt_algorithm;
      kt_data.ghost_area = 0.01;
      kt_data.input_particles = input_vector;
      kt_data.Run();

		std::vector<fastjet::PseudoJet> ktjets = kt_data.inclusive_jets;
      Int_t njets=ktjets.size();
		if(njets>njetsremove)
      kt_data.GetMedianAndSigma(rho, sigma, njetsremove);
		else
		kt_data.GetMedianAndSigma(rho, sigma,0);      

      FJWrapper akt_data;
      akt_data.r = r;
      akt_data.maxrap = max_rap;
      akt_data.algor = fastjet::antikt_algorithm;
      akt_data.ghost_area = 0.01;
      akt_data.input_particles = input_vector;
      akt_data.Run();


      // saving jets
      Int_t goodjet = 0;
      TLorentzVector jetlv(0, 0, 0, 0);

      std::vector<fastjet::PseudoJet> jets = akt_data.inclusive_jets;
      for(Int_t ijet = 0; ijet < (Int_t)jets.size(); ijet++)
	{
	  Double_t jphi = jets[ijet].phi();
	  Double_t jeta = jets[ijet].eta();
	  Double_t jpT = jets[ijet].perp();
	  Double_t jM = jets[ijet].m();
	  Double_t area = akt_data.clust_seq->area(jets[ijet]);

	  if(TMath::Abs(jeta) > 1.0 - r) continue;

	  Int_t N = akt_data.clust_seq->constituents(jets[ijet]).size();
	      
	  std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[ijet]));

	  new ((*inclusivearr)[goodjet]) jet();
	  jet *inclusivejet = (jet*)inclusivearr->At(goodjet);

	  inclusivejet->jet_fv.SetPtEtaPhiM(jpT, jeta, jphi, jM);
	  inclusivejet->Nconst = N;
	  inclusivejet->area = area;
	  inclusivejet->pTleading = constituents[0].perp();


//jet properties----------------
Double_t ntracks=constituents.size();
for(Int_t it = 0; it < ntracks; it++)
{
	Double_t tpT=constituents[it].perp();
	htptfrac_jpt->Fill(tpT/jpT,jpT);
   htptfrac_ntr->Fill(tpT/jpT,ntracks);
   hjetpt_ntr->Fill(jpT,ntracks);
}


//----------------------------
	      
	  goodjet++;
	}
      
      foutput->cd();
      ftreejets->Fill();
      readerdata.PrintStatus(60); //every 1min 

      evt++;
    }//event loop
if(evt>0)
{
MeanpT=MeanpT/evt;
Ntracks=Ntracks/evt;
}
cout<<"N tracks:"<<Ntracks<<" mean pT: "<<MeanpT<<endl;

  foutput->cd();
  ftreejets->Write();
htptfrac_jpt->Write();
htptfrac_ntr->Write();
   hjetpt_ntr->Write();
refmult_day->Write();
htrackpT->Write();
hevents->Write();


  foutput->Close();

  delete refmultCorrUtil;
  readerdata.PrintStatus();
}
