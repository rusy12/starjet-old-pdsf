#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetVector.h>
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "StRefMultCorr.h"
#include <TStarJetPicoPrimaryTrack.h>
#include "StJetTrackEvent.h"
#include "TChain.h"

#include <fstream>
#include <iostream>
using namespace std;

static char* ALEX_EVENT_TREE   = "JetTrackEvent";
static char* ALEX_EVENT_BRANCH = "Events";

void qa_alex(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  Float_t max_rap = atof(gSystem->Getenv("MAXRAP"));
  Float_t DCACut = atof(gSystem->Getenv("DCA"));
  Float_t  zVertexCut= atof(gSystem->Getenv("ZVERTEX"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcutMin = atoi(gSystem->Getenv("REFMULTMIN"));
  Int_t refmultcutMax = atoi(gSystem->Getenv("REFMULTMAX"));

  TString pinputdir="root://pstarxrdr1.nersc.gov//star/picodsts/aschmah/Jet/AuAu200_run11//histo_out_V2/mode_1/";

  Int_t start_event_use=0;
  Int_t stop_event_use=nev+start_event_use;
	if(nev<1)stop_event_use=1E8;

  //Int_t zetcut = atoi(gSystem->Getenv("ZVERTEX"));
  //Int_t doEffiCorr = atoi(gSystem->Getenv("DOEFFICORR"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] Trigger=%s", strigger.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
  
//CUTS
  //Int_t zVertexCut=30; //max z-vertex position in cm
  Float_t nSigmaCut=3.0;
  //Float_t DCACut=1.0;
  Int_t nFitPointsCut=15; //minimal number of fitted points
  //Float_t nFitOverMaxPointsCut=0.55;
  Float_t pTmin=0.2; //TPC track min pT 
  Float_t pTmax=30.0; //TPC track max pT

//VARIABLES
  TString str = "null";
  str = Form("%s/qa_alex.root", outDir.Data());

  int runid = 0;
  int refmult = 0;
  int refmultCor = 0;
  int zdcrate = 0;
  float  weight = 1.0; //MB trigger bias weighting factor
  Double_t zvertex = 0;   
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  //fourvector *trig4vec = new fourvector();
  TClonesArray *inclusivearr = new TClonesArray("jet", 10);

  TFile *foutput = new TFile(str.Data(), "RECREATE");

	TH2D *refmult_day=new TH2D("refmult_day","refMultCorr_vs_day",500,0.5,500.5,200,0.5,200.5);
  	TH1D* hzvtx=new TH1D("hzvtx","zvertex",80,-40,40);
  	TH1I* hgoodf=new TH1I("hgoodf","n good files",2,-0.5,1.5);

  TChain *ch;
  StJetTrackEvent     *JetTrackEvent;
  //StJetTrackParticle  *JetTrackParticle;

//-----------------------------------------------------
//Load Input Files
//-----------------------------------------------------
Long64_t entries_save = 0;
Int_t iFile=0;
Int_t nGoodFiles=0;
if (!sInFileList.IsNull())   // if input file is ok
{
	cout << "Open file list " << sInFileList << endl;
   ifstream in(sInFileList);  // input stream
   if(in)
   {
   	cout << "file list is ok" << endl;
  		ch = new TChain( ALEX_EVENT_TREE, ALEX_EVENT_TREE );
		char strg[255];       // char array for each file name
		while(in)
		{
				  in.getline(strg,255);  // take the lines of the file list
				  if(strg[0] != 0)
				  {
							 iFile++;
							 if(iFile<=nSkipFiles)continue;
							 if(iFile>nSkipFiles+nFiles)continue;
							 TString addfile;
							 addfile += strg;
							 addfile = pinputdir+addfile;
							 Long64_t file_entries;
							 ch->AddFile(addfile.Data(),-1, ALEX_EVENT_TREE );
							 file_entries = ch->GetEntries();
							 cout << "File added to data chain: " << addfile.Data() << " with " << (file_entries-entries_save) << " entries" << endl;
							 entries_save = file_entries;
							nGoodFiles++;
							hgoodf->Fill(1);
				  }
		}
	}
}
else
{
		  cout << "WARNING: file input is problemtic" << endl;
}

cout<<"Good files:"<<nGoodFiles<<"/"<<nFiles<<endl;
JetTrackEvent = new StJetTrackEvent();
ch->SetBranchAddress( ALEX_EVENT_BRANCH, &JetTrackEvent );

StRefMultCorr* refmultCorrUtil  = new StRefMultCorr("refmult");

//---------------------------
//EVENT LOOP
//---------------------------
int Ntracks=0;
float MeanpT=0;
int evt=0;
Long64_t stop_event_use_loop = stop_event_use;
if(stop_event_use_loop > entries_save) stop_event_use_loop = entries_save;
for(Long64_t counter = start_event_use; counter < stop_event_use_loop; counter++)
{
		  if (counter != 0  &&  counter % 100 == 0)
					 cout << "." << flush;
		  if (counter != 0  &&  counter % 1000 == 0)
		  {
					 if((stop_event_use_loop-start_event_use) > 0)
					 {
                    Double_t event_percent = 100.0*((Double_t)(counter-start_event_use))/((Double_t)(stop_event_use_loop-start_event_use));
                    cout << " " << counter << " (" << event_percent << "%) " << "\n" << "==> Processing data " << flush;
                }
		  }

		  if (!ch->GetEntry( counter )) // take the event -> information is stored in event
					 break; 


		  Float_t  prim_vertex_x   = JetTrackEvent->getx();
		  Float_t  prim_vertex_y   = JetTrackEvent->gety();
		  			  zvertex   = JetTrackEvent->getz();
		  			  runid           = JetTrackEvent->getid();
		  Float_t  refMult         = JetTrackEvent->getmult();
		  Float_t  n_prim          = JetTrackEvent->getn_prim();
		  Float_t  ZDCx            = JetTrackEvent->getZDCx();
		  Float_t  BBCx            = JetTrackEvent->getBBCx();
		  Float_t  vzVPD           = JetTrackEvent->getvzVpd();
		  Int_t    N_Particles     = JetTrackEvent->getNumParticle();

		  refmultCorrUtil->init(runid);
//cout<<"z:  "<<zvertex<<endl;
		  if(TMath::Abs(zvertex)>zVertexCut)continue;
//cout<<"runid:"<<runid<<endl;
   	  if(refmultCorrUtil->isBadRun(runid)) continue;
		  refmultCorrUtil->initEvent(refMult, zvertex, ZDCx);
		  weight = refmultCorrUtil->getWeight();
		  refmultCor = refmultCorrUtil->getRefMultCorr() ;
//cout<<"refmult:"<<refmultCor<<endl;
		  if(refmultCor<=refmultcutMin || refmultCor>refmultcutMax)continue;
			
		//cout<<"weight: "<<weight<<" refmult: "<<refMult<<" z-vertex: "<<zvertex<<endl;

		  Int_t day = (Int_t)(runid/1000)-12000;
		  refmult_day->Fill(refmultCor,day);
			hzvtx->Fill(zvertex);

/*
		  std::vector<fastjet::PseudoJet> input_vector;
		  int ntracks=0;
		  float meanpT=0;
		  for(Int_t i_Particle = 0; i_Particle < N_Particles; i_Particle++)
		  {
					 // Particle information
					 JetTrackParticle            = JetTrackEvent->getParticle(i_Particle);
					 Float_t dca                 = JetTrackParticle->get_dca_to_prim();
					 Float_t m2                  = JetTrackParticle->get_Particle_m2 ();
					 Float_t nSPi                = TMath::Abs(JetTrackParticle->get_Particle_nSigmaPi());
					 Float_t nSK                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaK());
					 Float_t nSP                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaP());
					 Float_t qp                  = JetTrackParticle->get_Particle_qp();
					 Float_t nhitsfit            = JetTrackParticle->get_Particle_hits_fit();
					 TLorentzVector TLV_Particle_prim = JetTrackParticle->get_TLV_Particle_prim();
					 TLorentzVector TLV_Particle_glob = JetTrackParticle->get_TLV_Particle_glob();

					 TLorentzVector TLV_Particle_use = TLV_Particle_prim;
					 //if(eflab_prim_glob == 1) TLV_Particle_use = TLV_Particle_glob;

					 Double_t track_phi    = TLV_Particle_use.Phi();
					 Double_t track_pT     = TLV_Particle_use.Pt();

					 //apply cuts
					 if(track_pT != track_pT) continue; // that is a NaN test. It always fails if track_pT = nan.
					 Double_t track_eta    = TLV_Particle_use.PseudoRapidity();
				    if(nhitsfit<nFitPointsCut)continue;
					 if(track_pT<pTmin || track_pT>pTmax) continue;
					 if(TMath::Abs(dca)>DCACut)continue;
					 if(nSPi>nSigmaCut && nSK>nSigmaCut && nSP>nSigmaCut)continue; //is it a charged hadron?

					 ntracks++;
					 meanpT+=track_pT;

					 fastjet::PseudoJet pseudoJet(TLV_Particle_use.Px(),TLV_Particle_use.Py(),TLV_Particle_use.Pz(),TLV_Particle_use.E());
					 pseudoJet.set_user_index(i_Particle); // keep track of particles
					 input_vector.push_back(pseudoJet);
		  }
		  if(ntracks>0)
					 meanpT=meanpT/ntracks;
		  MeanpT+=meanpT;
		  Ntracks+=ntracks;

		  //cout<<"refmult: "<<refmultCor<<" n tracks: "<<ntracks<<endl;

		  if (input_vector.size() <= 0) continue;
*/

}//event loop
if(evt>0)
{
//MeanpT=MeanpT/evt;
//Ntracks=Ntracks/evt;
}
//cout<<"N tracks:"<<Ntracks<<" mean pT: "<<MeanpT<<endl;

delete refmultCorrUtil;

foutput->cd();
hzvtx->Write();
hgoodf->Write();
refmult_day->Write();

foutput->Close();

}
