#include "embedding.h"

#include "fjwrapper.h"
#include <fastjet/PseudoJet.hh>


ClassImp(embedding)

//____________________________________________________________________________
embedding::embedding()
{
  fPtEmb = 0;
  fetaEmb = 0;
  fphiEmb = 0;
  fetaEmbReco = 0;
  fphiEmbReco = 0;
  fPtEmbReco = 0;
  fPtEmbPart = 0;
  fAreaEmb = 0;
  fPtEmbCorr = 0;
  fdeltapT = 0;
  ffoundJet = 0;
  fdR=0;
  fPtLeading = 0;
  fRho=0;
}

//____________________________________________________________________________
embedding::embedding(Int_t foundJet, Double_t pT, Double_t eta, Double_t phi, Double_t Area, Double_t pTreco, Double_t pTcorr,
 Double_t etareco, Double_t phireco, Double_t dR, Double_t pTleading, Double_t pTpart, Double_t deltaPt, Float_t rho)
{
  ffoundJet = foundJet;
  fPtEmb = pT;
  fetaEmb = eta ;
  fphiEmb = phi;
  fetaEmbReco = etareco ;
  fphiEmbReco = phireco;
  fPtEmbPart = pTpart;
  fAreaEmb = Area;
  fdeltapT = deltaPt;
  fPtEmbReco = pTreco;
  fPtEmbCorr = pTcorr;
  fdR = dR;
  fPtLeading = pTleading;
  fRho=rho;
}

//____________________________________________________________________________
embedding::~embedding()
{
}

