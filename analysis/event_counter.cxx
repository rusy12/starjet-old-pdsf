#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoPrimaryTrack.h>
#include <TStarJetPicoUtils.h>



/*
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
*/
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void event_counter(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
//  double r = atof(gSystem->Getenv("RPARAM"));
//  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
//  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
//  Int_t refmultcut = atoi(gSystem->Getenv("REFMULT"));
  //Int_t zetcut = atoi(gSystem->Getenv("ZVERTEX"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
  

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  /*TString str = "null";
  str = Form("%s/nevents_%s.root", outDir.Data(),strigger.Data());
  TFile *foutput = new TFile(str.Data(), "RECREATE");
*/
//-------Histograms--------------
//TH1I*    hNumEvents   = new TH1I("hNumEvents" ,"number of events produced",1,0.5,1.5);
//--------------------------------
  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nev);
    
  Int_t evt = 0;

  while (readerdata.NextEvent() == kTRUE)
    {
		//hNumEvents->Fill(1);

  }
  readerdata.PrintStatus();
 // foutput->Write();
 // foutput->Close();
}
