#ifndef __fourvector__hh
#define __fourvector__hh

#include "Rtypes.h"
#include "TLorentzVector.h"

class fourvector : public TObject
{
 public:
  fourvector();
  fourvector(Double_t pT, Double_t eta, Double_t phi, Double_t M);
  ~fourvector();

  void SetPtEtaPhiM(Double_t pT, Double_t eta, Double_t phi, Double_t M);
  TLorentzVector GetTLorentzVector();

  inline double GetEta () { return feta;}
  inline double GetPhi () { return fphi;}
  inline double GetPt () { return fpT;}
  
 protected:
  Double32_t fM;   //[0, 0, 16]
  Double32_t fpT;  //[0, 0, 16]
  Double32_t feta; //[0, 0,  8]
  Double32_t fphi; //[0, 0,  8]

  ClassDef(fourvector, 1);
};

#endif
