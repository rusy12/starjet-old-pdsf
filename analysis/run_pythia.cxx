#include "TF1.h"
#include "TFile.h"
#include "TRandom.h"
#include "TPythia6.h"
#include "TParticle.h"
#include "TParticlePDG.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "Riostream.h"
#include "fjwrapper.h"
#include <fastjet/PseudoJet.hh>
#include "utils.h"
#include "fourvector.h"
#include "run_pythia.h"

double MakeCone(TPythia6 *pythia, TClonesArray *pararr,Double_t pT, Double_t jetEta, Double_t jetPhi, Bool_t charged, TString frag)
{

  Bool_t found = kFALSE;
  Double_t jetpTpart = 0;

  TClonesArray *simarr = new TClonesArray("TParticle", 1000);

  //Double_t radius = 0.4;
  //TF1 *fJetProf = new TF1("fJetProf", "1 - 1/(1 + TMath::Exp(-(x-[0])/[1]))", 0, 1.0);
  //fJetProf->SetParameters(0.5, 0.05);
  
  while(!found)
    {
      simarr->Delete();

      //Double_t phi_parton = gRandom->Uniform()*2.0*TMath::Pi();
      Double_t phi_parton = jetPhi;
      //Double_t eta_parton = gRandom->Uniform(-1., +1.);
      Double_t eta_parton = jetEta;
      Double_t theta = 2.0*TMath::ATan(TMath::Exp(-1*eta_parton));
      Double_t E = pT/TMath::Sin(theta);
      
		//cout<<"E: "<<E<<" jet eta: "<<eta_parton<<" jet phi: "<<phi_parton<<endl;
		if(frag=="u")
   	   pythia->Py1ent(1, 2, E, theta, phi_parton); //u->jet
		else if (frag=="g")
	      pythia->Py1ent(1, 21, E, theta, phi_parton); //g->jet

      pythia->Pyexec();

      TLorentzVector partonlv(0, 0, 0, 0);
      partonlv.SetPtEtaPhiE(pT, eta_parton, phi_parton, E);

      Int_t final = pythia->ImportParticles(simarr, "Final");
      Int_t nparticles = simarr->GetEntries();
     //cout<<"npart: "<<nparticles<<endl; 
      TLorentzVector conelvPart; //particle level jet vector
  		TLorentzVector partlvPart; //particle level jet constituent vector

      Int_t goodparpart = 0;
	
      for(Int_t ipart = 0; ipart < nparticles; ipart++)
	{
	  TParticle *particle = (TParticle*)simarr->At(ipart);

	  
	  particle->Momentum(partlvPart);
	  partlvPart.SetPtEtaPhiM(partlvPart.Pt(), partlvPart.Eta(), partlvPart.Phi(), 0); //set M=0, probably not necessary

	  Double_t pTpart = partlvPart.Pt();
    
 	  if(pTpart < 0.2) continue;
	 
	 if(charged)
         {
          Double_t charge = particle->GetPDG()->Charge();
          if(!charge) continue;
         }

	  Double_t eta = partlvPart.Eta();
	  Double_t phi = partlvPart.Phi();
	  Double_t M = 0;

	  new ((*pararr)[goodparpart]) fourvector(pTpart, eta, phi, M);
	  goodparpart++;
	  conelvPart += partlvPart;

	}//particle loop [in particle level jet]

     jetpTpart = conelvPart.Pt();
   if(!goodparpart) continue;
   found = kTRUE;
	}//particle level jet exist 	

  delete simarr;
  return jetpTpart;
}


