#ifndef __embedding_hh
#define __embedding_hh

#include "TObject.h"
#include "Rtypes.h"

class embedding : public TObject
{
 public:
  embedding();
  embedding(Int_t foundJet, Double_t pT, Double_t eta, Double_t phi, Double_t Area, Double_t pTreco, Double_t pTcorr,
 Double_t etareco, Double_t phireco, Double_t dR, Double_t pTleading, Double_t pTpart, Double_t deltaPt, Float_t rho);
  ~embedding();

  // EMBEDDING
  Int_t ffoundJet; 

  Double32_t fPtEmb;     //[0, 0, 16]
  Double32_t fetaEmb;    //[0, 0,  8]
  Double32_t fphiEmb;    //[0, 0,  8]
  Double32_t fetaEmbReco;    //[0, 0,  8]
  Double32_t fphiEmbReco;    //[0, 0,  8]
  Double32_t fdR;    //[0, 0,  8]
  Double32_t fAreaEmb;   //[0, 0,  8]
  Double32_t fRho;   //[0, 0,  8]
  Double32_t fPtEmbCorr;  //[0, 0, 16]
  Double32_t fPtEmbPart;  //[0, 0, 16]
  Double32_t fdeltapT;   //[0, 0, 16]
  Double32_t fPtEmbReco; //[0, 0, 16]
  Double32_t fPtLeading; //[0, 0, 16]
 
  ClassDef(embedding, 2);
};

#endif
