#ifndef __checker__hh
#define __checker__hh

#include "TObject.h"
#include "Rtypes.h"

class checker : public TObject
{
 public:
  checker();
  ~checker();
  void addBadSector(Float_t eta1, Float_t eta2, Float_t phi1, Float_t phi2);
  bool isBadSector(Float_t eta, Float_t phi);
  bool isNearBadSector(Float_t eta, Float_t phi, Float_t band); //is it inside or near a bad sector?
  void addBadDay(Int_t day);
  bool isBadDay(Int_t day);
  bool isBadRun(Int_t);

protected:
static const Int_t fnSectorsMax=5; //for array range allocation
static const Int_t fnBadDaysMax=50; //for array range allocation
static const Int_t fnBadRuns=179; 
Float_t fphiMinCut[fnSectorsMax];
Float_t fphiMaxCut[fnSectorsMax];
Float_t fetaMinCut[fnSectorsMax];
Float_t fetaMaxCut[fnSectorsMax];
Int_t fnSectors;
Int_t fBadDay[fnBadDaysMax];
Int_t fnBadDays;
ClassDef(checker, 1);
};

#endif

