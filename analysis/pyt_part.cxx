//This macro runs over generated pythia events and 
//plots spectra of good tracks

#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "StRefMultCorr.h"
#include "pyt_part.h"
#include "PySimSimulationHeader.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void pyt_part(Long64_t nev)
{
  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  TString dataDir = gSystem->Getenv("INPUTDIR");
  TString eventDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  Int_t charged=1;

  TClonesArray *fpartarr;

//INPUT
TString str = Form("%s/pythia.root", dataDir.Data());
cout<<"[i] Input file: "<<str<<endl;
TFile *finput = new TFile(str.Data(), "OPEN");
TTree *ftreepyt = (TTree*)finput->Get("event");
ftreepyt->SetBranchAddress("particles", &fpartarr);

  
 TH1D* hpartpt=new TH1D("hpartpt","particle pT; p_{T} (GeV/c)",100,0,100);
  TH1I* hevent=new TH1I("hevent","N events",1,0,10);
  TH1D* hpartpt_scl=new TH1D("hpartpt_scl","particle pT; p_{T} (GeV/c);1/N_{event} dN/dp_{T}",100,0,100);
  TH1I *hevts = new TH1I("hevts", "hevts", 2, 0, 2);

//INPUT - events 
  str = Form("%s/pythia.root", eventDir.Data());
  TFile *fevents = new TFile(str.Data(), "OPEN");
  //TClonesArray *aparticles = new TClonesArray("TParticle", 100);
  //TTree * tevents = (TTree*)fevents->Get("event");
  //tevents->SetBranchAddress("particles", &aparticles);

//OUTPUT
  str = Form("%s/histos_pythiajet_part.root", dataDir.Data());
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  PySimSimulationHeader* header = (PySimSimulationHeader*)fevents->Get("PySimSimulationHeader");

//scale factors - for connecting pT hard bins
Double_t xsection = header->GetXsection();
//Int_t nentries = tevents->GetEntries();
Double_t nevents = header->GetNevents();
Double_t ntrials = header->GetNtrials();

//EVENT LOOP 
//Int_t evt = 0;
Int_t nevts = ftreepyt->GetEntries();
if(nevts>nev && nev>0)nevts=nev;
cout<<"Run set for "<<nevts<<" events"<<endl;
for(Int_t ievt = 0; ievt < nevts; ievt++)
{
if(ievt%1000==0)cout<<"filling event "<<ievt<<endl;
ftreepyt->GetEntry(ievt);
std::vector<fastjet::PseudoJet> input_vector;
Int_t nparticles = fpartarr->GetEntries();

hevts->Fill(1);
//PARTICLE LOOP
//Double_t part=0;
TLorentzVector partlv;
for(Int_t ipart = 0; ipart < nparticles; ipart++)
{
TParticle *particle = (TParticle*)fpartarr->At(ipart);
Int_t pdg = TMath::Abs(particle->GetPdgCode());

if(pdg == 12 || pdg == 14 || pdg == 16 || pdg == 18 ||
pdg == 2112 || pdg == 130 ) continue; //undetectable particles

if(charged){
 Double_t chrg = particle->GetPDG()->Charge();
              if(!chrg) continue;
			//if(pdg==22) continue; //neutral particles
		}
      particle->Momentum(partlv);

      if(partlv.Pt() < 0.2 || partlv.Pt()>20) continue;

      if(TMath::Abs(partlv.Eta()) > max_rap) continue;
hpartpt->Fill(partlv.Pt());
delete particle;
    }
	//end of particel loop

  hevent->Fill(1);
    }
//end of event loop
  foutput->cd();
	hpartpt->Clone("hpartpt_scl");
Double_t jetscale = (nevents/ntrials * xsection);
  hpartpt_scl->Scale(jetscale);
  hpartpt->Write();
  hevent->Write();
  hpartpt_scl->Write();
  foutput->Close();

}
