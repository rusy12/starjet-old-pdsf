#ifndef __jet__hh
#define __jet__hh

#include "TClonesArray.h"
#include "fourvector.h"

#include "Rtypes.h"

class FourVector;

class jet : public TObject
{
 public:
  jet();
  ~jet();

  void AddConst(Int_t idx, TLorentzVector lv);
  
  Int_t embeddedJet_idx;
  
  Int_t Nconst;
  Double32_t area;      //[0, 0, 16]
  Double32_t pTleading; //[0, 0, 16]
  Double32_t pTDete; //[0, 0, 16]
  Double32_t pTleadingDete; //[0, 0, 16]
  Double32_t etaDete; //[0, 0, 16]
  Double32_t phiDete; //[0, 0, 16]

  fourvector jet_fv;

  TClonesArray *fconst;

  ClassDef(jet, 1);
};

#endif
