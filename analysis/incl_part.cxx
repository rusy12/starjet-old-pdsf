#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "StRefMultCorr.h"
#include "incl_part.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void incl_part(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcut = atoi(gSystem->Getenv("REFMULT"));
  //Int_t zetcut = atoi(gSystem->Getenv("ZVERTEX"));
  //Int_t doEffiCorr = atoi(gSystem->Getenv("DOEFFICORR"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
  

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  evCuts->SetVertexZCut(30);//[cm] 
  //evCuts->SetRefMultCut(refmultcut);

  readerdata.GetTrackCuts()->SetDCACut(1.);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(20);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy

  TString str = "null";
  str = Form("%s/particle_spectrum.root", outDir.Data());

  int runid = 0;
  int refmult = 0;
  int refmultCor = 0;
  int zdcrate = 0;
  Double_t zvertex = 0;   
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  //fourvector *trig4vec = new fourvector();
  TClonesArray *inclusivearr = new TClonesArray("jet", 10);

  TFile *foutput = new TFile(str.Data(), "RECREATE");

  TH1D* hpartpt=new TH1D("hpartpt","particle pT; p_{T} (GeV/c)",100,0,100);
  TH1I* hevent=new TH1I("hevent","N events",1,0,10);
  TH1D* hpartpt_scl=new TH1D("hpartpt_scl","particle pT; p_{T} (GeV/c);1/N_{event} dN/dp_{T}",100,0,100);

  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nev);

	StRefMultCorr* refmultCorrUtil  = new StRefMultCorr("refmult");
    
  Int_t evt = 0;

  std::vector<fastjet::PseudoJet> input_vector;

  while (readerdata.NextEvent() == kTRUE)
    {
      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      runid = header->GetRunId();
      refmult = header->GetReferenceMultiplicity();
		zvertex = header->GetPrimaryVertexZ();
		zdcrate = header->GetZdcCoincidenceRate();

		refmultCorrUtil->init(runid);
   	if(refmultCorrUtil->isBadRun(runid)) continue;
      refmultCorrUtil->initEvent(refmult, zvertex, zdcrate) ;
		refmultCor = refmultCorrUtil->getRefMultCorr() ;
		if(refmultCor<=refmultcut)continue;


      TStarJetVectorContainer<TStarJetVector>* container = readerdata.GetOutputContainer();
      
		TStarJetVector *part = 0;

	   for (Int_t ip = 0; ip < container->GetEntries(); ip++)
    	{
      	part = container->Get(ip);
	      if(!part->IsChargedHadron())continue;
			if(TMath::Abs(part->Eta())>max_rap)continue;
   		if (part->Pt() > 0.2 && part->Pt() < 20)
     {
			hpartpt->Fill(part->Pt());
     }
    }
    delete part;
    delete container;
      
      readerdata.PrintStatus(60); //every 1min 

hevent->Fill(1);
      evt++;
    }
  foutput->cd();
	hpartpt->Clone("hpartpt_scl");
  hpartpt_scl->Scale(1.0/evt, "width");
  hpartpt->Write();
  hevent->Write();
  hpartpt_scl->Write();
  foutput->Close();

  delete refmultCorrUtil;
  readerdata.PrintStatus();
}
