#ifndef __UTILS_HH
#define __UTILS_HH

#include "TStarJetVector.h"
#include "TStarJetVectorContainer.h"

#include <fastjet/PseudoJet.hh>

std::vector<fastjet::PseudoJet> 
make_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
		  double minpt = 0.2, double maxpt = 1E+10, Int_t indexOffset = 0);

std::vector<fastjet::PseudoJet> 
make_charged_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
			  double minpt = 0.2, double maxpt = 1E+10, Int_t indexOffset = 0);

std::vector<fastjet::PseudoJet> 
make_HT_trigger_vector(TStarJetVectorContainer<TStarJetVector> *container,
		       double minpt = 0.2, double maxpt = 1E+10, Int_t indexOffset = 0);

/*std::vector<fastjet::PseudoJet> 
make_efficorrected_charged_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
			  double minpt = 0.2, double maxpt = 1E+10, Int_t indexOffset = 0);*/

Double_t Eff_track_rec_function(Double_t* x,Double_t* par);

Double_t efficiency08(Double_t eta, Double_t pT);

Double_t efficiency11(Double_t pt, TF1* effLow, TF1* effHigh);

Double_t efficiencyAlex(Double_t pt,Double_t increment=0,bool kcentral=true);
#endif
