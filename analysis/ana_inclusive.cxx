#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TStopwatch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "checker.h"
#include "StRefMultCorr.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void ana_inclusive(Long64_t nev)
{
  TStopwatch timer;
  timer.Start();
  
  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  TString dataDir = gSystem->Getenv("INPUTDIR");

  double r = atof(gSystem->Getenv("RPARAM"));
  double acut = atof(gSystem->Getenv("ACUT"));
  //double zetcut = atof(gSystem->Getenv("ZVERTEX"));
  //double zetcut = 30.0;
  int year=11;  

  cout << Form("[i] WRKDIR=%s", dataDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Acut=%.2lf", r, acut) << endl;

  int runid = 0;
  int refmult = 0;
  Double_t zvertex = 0;
  float weight = 1.0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double_t rpangle = 0;   
  TClonesArray *inclusivearr = new TClonesArray("jet", 10);

  TString str = "null";
  str = Form("%s/inclusivejet_R%.1lf.root", dataDir.Data(), r);

  TFile *finput = new TFile(str.Data(), "OPEN");
  TTree* ftreejets = (TTree*)finput->Get("InclusiveJets");
  ftreejets->SetBranchAddress("runid", &runid);
  ftreejets->SetBranchAddress("refmult", &refmult);
  ftreejets->SetBranchAddress("zvertex", &zvertex);
  ftreejets->SetBranchAddress("weight", &weight);
  ftreejets->SetBranchAddress("rho", &rho);
  ftreejets->SetBranchAddress("sigma", &sigma);
  ftreejets->SetBranchAddress("rpangle", &rpangle);
  ftreejets->SetBranchAddress("akt_inclusive", &inclusivearr);
  
  //str = Form("%s/histos_inclusivejet_R%.1lf_refm%i.root", dataDir.Data(), r, refcut);
  str = Form("%s/histos_inclusivejet_R%.1lf.root", dataDir.Data(), r);
  
  TFile *foutput = new TFile(str.Data(), "RECREATE");

//histogram definitions
  Int_t nptbins=800;
  Float_t ptminbin=-100;
  Float_t ptmaxbin=100;
  Int_t netabins=100*2;
  Float_t etaminbin=-1;
  Float_t etamaxbin=1;
  Int_t nphibins=120;
  Float_t phiminbin=-TMath::Pi();
  Float_t phimaxbin=TMath::Pi();

  TH1F *hevts = new TH1F("hevts", "hevts", 2, 0, 2);
  TH1F *hzvertex=new TH1F("hzvertex", "hzvertex", 100, -50, 50);

  TH1D *hpT_pTl0 = new TH1D("hpT_pTl0", "jet pT for pTlead>0 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	
  TH1D *hpT_pTl3 = new TH1D("hpT_pTl3", "jet pT for pTlead>3 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	
  TH1D *hpT_pTl4 = new TH1D("hpT_pTl4", "jet pT for pTlead>4 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	
  TH1D *hpT_pTl5 = new TH1D("hpT_pTl5", "jet pT for pTlead>5 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	
  TH1D *hpT_pTl6 = new TH1D("hpT_pTl6", "jet pT for pTlead>6 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	
  TH1D *hpT_pTl7 = new TH1D("hpT_pTl7", "jet pT for pTlead>7 ; p_{T} [GeV/c]", nptbins, ptminbin, ptmaxbin); 	


  TH2D *hpT_pTlead = new TH2D("hpT_pTlead", "jet pTcorr vs pTleading; p_{T} [GeV/c]; p_{T}^{lead} [GeV/c]", nptbins, ptminbin, ptmaxbin, 25, 0, 25);
  TH2D *hpT_pTlead_nobadsec = new TH2D("hpT_pTlead_nobadsec", "jet pTcorr vs pTleading without bad sector; p_{T} [GeV/c]; p_{T}^{lead} [GeV/c]", nptbins, ptminbin, ptmaxbin, 25, 0, 25);
  TH2D *hpT_pTlead_nobadsecedge1R = new TH2D("hpT_pTlead_nobadsecedge1R", "jet pTcorr vs pTleading without bad sector+edge(1R); p_{T} [GeV/c]; p_{T}^{lead} [GeV/c]", nptbins, ptminbin, ptmaxbin, 25, 0, 25);

  TH2D *heta_phi = new TH2D("heta_phi", "jet eta vs phi;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  TH2D *heta_phi_nobadsec = new TH2D("heta_phi_nobadsec", "jet eta vs phi without bad sector; #eta; #phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  TH2D *heta_phi_nobadsecedge1R = new TH2D("heta_phi_nobadsecedge1R", "jet eta vs phi without bad sector+edge(0.5R); #eta; #phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  TH2D *heta_phi_pTl5 = new TH2D("heta_phi_pTl5", "jet eta vs phi, pTlead>5;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  TH2D *heta_phi_nobadsec_pTl5 = new TH2D("heta_phi_nobadsec_pTl5", "jet eta vs phi without bad sector, pTlead>5GeV; #eta; #phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  TH2D *heta_phi_nobadsecedge1R_pTl5 = new TH2D("heta_phi_nobadsecedge1R_pTl5", "jet eta vs phi w/o bad sector+edge(1R), pTlead>5GeV; #eta; #phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);

	TH2D *hjetconst=new TH2D("hjetconst","hjetconst",100,0.5,100.5,nptbins, ptminbin, ptmaxbin);
	TH2D *hjetfrac=new TH2D("hjetfrac","hjetfrac",20,0,1,nptbins, ptminbin, ptmaxbin);
	TH1D *hjetarea = new TH1D("hjetarea","jet area",100,0,1);
	TH2D *hjetpTarea = new TH2D("hjetpTarea","jet pTmeasured vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
	TH2D *hjetpTcorrArea = new TH2D("hjetpTcorrArea","jet pTreco vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
	TH1D *hrho = new TH1D("hrho","rho",50,0,50);

//----------------------------

//define bad TPC sectors, bad days,...
   checker * eventCheck = new checker();
   //eventCheck->addBadSector(-1,1,-0.78,-0.26); //the bad sector is however not so bad and can be used
//----------------------------

  for(Int_t ievt = 0; ievt < ftreejets->GetEntries(); ievt++)
   {
      ftreejets->GetEntry(ievt);
      
      //if(TMath::Abs(zvertex) > zetcut) continue;

		//check bad runs
		if(eventCheck->isBadRun(runid))continue;
      
	/*	Int_t day=(runid/1000)-((year+1)*1000);
		bool badDay=eventCheck->isBadDay(day);
      if(badDay) continue;
		*/
      hevts->Fill(1,weight);
		hzvertex->Fill(zvertex,weight);
		hrho->Fill(rho,weight);

      for(Int_t ijet = 0; ijet < inclusivearr->GetEntries(); ijet++)
	   {
 	   	jet *inclusivejet = (jet*)inclusivearr->At(ijet);
	   	TLorentzVector jetlv = (inclusivejet->jet_fv).GetTLorentzVector();
	   	jetlv.SetPhi(TVector2::Phi_mpi_pi(jetlv.Phi()));

	   	Double_t area=inclusivejet->area;
		   Double_t pTcorr = jetlv.Pt() - area * rho;
		   Double_t pTuncorr = jetlv.Pt();
		   Double_t phi = jetlv.Phi();
		   Double_t eta = jetlv.Eta();
		   Double_t pTlead = inclusivejet->pTleading;
	   	Int_t nconst = inclusivejet->Nconst;

			//set acceptance
		   Float_t etaMinCut=-(1-r);
			Float_t etaMaxCut=(1-r);

			if(eta>etaMaxCut || eta<etaMinCut) continue;


			hjetarea->Fill(area,weight);
			hjetpTarea->Fill(jetlv.Pt(),area,weight);
			hjetpTcorrArea->Fill(pTcorr,area,weight);

	   	if(area < acut) continue;
			
			Double_t rpanglerad=(Double_t) (rpangle*TMath::Pi())/180.0;

			//---filling histograms--------------

			hjetconst->Fill(nconst,pTlead,weight);
			hjetfrac->Fill(pTlead/pTcorr,pTlead,weight);

			hpT_pTl0->Fill(pTcorr,weight);
			if(pTlead>3)
			hpT_pTl3->Fill(pTcorr,weight);
			if(pTlead>4)
			hpT_pTl4->Fill(pTcorr,weight);
			if(pTlead>5)
			hpT_pTl5->Fill(pTcorr,weight);
			if(pTlead>6)
			hpT_pTl6->Fill(pTcorr,weight);
			if(pTlead>7)
			hpT_pTl7->Fill(pTcorr,weight);

			hpT_pTlead->Fill(pTcorr,pTlead,weight); 
			heta_phi->Fill(eta,phi,weight);
			if(pTlead>5)heta_phi_pTl5->Fill(eta,phi,weight);

/*
			bool badSector=eventCheck->isBadSector(eta,phi); //remove jets inside the bad sector
	      if(badSector) continue;

			heta_phi_nobadsec->Fill(eta,phi);
			if(pTlead>5) heta_phi_nobadsec_pTl5->Fill(eta,phi);
			hpT_pTlead_nobadsec->Fill(pTcorr,pTlead); 


			badSector=eventCheck->isNearBadSector(eta,phi, r); //remove jets bordering with the bad sector
	      if(badSector) continue;

			heta_phi_nobadsecedge1R->Fill(eta,phi);
			if(pTlead>5)heta_phi_nobadsecedge1R_pTl5->Fill(eta,phi); 
			hpT_pTlead_nobadsecedge1R->Fill(pTcorr,pTlead); 
*/
/*
			//for event-plane calculation
			Double_t angle=TMath::Abs(phi-rpanglerad);
			if(angle>TMath::Pi())angle=2.*TMath::Pi()-angle;
         hjet_ep->Fill(pTcorr,angle,rpanglerad);

			if(pTlead>5) 
			{
	         hjet_ep5->Fill(pTcorr,angle,rpanglerad);

				//cout<<"5 phi: "<<phi<<" angle: "<<angle<<" rpangle: "<<rpanglerad<<endl;
			}

			if(pTlead>7)
				{
	         hjet_ep7->Fill(pTcorr,angle,rpanglerad);
				}
	  */ 
	   }
   }

  foutput->cd();
  foutput->Write();
  foutput->Close();
  delete foutput;

  finput->Close();
  delete finput;

  timer.Stop();
  timer.Print();
}
