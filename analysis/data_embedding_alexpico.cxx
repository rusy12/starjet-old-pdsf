#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TRandom1.h>
#include <TDatime.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TPythia6.h>
#include <TParticle.h>

#include <TStarJetVector.h>
#include <TStarJetPicoPrimaryTrack.h>

#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "StRefMultCorr.h"
#include "StJetTrackEvent.h"
#include "TChain.h"

#include "StMemStat.h"
#include "embedding.h"
#include "find_embedded_jet.h"
#include "fjwrapper.h"
#include "fourvector.h"
#include "utils.h"
#include "run_pythia.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <fstream> 
#include <iostream>
using namespace std;

static char* ALEX_EVENT_TREE   = "JetTrackEvent";
static char* ALEX_EVENT_BRANCH = "Events";


void data_embedding_alexpico(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  double r = atof(gSystem->Getenv("RPARAM"));
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
  Float_t max_rap = atof(gSystem->Getenv("MAXRAP"));
  Float_t DCACut = atof(gSystem->Getenv("DCA"));
  Float_t zVertexCut = atof(gSystem->Getenv("ZVERTEX"));
  Float_t rkt= atof(gSystem->Getenv("RRHO")); //R for rho calculation
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcutMin = atoi(gSystem->Getenv("REFMULTMIN"));
  Int_t refmultcutMax = atoi(gSystem->Getenv("REFMULTMAX"));
  Int_t pTembFixed = atoi(gSystem->Getenv("PTEMBFIXED"));
  Int_t njetsremove=atoi(gSystem->Getenv("NJETSREMOVE"));
  Int_t pythiaJets=atoi(gSystem->Getenv("PYTHIAJETS"));

  TString pinputdir="root://pstarxrdr1.nersc.gov//star/picodsts/aschmah/Jet/AuAu200_run11//histo_out_V2/mode_1/";

  Int_t start_event_use=0;
  Int_t stop_event_use=nev+start_event_use;
	if(nev<1)stop_event_use=1E8;

//-------------------------------------------------

  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Trigger=%s", r, strigger.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
//SETTING RANDOM SEED
TDatime dt;
UInt_t curtime = dt.Get();
UInt_t procid = gSystem->GetPid();
UInt_t seed = curtime - procid;
TRandom1 *rndNum = new TRandom1(seed);
cout<<"setting seed: "<<seed<<endl;

// PYTHIA
TPythia6 * fpythia = new TPythia6();
fpythia->SetMRPY(1, seed);
//cout<<"Pythia initialized"<<endl;


//-------------------------------------------------


	//CUTS
  //const int zVertexCut=30; //max z-vertex position in cm
  const float nSigmaCut=3.0;
  //const float DCACut=1.0;
  const int nFitPointsCut=15; //minimal number of fitted points
  //const float nFitOverMaxPointsCut=0.55;
  const float pTmin=0.2; //TPC track min pT 
  const float pTmax=30.0; //TPC track max pT


  const Int_t nemb =16; 
  Double_t fEmbPt[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 15.0, 20.0, 30.0, 40.0, 50.0, 70.0, 90.0};
  const Double_t pTembMin=3; //maximal pT of embedded jets
  const Double_t pTembMax=50; //maximal pT of embedded jets
  TString str = "null";
  str = Form("%s/embedding_R%.1lf.root", outDir.Data(), r);

  StMemStat memstat;
  memstat.Start();

//declare variables
  int runid = 0;
  int refmult = 0;
  int refmultCor = 0; //corrected refmult
  int zdcrate = 0;
  Double_t weight = 1.0; //MB trigger efficiency weight factor
  Double_t zvertex = 0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]

//setup tree branches
  TClonesArray *embedding_arr = new TClonesArray("embedding", nemb); //single particle jet

  TClonesArray *simarr = new TClonesArray("fourvector", 1000); //array containing particles from generated Pythia jets

  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TTree* ftreejets = new TTree("Embedding","Embedding");
  TBranch *br_runid = ftreejets->Branch("runid", &runid);
  TBranch *br_weight = ftreejets->Branch("weight", &weight);
  TBranch *br_embedding = ftreejets->Branch("embedding", &embedding_arr); 
  TBranch *br_refmult = ftreejets->Branch("refmult", &refmult);
  TBranch *br_rho = ftreejets->Branch("rho", &rho);
  TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
  TBranch *br_rpangle = ftreejets->Branch("rpangle", &rpangle);


  //tchain for input files
  TChain *ch;
  StJetTrackEvent     *JetTrackEvent;
  StJetTrackParticle  *JetTrackParticle;

//-----------------------------------------------------
//Load Input Files
//-----------------------------------------------------
Long64_t entries_save = 0;
Int_t iFile=0;
if (!sInFileList.IsNull())   // if input file is ok
{
	cout << "Open file list " << sInFileList << endl;
   ifstream in(sInFileList);  // input stream
   if(in)
   {
   	cout << "file list is ok" << endl;
  		ch = new TChain( ALEX_EVENT_TREE, ALEX_EVENT_TREE );
		char strg[255];       // char array for each file name
		while(in)
		{
				  in.getline(strg,255);  // take the lines of the file list
				  if(strg[0] != 0)
				  {
							 iFile++;
							 if(iFile<=nSkipFiles)continue;
							 if(iFile>nSkipFiles+nFiles)continue;
							 TString addfile;
							 addfile += strg;
							 addfile = pinputdir+addfile;
							 Long64_t file_entries;
							 ch->AddFile(addfile.Data(),-1, ALEX_EVENT_TREE );
							 file_entries = ch->GetEntries();
							 cout << "File added to data chain: " << addfile.Data() << " with " << (file_entries-entries_save) << " entries" << endl;
							 entries_save = file_entries;
				  }
		}
	}
}
else
{
		  cout << "WARNING: file input is problemtic" << endl;
}
JetTrackEvent = new StJetTrackEvent();
ch->SetBranchAddress( ALEX_EVENT_BRANCH, &JetTrackEvent );

StRefMultCorr* refmultCorrUtil  = new StRefMultCorr("refmult");

//---------------------------
//EVENT LOOP
//---------------------------
int Ntracks=0;
float MeanpT=0;
int evt=0;
Long64_t stop_event_use_loop = stop_event_use;
if(stop_event_use_loop > entries_save) stop_event_use_loop = entries_save;
for(Long64_t counter = start_event_use; counter < stop_event_use_loop; counter++)
{
		  if (counter != 0  &&  counter % 100 == 0)
					 cout << "." << flush;
		  if (counter != 0  &&  counter % 1000 == 0)
		  {
					 if((stop_event_use_loop-start_event_use) > 0)
					 {
                    Double_t event_percent = 100.0*((Double_t)(counter-start_event_use))/((Double_t)(stop_event_use_loop-start_event_use));
                    cout << " " << counter << " (" << event_percent << "%) " << "\n" << "==> Processing data " << flush;
                }
		  }

		  if (!ch->GetEntry( counter )) // take the event -> information is stored in event
					 break; 


		  runid           = JetTrackEvent->getid();
		  refmult         = JetTrackEvent->getmult();
		  Float_t  prim_vertex_x   = JetTrackEvent->getx();
		  Float_t  prim_vertex_y   = JetTrackEvent->gety();
		  Float_t  prim_vertex_z   = JetTrackEvent->getz();
		  Float_t  n_prim          = JetTrackEvent->getn_prim();
		  Float_t  ZDCx            = JetTrackEvent->getZDCx();
		  Float_t  BBCx            = JetTrackEvent->getBBCx();
		  Float_t  vzVPD           = JetTrackEvent->getvzVpd();
		  Int_t    N_Particles     = JetTrackEvent->getNumParticle();

		  		  refmultCorrUtil->init(runid);
   	  if(refmultCorrUtil->isBadRun(runid)) continue;
		  refmultCorrUtil->initEvent(refmult, prim_vertex_z, ZDCx);
		  weight = refmultCorrUtil->getWeight();

		  refmultCor = refmultCorrUtil->getRefMultCorr() ;
		  if(refmultCor<=refmultcutMin || refmultCor>refmultcutMax)continue;
			
		  if(TMath::Abs(prim_vertex_z)>zVertexCut)continue;

			zvertex=prim_vertex_z;

		  Int_t day = (Int_t)(runid/1000)-12000;

			embedding_arr->Delete();  
      	simarr->Delete();

		  std::vector<fastjet::PseudoJet> input_vector;
		  int ntracks=0;
		  float meanpT=0;
		  for(Int_t i_Particle = 0; i_Particle < N_Particles; i_Particle++)
		  {
					 // Particle information
					 JetTrackParticle            = JetTrackEvent->getParticle(i_Particle);
					 Float_t dca                 = JetTrackParticle->get_dca_to_prim();
					 Float_t m2                  = JetTrackParticle->get_Particle_m2 ();
					 Float_t nSPi                = TMath::Abs(JetTrackParticle->get_Particle_nSigmaPi());
					 Float_t nSK                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaK());
					 Float_t nSP                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaP());
					 Float_t qp                  = JetTrackParticle->get_Particle_qp();
					 Float_t nhitsfit            = JetTrackParticle->get_Particle_hits_fit();
					 TLorentzVector TLV_Particle_prim = JetTrackParticle->get_TLV_Particle_prim();
					 TLorentzVector TLV_Particle_glob = JetTrackParticle->get_TLV_Particle_glob();

					 TLorentzVector TLV_Particle_use = TLV_Particle_prim;
					 //if(eflab_prim_glob == 1) TLV_Particle_use = TLV_Particle_glob;

					 Double_t track_phi    = TLV_Particle_use.Phi();
					 Double_t track_pT     = TLV_Particle_use.Pt();

					 //apply cuts
					 if(track_pT != track_pT) continue; // that is a NaN test. It always fails if track_pT = nan.
					 Double_t track_eta    = TLV_Particle_use.PseudoRapidity();
				    if(nhitsfit<nFitPointsCut)continue;
					 if(track_pT<pTmin || track_pT>pTmax) continue;
					 if(TMath::Abs(dca)>DCACut)continue;
					 if(nSPi>nSigmaCut && nSK>nSigmaCut && nSP>nSigmaCut)continue; //is it a charged hadron?

					 ntracks++;
					 meanpT+=track_pT;

					//make input vector from particles
					 fastjet::PseudoJet pseudoJet(TLV_Particle_use.Px(),TLV_Particle_use.Py(),TLV_Particle_use.Pz(),TLV_Particle_use.E());
					 pseudoJet.set_user_index(i_Particle); // keep track of particles
					 input_vector.push_back(pseudoJet);
		  }


//Run jet reconstruction on data (for background calculation)
      std::vector<fastjet::PseudoJet> input_vector_data = input_vector;
      FJWrapper kt_data;
      kt_data.r = rkt;
      kt_data.maxrap = max_rap;
      kt_data.algor = fastjet::kt_algorithm;
		kt_data.ghost_area = 0.01;
      kt_data.input_particles = input_vector_data;
      kt_data.Run();

		std::vector<fastjet::PseudoJet> jets = kt_data.inclusive_jets;
		Int_t njets=jets.size();
	if(njets>njetsremove)
      kt_data.GetMedianAndSigma(rho, sigma,njetsremove);  //remove n most energetic jets from the calculation
 	else
		kt_data.GetMedianAndSigma(rho, sigma,0);
   
   for (Int_t iemb = 0; iemb < nemb; iemb++){

	  //set embedded pT, eta, phi
	  Double_t pT_emb=fEmbPt[iemb];
	  if(!pTembFixed || pythiaJets)
			pT_emb=rndNum->Uniform(pTembMin,pTembMax);
//cout<<"pT gen:"<<pT_emb<<endl;
	  Double_t eta_rnd = rndNum->Uniform(-max_rap + r, max_rap - r);
	  Double_t phi_rnd = rndNum->Uniform(0, 2.*TMath::Pi());
  
	  std::vector<fastjet::PseudoJet> input_vector_emb = input_vector; //single particle jet embedded into event


		float jetPtPyt=0;
	  //SINGLE PARTICLE JETS
		if(pythiaJets==0)
		{
		  TLorentzVector v;
		  v.SetPtEtaPhiM(pT_emb, eta_rnd, phi_rnd, 0);
		  fastjet::PseudoJet embededParticle(v.Px(), v.Py(), v.Pz(), v.E());
		  embededParticle.set_user_index(99999);
		  input_vector_emb.push_back(embededParticle);
		}
		//PYTHIA jets
		else
		{	
     		jetPtPyt = MakeCone(fpythia, simarr, pT_emb, eta_rnd, phi_rnd,charged);
		  	Int_t Nparticles = simarr->GetEntries();
			//cout<<"number of particles in jet: "<<Nparticles;
	  		for(Int_t ipart = 0; ipart < Nparticles; ipart++)
		   {
		  		fourvector *fv = (fourvector*)simarr->At(ipart);
		      TLorentzVector lv = fv->GetTLorentzVector();
				fastjet::PseudoJet embededParticle(lv.Px(), lv.Py(), lv.Pz(), lv.E());
		      embededParticle.set_user_index(99999);
				//cout<<ipart+1<<" particle pT:"<<lv.Pt()<<endl;
   	      input_vector_emb.push_back(embededParticle);
			}
	     simarr->Clear("C");     
		}


//********************
//*JET RECONSTRUCTION*
//********************

	  FJWrapper akt_data_emb;
	  akt_data_emb.r = r;
	  akt_data_emb.maxrap = max_rap;
	  akt_data_emb.algor = fastjet::antikt_algorithm;
	  akt_data_emb.ghost_area = 0.01;
	  akt_data_emb.input_particles = input_vector_emb;
	  akt_data_emb.Run();

	  new ((*embedding_arr)[iemb]) embedding();

		//jet matching
		if(pythiaJets==0)
		  find_jet_sp(&akt_data_emb, (embedding*)embedding_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, rho);
		else
	     find_jet_E(&akt_data_emb, (embedding*)embedding_arr->At(iemb), jetPtPyt, eta_rnd, phi_rnd, rho);
   }//embedding_pT   
      foutput->cd();
      ftreejets->Fill();

      evt++;
    }

  foutput->cd();
  ftreejets->Write();
  foutput->Close();


  memstat.Stop();
}
