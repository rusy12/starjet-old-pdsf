#include "TLorentzVector.h"

#include "jet.h"

ClassImp(jet)

//________________________________________________________
jet::jet():
  jet_fv()
{
  // default constructor
  embeddedJet_idx = -1;
  Nconst = 0.;
  area = 0.0;
  pTleading = 0.0;

  pTDete=0.0;
  pTleadingDete=0;
  etaDete=0;
  phiDete=0;

  fconst = new TClonesArray("fourvector", 20);
}

//________________________________________________________
jet::~jet()
{
  // default destructor
  fconst->Delete();
  delete fconst;
}

//________________________________________________________
void jet::AddConst(Int_t idx, TLorentzVector lv)
{
  Double_t pT = lv.Pt();
  Double_t eta = lv.Eta();
  Double_t phi = lv.Phi();
  Double_t M = lv.M();

  new ((*fconst)[idx]) fourvector(pT, eta, phi, M);
}

