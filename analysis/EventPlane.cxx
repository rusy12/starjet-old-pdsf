#include <TSystem.h>
#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TFile.h"
#include "TRandom.h"
#include "Riostream.h"
#include "TNtuple.h"
#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoPrimaryTrack.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetPicoTower.h>
#include <TStarJetPicoTowerCuts.h>

#include "EventPlane.h"

//constructor
EventPlane::EventPlane()
{

//load parameters----------------------------------
   outDir = gSystem->Getenv("OUTPUTDIR");
   strigger = gSystem->Getenv("TRIGGER"); //MB | HT
   sInFileList = gSystem->Getenv("FILELISTFILE"); //input filelist
   nFiles = atoi(gSystem->Getenv("NFILES")); //number of input files to take
   nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES")); //skip first NSKIPFILES in the filelist
   refmultcut = atoi(gSystem->Getenv("REFMULT")); //cut on ref. multiplicity
  //zetcut = atoi(gSystem->Getenv("ZVERTEX"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------

  TString str = Form("%s/EP.root", outDir.Data());
  foutput = new TFile(str.Data(), "RECREATE");



}

//===================================================================================
EventPlane::~EventPlane()
{
	delete foutput;
}


//===================================================================================

void EventPlane::LoadCorrectionHistos()
{
//****************** Get the correction histograms for the event plane phi distribution ******************************
    cout << "Open the phi correction histograms for event plane analysis" << endl;

	 TString HistName;
    Double_t MaxEntry    = 0.0;
    Int_t Good_phi_file  = 0;
    Int_t Total_phi_file = 0;
    Int_t Start_day_phi_corr = 0;
    Int_t Stop_day_phi_corr  = 0;

    TFile *filephi_day_z_eta;
    Start_day_phi_corr = 170;
    Stop_day_phi_corr  = 181;
    filephi_day_z_eta = TFile::Open("EP_corrections/Phi_weights_AuAu27.root");  // open the file (this is also for AuAu 200)
    for(Int_t i = Start_day_phi_corr; i < Stop_day_phi_corr; i++)
    {
        hPhi_days_use[i] = 0;
        hPhi_days_in[i]  = 0;
        for(Int_t j = 0; j < nPhi_corr_z_bins; j++)
        {
            for(Int_t k = 0; k < nPhi_corr_eta_bins; k++)
            {
                for(Int_t l = 0; l < 2; l++)
                {
                    for(Int_t m = 0; m < nPhi_corr_pt_bins; m++)
                    {
                        HistName = "hPhi_corr_";
                        HistName += i;
                        HistName += "_";
                        HistName += j;
                        HistName += "_";
                        HistName += k;
                        HistName += "_";
                        HistName += l;
                        HistName += "_";
                        HistName += m;
                        if(
                           filephi_day_z_eta->FindObjectAny(HistName.Data()) != 0
                          )
                        {
                            hPhi_corr_in[i][j][k][l][m] = (TH1F*)filephi_day_z_eta->FindObjectAny(HistName.Data());
                            HistName = "hPhi_corr_in_";
                            HistName += i;
                            HistName += "_";
                            HistName += j;
                            HistName += "_";
                            HistName += k;
                            HistName += "_";
                            HistName += l;
                            HistName += "_";
                            HistName += m;
                            hPhi_corr_in[i][j][k][l][m]->SetName(HistName.Data());
                            MaxEntry = hPhi_corr_in[i][j][k][l][m]->GetBinContent(hPhi_corr_in[i][j][k][l][m]->GetMaximumBin());

// Calculate the mean of the 10 max entries to reduce fluctuations
                            Double_t Max_Mean_Val_array[10];
                            for(Int_t q = 0; q < 10; q++)
                            {
                                Max_Mean_Val_array[q] = 0.0;
                            }
                            for(Int_t p = 0; p < hPhi_corr_in[i][j][k][l][m]->GetNbinsX(); p++)
                            {
                                Double_t Phi_corr_in_val = hPhi_corr_in[i][j][k][l][m]->GetBinContent(p);
                                if(Phi_corr_in_val > Max_Mean_Val_array[9])
                                {
                                    Max_Mean_Val_array[9] = Phi_corr_in_val;
                                    for(Int_t q = 0; q < 9; q++)
                                    {
                                        if(Phi_corr_in_val > Max_Mean_Val_array[8-q])
                                        {
                                            Max_Mean_Val_array[8-q+1] = Max_Mean_Val_array[8-q];
                                            Max_Mean_Val_array[8-q]   = Phi_corr_in_val;
                                        }
                                    }
                                }
                            }
                            Double_t MaxEntry_mean = 0.0;
                            Int_t    MaxEntry_mean_counter = 0;
                            for(Int_t q = 0; q < 10; q++)
                            {
                                //cout << "Max_Mean_Val_array[" << q << "] = " << Max_Mean_Val_array[q] << endl;
                                if(Max_Mean_Val_array[q] > 0.0)
                                {
                                    MaxEntry_mean = MaxEntry_mean + Max_Mean_Val_array[q];
                                    MaxEntry_mean_counter++;
                                }
                            }
                            if(MaxEntry_mean_counter > 0)
                            {
                                MaxEntry_mean = MaxEntry_mean/((Double_t)MaxEntry_mean_counter);
                            }
                            //cout << "MaxEntry = " << MaxEntry << ", MaxEntry_mean = " << MaxEntry_mean << endl;

                            hPhi_corr_in_max_entry[i][j][k][l][m] = MaxEntry;

                            if(MaxEntry_mean > 0.0)
                            {
                                hPhi_corr_in[i][j][k][l][m]->Scale(1.0/MaxEntry_mean);
                                Total_phi_file++;
                            }
                            if(MaxEntry >= 100)
                            {
                                Good_phi_file++;
                            }

                            hPhi_days_in[i] = 1;

                       }
                    }
                }
            }
        }
    }

    for(Int_t i = 0; i < 365; i++)
    {
        for(Int_t j = 0; j < nPhi_corr_z_bins; j++)
        {
            for(Int_t k = 0; k < nPhi_corr_eta_bins; k++)
            {
                for(Int_t l = 0; l < 2; l++)
                {
                    for(Int_t m = 0; m < nPhi_corr_pt_bins; m++)
                    {
                        HistName = "hPhi_corr_";
                        HistName += i;
                        HistName += "_";
                        HistName += j;
                        HistName += "_";
                        HistName += k;
                        HistName += "_";
                        HistName += l;
                        HistName += "_";
                        HistName += m;
                        hPhi_corr[i][j][k][l][m] = new TH1F(HistName.Data(),HistName.Data(),100,-TMath::Pi(),TMath::Pi());
                    }
                }
            }
        }
    }



}//Load Correction Histograms

//===================================================================================

Double_t EventPlane::calc_event_plane_weight(Double_t phiA, Double_t p_t, Double_t eta, Int_t RunId, Double_t EventVertexZ, Int_t Polarity, Double_t &phi_w)
{
    Double_t total_weight = 1.0;
    Double_t MaxEntry = 0.0;

    Double_t sign = 0.0;
    if(eta > 0.0) sign = 1.0;
    if(eta < 0.0) sign = 1.0; // -1.0 for 1st order event plane

    Double_t weight_phi = 1.0;

    Float_t phi_corr_delta_z   = (phi_corr_z_stop-phi_corr_z_start)/((Float_t)nPhi_corr_z_bins);
    Float_t phi_corr_delta_eta = (phi_corr_eta_stop-phi_corr_eta_start)/((Float_t)nPhi_corr_eta_bins);
    Float_t phi_corr_delta_pt  = (phi_corr_pt_stop-phi_corr_pt_start)/((Float_t)nPhi_corr_pt_bins);

    //******************* Phi correction ****************************
    // determine the file time
    TString name;
    Long64_t val;
    Double_t day = 0.0;
    char NoP[50];
    sprintf(NoP,"%u",RunId);
    name = NoP[2];
    sscanf(name.Data(),"%Li",&val);
    day = 100.0 * val;
    name = NoP[3];
    sscanf(name.Data(),"%Li",&val);
    day = day + 10.0 * val;
    name = NoP[4];
    sscanf(name.Data(),"%Li",&val);
    day = day + 1.0 * val;

    Int_t day_bin     = (Int_t)day;
    Int_t z_bin       = (Int_t)((EventVertexZ-phi_corr_z_start)/phi_corr_delta_z);
    Int_t eta_bin     = (Int_t)((eta-phi_corr_eta_start)/phi_corr_delta_eta);
    Int_t pt_bin      = (Int_t)((p_t-phi_corr_pt_start)/phi_corr_delta_pt);

    if(pt_bin >= nPhi_corr_pt_bins) pt_bin = nPhi_corr_pt_bins-1;

    Int_t pol_bin        = 0;
    Int_t Phi_corr_bin   = 0;
    Int_t weight_counter = 0;
    if(Polarity > 0) pol_bin = 0;
    if(Polarity < 0) pol_bin = 1;
    if(
       day_bin    >= 0
       && eta_bin >= 0
       && z_bin   >= 0
       && pt_bin  >= 0
       && day_bin < nPhi_corr_days
       && eta_bin < nPhi_corr_eta_bins
       && pt_bin  < nPhi_corr_pt_bins
       && z_bin   < nPhi_corr_z_bins
       && hPhi_days_in[day_bin] == 1
      )
    {
        MaxEntry           = hPhi_corr_in_max_entry[day_bin][z_bin][eta_bin][pol_bin][pt_bin];
        //MaxEntry = 99; ???
        //cout << "MaxEntry = " << MaxEntry << endl;
        if(MaxEntry < 100) // phi correction file has too few entries, take correction from different day
        {
            Double_t MaxEntry_day_before   = 0.0;
            Double_t MaxEntry_day_after    = 0.0;
            Double_t weight_phi_day_before = 0.0;
            Double_t weight_phi_day_after  = 0.0;
            if((day_bin - 1) >= 0)
            {
                if(hPhi_days_in[day_bin - 1] == 1)
                {
                    MaxEntry_day_before = hPhi_corr_in_max_entry[day_bin-1][z_bin][eta_bin][pol_bin][pt_bin];
                    if(MaxEntry_day_before > 100)
                    {
                        Phi_corr_bin = hPhi_corr_in[day_bin-1][z_bin][eta_bin][pol_bin][pt_bin] ->FindBin(phiA);
                        weight_phi_day_before   = hPhi_corr_in[day_bin-1][z_bin][eta_bin][pol_bin][pt_bin] ->GetBinContent(Phi_corr_bin);
                        weight_counter++;
                    }
                }

            }
            if((day_bin + 1) < nPhi_corr_days)
            {
                if(hPhi_days_in[day_bin + 1] == 1)
                {
                    MaxEntry_day_after = hPhi_corr_in_max_entry[day_bin+1][z_bin][eta_bin][pol_bin][pt_bin];
                    if(MaxEntry_day_after > 100)
                    {
                        Phi_corr_bin = hPhi_corr_in[day_bin+1][z_bin][eta_bin][pol_bin][pt_bin] ->FindBin(phiA);
                        weight_phi_day_after   = hPhi_corr_in[day_bin+1][z_bin][eta_bin][pol_bin][pt_bin] ->GetBinContent(Phi_corr_bin);
                        weight_counter++;
                    }
                }
            }

            if(weight_counter > 0)
            {
                weight_phi = (weight_phi_day_before + weight_phi_day_after)/((Double_t)weight_counter);
                //cout << "weight_phi_day_before = " << weight_phi_day_before << ", weight_phi_day_after = " << weight_phi_day_after << endl;
            }
            else weight_phi = 1.0;
            //cout << "weight_phi = " << weight_phi << endl;

        }
        else // phi correction file has enough entries, everything ok
        {
            Phi_corr_bin = hPhi_corr_in[day_bin][z_bin][eta_bin][pol_bin][pt_bin] ->FindBin(phiA);
            weight_phi   = hPhi_corr_in[day_bin][z_bin][eta_bin][pol_bin][pt_bin] ->GetBinContent(Phi_corr_bin);
            //cout << "weight_phi = " << weight_phi << endl;
        }
    }
    //************** End phi correction ****************************

    if(weight_phi > 0.0) weight_phi = 1.0/weight_phi;
    else weight_phi = 1.0;

    if(weight_phi > 5.0) weight_phi = 5.0;  // don't correct for too much for small entries

    phi_w = weight_phi;

    Double_t p_t_weight = 1.0;
    if(p_t < 2.0)  p_t_weight = p_t;
    if(p_t >= 2.0) p_t_weight = 2.0;

    total_weight = p_t_weight*weight_phi*sign;

    //cout << "p_t_weight = " << p_t_weight << ", weight_phi = " << weight_phi << ", sign = "
    //   << sign << ", total_weight = " << total_weight << endl;

    return total_weight;
}

//===================================================================================

void EventPlane::EventPlaneAnalysis(TStarJetPicoEvent *event, TList*  primTracks)
{

      TStarJetPicoEventHeader *header = event->GetHeader();
      EventId = header->GetEventId();
      Int_t RunId = header->GetRunId();
      //const TArrayI*  TriggerIds = header->GetTriggerIdArray();
      //Int_t     NumOfTriggers = header->GetNOfTriggerIds();
      //Int_t     NumVPDEHits   = header->GetnumberOfVpdEastHits();
      //Int_t     NumVPDWHits   = header->GetnumberOfVpdWestHits();
      Float_t   VzTPC         = header->GetPrimaryVertexZ();
      //Float_t   VzVPD         = header->GetvpdVz();
      //Int_t     numPrim       = header->GetNOfPrimaryTracks();
      //Int_t     numGlobal     = header->GetNGlobalTracks();
      //Float_t rpangle = header->GetReactionPlaneAngle();
      Int_t refmult = header->GetReferenceMultiplicity();
		Float_t zvertex = VzTPC; //TPC z-vertex position


       TIter nextTrack(primTracks);
       double p_x;
       double p_y;
       double p_z;
       double p_t;
       double eta;
       double phi;
		 int charge;
      /*
		 double TracksDCAxy;
       double NFitHits; 
       double EtaDiff;
       double PhiDiff;
		*/
       TStarJetPicoPrimaryTrack* primTrack;

       while(primTrack = (TStarJetPicoPrimaryTrack*)nextTrack()  )
       {
         p_x      = primTrack->GetPx();
         p_y      = primTrack->GetPy();
         p_z      = primTrack->GetPz();
         p_t      = TMath::Sqrt(p_x*p_x+p_y*p_y);
         eta     = primTrack->GetEta(); 
         phi     = primTrack->GetPhi();
			charge  = primTrack->GetCharge();
/*
  		   TracksDCAxy  = primTrack->GetsDCAxy();
         NFitHits     = primTrack->GetNOfFittedHits();
         EtaDiff      = primTrack->GetEtaDiffHitProjected();
         PhiDiff      = primTrack->GetPhiDiffHitProjected();
*/


			 Float_t iQx   = TMath::Cos(2.0*phi);
          Float_t iQy   = TMath::Sin(2.0*phi);

          Float_t iQx1   = TMath::Cos(1.0*phi);
          Float_t iQy1   = TMath::Sin(1.0*phi);


			 Double_t phi_w;
          Double_t total_weight = calc_event_plane_weight(phi,p_t,eta,RunId,zvertex,(Int_t)charge,phi_w);

          Double_t p_t_weight = 1.0;
          if(p_t < 2.0)  p_t_weight = p_t;
          if(p_t >= 2.0) p_t_weight = 2.0;
          Double_t phi_weight = 1.0;
          if(p_t_weight > 0) phi_weight = total_weight/p_t_weight;

          EP_Qx_no_weight  += iQx;
          EP_Qy_no_weight  += iQy;

          EP_Qx_phi_weight += iQx*phi_weight;
          EP_Qy_phi_weight += iQy*phi_weight;

          Float_t iQx_add = total_weight*iQx;
          Float_t iQy_add = total_weight*iQy;

          EP_Qx    += iQx_add;
          EP_Qy    += iQy_add;

          EP_Qx_ptw += iQx*p_t_weight;
          EP_Qy_ptw += iQy*p_t_weight;


          // For eta sub-event method
          if(
             fabs(eta) > eta_gap
            )
          {
              if(eta >= 0.0)
              {
                  EP_Qx_eta_pos     += iQx_add;
                  EP_Qy_eta_pos     += iQy_add;
                  EP_Qx_eta_pos_ptw += iQx*p_t_weight;
                  EP_Qy_eta_pos_ptw += iQy*p_t_weight;
                  EP_Qx1_eta_pos_ptw += iQx1*p_t_weight;
                  EP_Qy1_eta_pos_ptw += iQy1*p_t_weight;
                  //Qtracks_used_eta_pos++;
              }
              if(eta < 0.0)
              {
                  EP_Qx_eta_neg     += iQx_add;
                  EP_Qy_eta_neg     += iQy_add;
                  EP_Qx_eta_neg_ptw += iQx*p_t_weight;
                  EP_Qy_eta_neg_ptw += iQy*p_t_weight;
                  EP_Qx1_eta_neg_ptw += iQx1*p_t_weight;
                  EP_Qy1_eta_neg_ptw += iQy1*p_t_weight;
                  //Qtracks_used_eta_neg++;
              }
          }


		}//track loop

return;
}
//===================================================================================

void EventPlane::RunEPAnalysis(Long64_t nEvents)
{
 
  TNtuple *epPos = new TNtuple("epPos","eventplane direction","EventId:EP_phi");

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  //evCuts->SetVertexZCut(30);//[cm] 
  evCuts->SetRefMultCut(refmultcut);

  readerdata.GetTrackCuts()->SetDCACut(1);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(20);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  //readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy

  
  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nEvents);
    
  while (readerdata.NextEvent() == kTRUE)
    {

      TStarJetPicoEvent *event = readerdata.GetEvent();
		TList*  primTracks  = readerdata.GetListOfSelectedTracks();
		EventPlaneAnalysis(event,primTracks); //calculate event plane q-vector
		
		//Recentering correction goes here

		//Save EP information to the tree
		epPos->Fill(EventId,EP_phi);
	
	}	

//write the output file
foutput->cd();
epPos->Write();
foutput->Close();

delete epPos;

return;
}
