#include "fourvector.h"

//____________________________________________________________________________
fourvector::fourvector()
{
  fM = 0.;
  fpT = 0.;
  feta = 0.;
  fphi = 0.;
}

//____________________________________________________________________________
fourvector::fourvector(Double_t pT, Double_t eta, Double_t phi, Double_t M)
{
  fM = M;
  fpT = pT;
  feta = eta;
  fphi = phi;
}

//____________________________________________________________________________
fourvector::~fourvector()
{
}

//____________________________________________________________________________
void fourvector::SetPtEtaPhiM(Double_t pT, Double_t eta, Double_t phi, Double_t M)
{
  fM = M;
  fpT = pT;
  feta = eta;
  fphi = phi;
}

//____________________________________________________________________________
TLorentzVector fourvector::GetTLorentzVector()
{
  TLorentzVector lv;
  lv.SetPtEtaPhiM(fpT, feta, fphi, fM);
  
  return lv;
}
