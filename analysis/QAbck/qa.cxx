#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoPrimaryTrack.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetPicoTower.h>
#include <TStarJetPicoTowerCuts.h>


#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

/*
#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
*/
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void qa(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
//  double r = atof(gSystem->Getenv("RPARAM"));
//  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
//  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcut = atoi(gSystem->Getenv("REFMULT"));
  //Int_t zetcut = atoi(gSystem->Getenv("ZVERTEX"));
//-------------------------------------------------
  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
  

  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, pp, ppHT, ppJP

  //evCuts->SetVertexZCut(30);//[cm] 
  evCuts->SetRefMultCut(refmultcut);

  readerdata.GetTrackCuts()->SetDCACut(1.5);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(22);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy

  TString str = "null";
  str = Form("%s/qa.root", outDir.Data());

  int runid = 0;
  int refmult = 0;
  Double_t zvertex = 0;   

  TFile *foutput = new TFile(str.Data(), "RECREATE");

//-------Histograms--------------
Int_t ptBins=200;
Int_t ptMin=0;
Int_t ptMax=50;
Int_t etaBins=120;
Int_t etaTBins=60;
Float_t etaMin=-1.5;
Float_t etaMax=1.5;
Int_t phiBins=360;
Float_t phiMin=-3.14159;
Float_t phiMax=3.14159;
Int_t runBins=200;
Float_t runMin=-0.5;
Float_t runMax=199.5;
Float_t dcaBins=150;
Float_t dcaMin=-1;
Float_t dcaMax=1;
Int_t zBins=120;
Float_t zMin=-30;
Float_t zMax=30;



TH1I*    hNumEvents   = new TH1I("hNumEvents" ,"number of events produced",1,0.5,1.5);
TH1D*    hNumEventsvsDay   = new TH1D("hNumEventsvsDay" ,"number of events produced after cuts per day; day; events",runBins,runMin,runMax);
TH2D*    hRefmultvsDay    = new TH2D("hRefmultvsDay","refmult vs day; refmult; day",100,0,1000,runBins,runMin,runMax);
TH2D*    hZvertexvsDay    = new TH2D("hZvertexvsDay","z_{vertex} vs day;z_{vertex} [cm]; day",zBins,zMin,zMax,runBins,runMin,runMax);


TH1D*    hTrackPt15    = new TH1D("hTrackPt15","track pT for pT>15; #p_{T} [GeV/c]; counts",ptBins,ptMin,ptMax);
TH2D*    hNHitsvsPt    = new TH2D("hNHitsvsPt","number of fitted hits vs pT; #nFittedHits; p_{T} [GeV/c]",100,0,100,ptBins,ptMin,ptMax);
TH3D*    hTrackEtavsPhiDay = new TH3D("hTrackEtavsPhiDay","Eta and Phi position of tracks with pT>0.2 GeV/c",etaBins,etaMin,etaMax,phiBins,phiMin,phiMax,runBins,runMin,runMax);
TH3D*    hTrackEtavszDay = new TH3D("hTrackEtavszDay","Eta position of tracks with pT>0.2 GeV/c vs z",etaBins,etaMin,etaMax,zBins,zMin,zMax,runBins,runMin,runMax);
TH2D*    hTrackEtavsPhi    = new TH2D("hTrackEtavsPhi","Eta and Phi position of tracks with pT>0.2 GeV/c; #eta; #phi",etaBins,etaMin,etaMax,phiBins,phiMin,phiMax);
TH2D*    hTrackPhivsPt    = new TH2D("hTrackPhivsPt","track Phi vs pT; #phi; p_{T} [GeV/c]",phiBins,phiMin,phiMax,ptBins,ptMin,ptMax);
TH2D*    hTrackEtavsPt    = new TH2D("hTrackEtavsPt","track Eta vs pT; #eta; p_{T} [GeV/c]",etaBins,etaMin,etaMax,ptBins,ptMin,ptMax);
TH2D*    hTrackPtvsDay    = new TH2D("hTrackPtvsDay","track pT vs day; p_{T} [GeV/c]; day",ptBins,ptMin,ptMax,runBins,runMin,runMax);
TH2D*    hTrackDCAvsDay   = new TH2D("hTrackDCAvsDay","track sDCAxy vs day; DCAxy [cm]; day",dcaBins,dcaMin,dcaMax,runBins,runMin,runMax);
TH2D*    hTrackDCAvsPt   = new TH2D("hTrackDCAvsPt","track sDCAxy vs pT; DCAxy [cm]; p_{T} [GeV/c]",dcaBins,dcaMin,dcaMax,ptBins,ptMin,ptMax);
TH3D*    hTrackDCAvsPtvsDay   = new TH3D("hTrackDCAvsPtvsDay","track sDCAxy vs pT vs Day; DCAxy [cm]; p_{T} [GeV/c]; day",dcaBins,dcaMin,dcaMax,ptBins,ptMin,ptMax,runBins,runMin,runMax);

TH2D*    hHitTowervsDay2 = new TH2D("hHitTowervsDay2"  ,"hits above 2 GeV in towers vs day",4802,0-0.5,4802-0.5,runBins,runMin,runMax);
TH1D*    hHitTower2 = new TH1D("hHitTower2"  ,"hits above 2 GeV in towers",4802,0-0.5,4802-0.5);
TH2D*    hHitTowervsDay5 = new TH2D("hHitTowervsDay5"  ,"hits above 5 GeV in towers vs day",4802,0-0.5,4802-0.5,runBins,runMin,runMax);
TH1D*    hHitTower5 = new TH1D("hHitTower5"  ,"hits above 5 GeV in towers",4802,0-0.5,4802-0.5);
TH2D*    hHitTowervsDay10 = new TH2D("hHitTowervsDay10"  ,"hits above 10 GeV in towers vs day",4802,0-0.5,4802-0.5,runBins,runMin,runMax);
TH1D*    hHitTower10 = new TH1D("hHitTower10"  ,"hits above 10 GeV in towers",4802,0-0.5,4802-0.5);
TH2D*    hTowTotEtvsDay = new TH2D("hTowTotEtvsDay","transversal energy deposit in the tower vs day",4802,0-0.5,4802-0.5,runBins,runMin,runMax);
TH1D*    hTowTotEt = new TH1D("hTowTotEt","transversal energy deposit in the tower",4802,0-0.5,4802-0.5);
TH3D*    hEtavsPhivsDay2  = new TH3D("hEtavsPhivsDay","Eta and Phi position of towers with Et>2 GeV vs day",etaTBins,etaMin,etaMax,phiBins,phiMin,phiMax,runBins,runMin,runMax);
TH2D*    hEtavsPhi2  = new TH2D("hEtavsPhi","Eta and Phi position of towers with Et>2 GeV",etaTBins,etaMin,etaMax,phiBins,phiMin,phiMax);


//--------------------------------
  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
  readerdata.Init(nev);
    
	int ntracks_avg=0;
  while (readerdata.NextEvent() == kTRUE)
    {
		hNumEvents->Fill(1);

      TStarJetPicoEvent *event = readerdata.GetEvent();

      TStarJetPicoEventHeader *header = event->GetHeader();
      Int_t EventId = header->GetEventId();
      Int_t runid = header->GetRunId();
      const TArrayI*  TriggerIds = header->GetTriggerIdArray();
      Int_t     NumOfTriggers = header->GetNOfTriggerIds();
      //Int_t     NumVPDEHits   = header->GetnumberOfVpdEastHits();
      //Int_t     NumVPDWHits   = header->GetnumberOfVpdWestHits();
      Float_t   VzTPC         = header->GetPrimaryVertexZ();
      //Float_t   VzVPD         = header->GetvpdVz();
      Int_t     numPrim       = header->GetNOfPrimaryTracks();
      Int_t     numGlobal     = header->GetNGlobalTracks();
      Float_t rpangle = header->GetReactionPlaneAngle();
      Float_t refmult = header->GetReferenceMultiplicity();
		Double_t zvertex = header->GetPrimaryVertexZ();

      Int_t day = (Int_t)(runid/1000)-12000;
      hNumEventsvsDay->Fill(day);
      hRefmultvsDay->Fill(refmult,day);
      hZvertexvsDay->Fill(zvertex,day);

/*		cout<<"TRIGGERS"<<endl;
     for(int member=0; member<TriggerIds->GetSize(); member++){
			cout<<"trigger ID: "<<TriggerIds->GetAt(member)<<endl;
		}
*/
		//Primary tracks
		TList*  primTracks          = readerdata.GetListOfSelectedTracks();

       TIter nextTrack(primTracks);
       double TrackPx;
       double TrackPy;
       double TrackPz;
       double TrackPt;
       double TrackEta;
       double TrackPhi;
       double EtaDiff;
       double PhiDiff;
       double TracksDCAxy;
       double NFitHits; 

       TStarJetPicoPrimaryTrack* primTrack;

		Int_t ntracks=0;
       while(primTrack = (TStarJetPicoPrimaryTrack*)nextTrack()  )
       {
         TrackPx      = primTrack->GetPx();
         TrackPy      = primTrack->GetPy();
         TrackPz      = primTrack->GetPz();
         TrackPt      = TMath::Sqrt(TrackPx*TrackPx+TrackPy*TrackPy);
         TrackEta     = primTrack->GetEta(); 
         TrackPhi     = primTrack->GetPhi();
  		   TracksDCAxy  = primTrack->GetsDCAxy();
         EtaDiff      = primTrack->GetEtaDiffHitProjected();
         PhiDiff      = primTrack->GetPhiDiffHitProjected();
         NFitHits     = primTrack->GetNOfFittedHits();

 		   hTrackEtavsPt->Fill(TrackEta,TrackPt);
   		hTrackPhivsPt->Fill(TrackPhi,TrackPt); 
		   hTrackPtvsDay->Fill(TrackPt,day);
	   	hTrackDCAvsDay->Fill(TracksDCAxy,day);
		   hTrackDCAvsPt->Fill(TracksDCAxy,TrackPt);
		   hTrackDCAvsPtvsDay->Fill(TracksDCAxy,TrackPt,day);	
		   hTrackEtavsPhi->Fill(TrackEta,TrackPhi);
		   hTrackEtavsPhiDay->Fill(TrackEta,TrackPhi,day);
	   	hTrackEtavszDay->Fill(TrackEta,zvertex,day);
		   hNHitsvsPt->Fill(NFitHits,TrackPt); 

			ntracks++;
    	 }

ntracks_avg+=ntracks;
	//TOWERS
	TList*  towers          = readerdata.GetListOfSelectedTowers();

       TIter nextTower(towers);
       double TowerE;
		 double TowerEt;
       double TowerEta;
       double TowerEtaC;
       double TowerPhi;
       Int_t  TowerId;

       TStarJetPicoTower* ptower;

       while(ptower = (TStarJetPicoTower*)nextTower()  )
       {
   		TowerE      = ptower->GetEnergy();
	      TowerId     = ptower->GetId();
       	TowerEta    = ptower->GetEta();
       	TowerPhi    = ptower->GetPhi();
       	TowerEtaC   = ptower->GetEtaCorrected();
         TowerEt = TowerE/TMath::CosH(TowerEtaC);

			//FILLING HISTOGRAMS
			hTowTotEt->Fill(TowerId,TowerEt);
			hTowTotEtvsDay->Fill(TowerId,day,TowerEt);
			if(TowerEt>2.0){
				hHitTowervsDay2->Fill(TowerId,day);
				hHitTower2->Fill(TowerId);
				hEtavsPhivsDay2->Fill(TowerEta,TowerPhi,day);
				hEtavsPhi2->Fill(TowerEta,TowerPhi);
			}
			if(TowerEt>5.0)
				hHitTower5->Fill(TowerId);
				hHitTowervsDay5->Fill(TowerId,day);
			if(TowerEt>10.0);
				hHitTower10->Fill(TowerId);
				hHitTowervsDay10->Fill(TowerId,day);
	  	 }
   	 readerdata.PrintStatus(60); //every 1min 
  }
int nevents=hNumEvents->GetEntries();
ntracks_avg=ntracks_avg/nevents;
cout<<"N tracks:"<<ntracks_avg<<endl;
  readerdata.PrintStatus();
  foutput->Write();
  foutput->Close();
}
