#ifndef __run_pythia__hh
#define __run_pythia__hh

#include "TPythia6.h"
#include "TClonesArray.h"

double MakeCone(TPythia6 *pythia, TClonesArray *pararr,Double_t pT, Double_t jetEta, Double_t jetPhi, Bool_t charged=1, TString frag="u");

#endif
