#ifndef __prior_maker_hh
#define __prior_maker_hh

#include "TObject.h"
#include "Rtypes.h"
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

class prior_maker : public TObject
 {

	public:
	prior_maker();
	virtual ~prior_maker(); 
	void init(const TString input,const TString output);
	//Double_t epsilon(const Float_t  pT,const Float_t rad, const Int_t pTthresh);
	void prior_histos(const Int_t njets=1E5);

	private:
	static const Int_t npriors_py=3;
	static const int nr=4;
	static const Int_t nLeadCuts=6;
	static const Int_t nptbins=800;
	static const Float_t ptminbin=-100;
	static const Float_t ptmaxbin=100;
	static const Int_t npows=4;
	static const Int_t firstpow=3;


	Int_t lastpow;
	Float_t pTleadcuts[nLeadCuts];
	Float_t R[nr];
 	TFile *fpyt[nr];
 	TFile *foutput;
	TH1D* hprior_py[nr][nLeadCuts];
	TH1D* hprior_pyde[nr][nLeadCuts];
	TH1D* hdiv_py[nr][nLeadCuts];
	TH1D* hdiv_pyde[nr][nLeadCuts];

   TH1D *hprior_pythia[nr][nLeadCuts];
   TH1D *hprior_pythiadete[nr][nLeadCuts];
	TH1D *hprior_powlaw[npows][nLeadCuts];
	TH1D *hprior_flat[nLeadCuts];
   TH1D *hprior_gauss[nLeadCuts];
   TH1D *hprior_levy[nLeadCuts];
   TH1D *hprior_levy_alex[nLeadCuts];

	
	TString str;

   ClassDef(prior_maker, 1)
};

#endif
