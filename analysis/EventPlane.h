#ifndef __EventPlane__hh
#define __EventPlane__hh

#include "TH1F.h"
#include "TFile.h"
#include "TList.h"
#include <TStarJetPicoEvent.h>

// Phi correction histograms
static const Int_t nPhi_corr_days     = 365; // 365
static const Int_t nPhi_corr_z_bins   = 5;   // 5
static const Int_t nPhi_corr_eta_bins = 6;   // 6
static const Int_t nPhi_corr_pt_bins  = 4;   // 4
static const Float_t phi_corr_z_start=-30; //Alex: -40
static const Float_t phi_corr_z_stop=30;  //Alex: 40
static const Float_t phi_corr_eta_start = -1.0; 
static const Float_t phi_corr_eta_stop  = 1.0;
static const Float_t phi_corr_pt_start  = 0.0;  // 0.0
static const Float_t phi_corr_pt_stop   = 1.6;
static const Float_t eta_gap = 0.05;

static TH1F* hPhi_corr[nPhi_corr_days][nPhi_corr_z_bins][nPhi_corr_eta_bins][2][nPhi_corr_pt_bins];  // [2] for different polarities, 0 = +, 1 = -
static TH1F* hPhi_corr_in[nPhi_corr_days][nPhi_corr_z_bins][nPhi_corr_eta_bins][2][nPhi_corr_pt_bins];
static Double_t hPhi_corr_in_max_entry[nPhi_corr_days][nPhi_corr_z_bins][nPhi_corr_eta_bins][2][nPhi_corr_pt_bins];
static Int_t hPhi_days_use[nPhi_corr_days];
static Int_t hPhi_days_in[nPhi_corr_days];

//Event plane vectors
static Double_t EP_Qx_no_weight;
static Double_t EP_Qy_no_weight;
static Double_t EP_Qx_phi_weight;
static Double_t EP_Qy_phi_weight;
static Double_t EP_Qx;
static Double_t EP_Qy;
static Double_t EP_Qx_ptw;
static Double_t EP_Qy_ptw;
static Double_t EP_Qx_eta_pos ;
static Double_t EP_Qy_eta_pos;
static Double_t EP_Qx_eta_pos_ptw;
static Double_t EP_Qy_eta_pos_ptw;
static Double_t EP_Qx1_eta_pos_ptw;
static Double_t EP_Qy1_eta_pos_ptw;
static Double_t EP_Qx_eta_neg;
static Double_t EP_Qy_eta_neg;
static Double_t EP_Qx_eta_neg_ptw ;
static Double_t EP_Qy_eta_neg_ptw ;
static Double_t EP_Qx1_eta_neg_ptw;
static Double_t EP_Qy1_eta_neg_ptw;

static Double_t EP_phi;
static Int_t EventId;
  
class EventPlane
{
 public:
	EventPlane();
	~EventPlane(); 

	void LoadCorrectionHistos();
	void RunEPAnalysis(Long64_t nEvents); 	


private:
	Double_t calc_event_plane_weight(Double_t phiA, Double_t p_t, Double_t eta, Int_t RunId, Double_t EventVertexZ, Int_t Polarity, Double_t &phi_w);
void EventPlaneAnalysis(TStarJetPicoEvent *event, TList* primTracks);

protected:
  TFile* foutput;
  TString outDir;
  TString strigger;
  Float_t max_rap;
  TString sInFileList;
  Int_t nFiles;
  Int_t nSkipFiles;
  Int_t refmultcut;
  Int_t zetcut;

};
#endif
