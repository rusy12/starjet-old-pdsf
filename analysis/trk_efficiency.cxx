#include <TSystem.h>
#include <TRandom.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TH2D.h>

#include "utils.h"
#include "trk_efficiency.h"

void trk_efficiency(int nstat)
{

Int_t nbins=100;
Double_t pTmin=0;
Double_t pTmax=10;

Double_t etamin=-1;
Double_t etamax=1;


TString outdir="../out_test";
TString outfile=Form("%s/trk_effi.root",outdir.Data());
TFile* fout = new TFile(outfile.Data(), "recreate");

TH1D* heff=new TH1D("heff","efficiency",60,-1,2);
TH1D* effpT=new TH1D("heffpT","efficiency vs pT;p_{T}",nbins,pTmin,pTmax);
TH1D* effeta=new TH1D("heffeta","efficiency vs eta;#eta",nbins,etamin,etamax);
TH2D* effetapT=new TH2D("heffetapT", "efficiency vs eta vs pT;p_{T};#eta",nbins,pTmin,pTmax, nbins, etamin, etamax);

for(Int_t step=0; step<nstat; step++){
Double_t pT=gRandom->Uniform(pTmin,pTmax);
Double_t eta=gRandom->Uniform(etamin,etamax);
Double_t eff=1; /*efficiency(eta,pT);*/
heff->Fill(eff);
effpT->Fill(pT,eff);
effeta->Fill(eta,eff);
effetapT->Fill(pT,eta,eff);

}

heff->Write("heff");
effpT->Write("heffpT");
effeta->Write("heffeta");
effetapT->Write("heffetapT");

fout->Close();

return;
}
