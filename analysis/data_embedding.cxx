#include <TSystem.h>
#include <TFile.h>
#include <TH2F.h>
#include <TRandom.h>
//#include <TRandom1.h>
//#include <TDatime.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TPythia6.h>
#include <TParticle.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "StRefMultCorr.h"
#include "StMemStat.h"
#include "embedding.h"
#include "find_embedded_jet.h"
#include "fjwrapper.h"
#include "fourvector.h"
#include "utils.h"
//#include "run_pythia.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

void data_embedding(Long64_t nev)
{
//load parameters----------------------------------
  //TString dataDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  TString strigger = gSystem->Getenv("TRIGGER");
  double r = atof(gSystem->Getenv("RPARAM"));
  Bool_t charged = atoi(gSystem->Getenv("CHARGED"));
  Float_t max_rap = atoi(gSystem->Getenv("MAXRAP"));
  TString sInFileList = gSystem->Getenv("FILELISTFILE");
  Int_t nFiles = atoi(gSystem->Getenv("NFILES"));
  Int_t nSkipFiles = atoi(gSystem->Getenv("NSKIPFILES"));
  Int_t refmultcutMin = atoi(gSystem->Getenv("REFMULTMIN"));
  Int_t refmultcutMax = atoi(gSystem->Getenv("REFMULTMAX"));
  //Int_t doEffiCorr = atoi(gSystem->Getenv("DOEFFICORR"));
  Int_t pTembFixed = atoi(gSystem->Getenv("PTEMBFIXED"));
  Int_t njetsremove=atoi(gSystem->Getenv("NJETSREMOVE"));

	float rkt=0.4; //R size for kT jets (for rho calculation)
//-------------------------------------------------

  cout << Form("[i] Output=%s",outDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Trigger=%s", r, strigger.Data()) << endl;
  cout << Form("[i] Files from: %s", sInFileList.Data()) << endl;
  cout << Form("[i] NFiles: %d NSkipFiles: %d", nFiles, nSkipFiles) << endl;
//-------------------------------------------------
//SETTING RANDOM SEED
/*  TDatime dt;
  UInt_t curtime = dt.Get();
  UInt_t procid = gSystem->GetPid();
  UInt_t seed = curtime - procid;
TRandom1 *rndNum = new TRandom1(seed);
cout<<"setting seed: "<<seed<<endl;*/
//-------------------------------------------------
  TStarJetPicoReader readerdata;
  TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection(strigger.Data()); //All, MB, HT, central, pp, ppHT, ppJP
  //cout << "[i] Setting refmult cut for AuAu 10\% central" << endl;
  //evCuts->SetRefMultCut(refmultcut); //cut on corrected refmult instead
  evCuts->SetVertexZCut(30.); 

  readerdata.GetTrackCuts()->SetDCACut(1.);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(14);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(0.55);
  readerdata.SetApplyMIPCorrection(kFALSE);
  //readerdata.SetApplyFractionHadronicCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy
  const Double_t pTcut=0.2; //minimal pT of tracks
  const Double_t pTmax=30.0; //maximal pT of tracks

  const Int_t nemb =16; 
  Double_t fEmbPt[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 15.0, 20.0, 30.0, 40.0, 50.0, 70.0, 90.0};
//  const Double_t pTembMin=0.2; //maximal pT of embedded jets
//  const Double_t pTembMax=50; //maximal pT of embedded jets
  TString str = "null";
  str = Form("%s/embedding_R%.1lf.root", outDir.Data(), r);

  StMemStat memstat;
  memstat.Start();

// SETTING RANDOM SEED
  TDatime dt;
  UInt_t curtime = dt.Get();
  UInt_t procid = gSystem->GetPid();
  UInt_t seed = curtime - procid;
  gRandom->SetSeed(seed);
cout<<"[i] Random seed set to "<<seed<<endl;

 // PYTHIA
  TPythia6 * fpythia = new TPythia6();
  fpythia->SetMRPY(1, seed);
//cout<<"Pythia initialized"<<endl;

  int runid = 0;
  int refmult = 0;
  int refmultCor = 0; //corrected refmult
  int zdcrate = 0;
  Double_t zvertex = 0;
  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  Double32_t rpangle = 0; //[0, 0, 16]
  TClonesArray *embedding_arr = new TClonesArray("embedding", nemb); //single particle jet
  TClonesArray *emb_pythia_part_arr = new TClonesArray("embedding", nemb); //pythia particle level, no BG
  //TClonesArray *emb_pythia_dete_arr = new TClonesArray("embedding", nemb); //pythia detector level, no BG, pT jet scaled so pTjet=pTemb
  TClonesArray *emb_pythia_dete_arr2 = new TClonesArray("embedding", nemb); //pythia detector level, no BG
  TClonesArray *emb_pythia_dete_rec_arr = new TClonesArray("embedding", nemb); //pythia detector level, with BG, pT jet scaled so pTjet=pTemb
  TClonesArray *emb_pythia_dete_rec_arr2 = new TClonesArray("embedding", nemb); //pythia detector level, with BG, no pT scaling
  TClonesArray *simarr = new TClonesArray("fourvector", 1000);
  TClonesArray *simarr_corr = new TClonesArray("fourvector", 1000);
  TClonesArray *simarr_corr2 = new TClonesArray("fourvector", 1000);

  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TTree* ftreejets = new TTree("Embedding","Embedding");
  TBranch *br_runid = ftreejets->Branch("runid", &runid);
  TBranch *br_embedding = ftreejets->Branch("embedding", &embedding_arr); 
  TBranch *br_emb_pythia_part = ftreejets->Branch("emb_pythia_part", &emb_pythia_part_arr); 
  //TBranch *br_emb_pythia_dete = ftreejets->Branch("emb_pythia_dete", &emb_pythia_dete_arr); 
  TBranch *br_emb_pythia_dete2 = ftreejets->Branch("emb_pythia_dete2", &emb_pythia_dete_arr2); 
  TBranch *br_emb_pythia_dete_rec = ftreejets->Branch("emb_pythia_dete_rec", &emb_pythia_dete_rec_arr); 
  TBranch *br_emb_pythia_dete_rec2 = ftreejets->Branch("emb_pythia_dete_rec2", &emb_pythia_dete_rec_arr2); 
  TBranch *br_refmult = ftreejets->Branch("refmult", &refmult);
  TBranch *br_rho = ftreejets->Branch("rho", &rho);
  TBranch *br_sigma = ftreejets->Branch("sigma", &sigma);
  TBranch *br_rpangle = ftreejets->Branch("rpangle", &rpangle);

  StRefMultCorr* refmultCorrUtil  = new StRefMultCorr("refmult");

  TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),
							 "JetTree",
							 nFiles,
							 nSkipFiles);
  readerdata.SetInputChain(ch);
cout<<"Initializing picoreader"<<endl;
  readerdata.Init(nev);
    
  Int_t evt = 0;
	cout<<"doing event 0"<<endl;
  std::vector<fastjet::PseudoJet> input_vector;

  while (readerdata.NextEvent() == kTRUE)
    {
//		cout<<"Memory used: "<<memstat.Used()<<endl;
      embedding_arr->Delete();
      emb_pythia_part_arr->Delete();
      //emb_pythia_dete_arr->Delete();
      emb_pythia_dete_arr2->Delete();
      emb_pythia_dete_rec_arr->Delete();
      emb_pythia_dete_rec_arr2->Delete();
      simarr->Delete();
      simarr_corr->Delete();
      simarr_corr2->Delete();
      TStarJetVectorContainer<TStarJetVector>* containerdata = readerdata.GetOutputContainer();
      
   if(charged){
		input_vector = make_charged_input_vector(containerdata, pTcut,pTmax);
		}    
   else
	input_vector = make_input_vector(containerdata, pTcut,pTmax);
      
      if (input_vector.size() <= 0) continue;
      
      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      rpangle = header->GetReactionPlaneAngle();
      refmult = header->GetReferenceMultiplicity();
      runid = header->GetRunId();
      zvertex = header->GetPrimaryVertexZ();
      zdcrate = header->GetZdcCoincidenceRate();

		//calculate corrected refmult
   	refmultCorrUtil->init(runid);
      if(refmultCorrUtil->isBadRun(runid)) continue;
      refmultCorrUtil->initEvent(refmult, zvertex, zdcrate) ;
      refmultCor = refmultCorrUtil->getRefMultCorr() ;
      if(refmultCor<=refmultcutMin || refmultCor>refmultcutMax)continue;


      std::vector<fastjet::PseudoJet> input_vector_data = input_vector;

      FJWrapper kt_data;
      kt_data.r = rkt;
      kt_data.maxrap = max_rap;
      kt_data.algor = fastjet::kt_algorithm;
		kt_data.ghost_area = 0.01;
      kt_data.input_particles = input_vector_data;
      kt_data.Run();

		std::vector<fastjet::PseudoJet> jets = kt_data.inclusive_jets;
		Int_t njets=jets.size();
	if(njets>njetsremove)
      kt_data.GetMedianAndSigma(rho, sigma,njetsremove);  //remove n most energetic jets from the calculation
 	else
		kt_data.GetMedianAndSigma(rho, sigma,0);
   
	//for (Int_t iemb = 0; iemb < nemb; iemb++){
   for (Int_t iemb = 0; iemb < nemb; iemb++){

	  //set embedded pT, eta, phi
	  //Double_t pT_emb=gRandom->Uniform(pTembMin,pTembMax);
	  Double_t pT_emb=fEmbPt[iemb];
	  Double_t eta_rnd = gRandom->Uniform(-max_rap + r, max_rap - r);
	  Double_t phi_rnd = gRandom->Uniform(0, 2.*TMath::Pi());
/*
	  if(pTembFixed)pT_emb = fEmbPt[iemb]; //pT probe fixed
	  else { //pT probe in an interval
		if(iemb==(nemb-1)) pT_emb =gRandom->Uniform(fEmbPt[iemb],50);
		else pT_emb =gRandom->Uniform(fEmbPt[iemb],fEmbPt[iemb+1]);
	  }
*/
	  
	  std::vector<fastjet::PseudoJet> input_vector_sp = input_vector; //single particle jet embedded into event
/*	  std::vector<fastjet::PseudoJet> input_vector_pythia_dete_rec = input_vector; //detector level pythia jet embedded into event, pT jet scaled so pTjet=pTemb
	  std::vector<fastjet::PseudoJet> input_vector_pythia_dete_rec2 = input_vector; //detector level pythia jet embedded into event
	  std::vector<fastjet::PseudoJet> input_vector_pythia_part; //particle level pythia jet
	  //std::vector<fastjet::PseudoJet> input_vector_pythia_dete; //detector level pythia jet
	  std::vector<fastjet::PseudoJet> input_vector_pythia_dete2; //detector level pythia jet
*/
	  //SINGLE PARTICLE JETS
	  TLorentzVector v;
	  v.SetPtEtaPhiM(pT_emb, eta_rnd, phi_rnd, 0);
	  fastjet::PseudoJet embededParticle(v.Px(), v.Py(), v.Pz(), v.E());
	  embededParticle.set_user_index(99999);
	  input_vector_sp.push_back(embededParticle);

/*     //PYTHIA JETS
     Double_t jetPt_corr = MakeCone(fpythia, simarr, simarr_corr, simarr_corr2, pT_emb, eta_rnd, phi_rnd, r, pTcut, charged);

//cout<<"corr pT: "<<jetPt_corr<<" emb pT: "<<pT_emb<<endl;
//cout<<"pTemb="<<pT_emb<<" jetpT="<<jetPt<<endl;
	  Int_t Nparticles = simarr->GetEntries();
		//cout<<"number of particles in jet: "<<Nparticles;
	  //TLorentzVector corr_jetlv(0., 0., 0., 0.);
	  for(Int_t ipart = 0; ipart < Nparticles; ipart++)
	   {
	  		fourvector *fv = (fourvector*)simarr->At(ipart);
	      TLorentzVector lv = fv->GetTLorentzVector();
//cout<<"particle "<<ipart<<" pT="<<lv.Pt()<<endl;
			fastjet::PseudoJet embededParticle(lv.Px(), lv.Py(), lv.Pz(), lv.E());
	      embededParticle.set_user_index(99999);
         input_vector_pythia_part.push_back(embededParticle);
		}
     simarr->Clear("C");     

		//pythia - detector level, scaled pT jet so pTjet=pTemb
	  Nparticles = simarr_corr->GetEntries();
	//cout<<"after correction: "<<Nparticles<<endl;
	  TLorentzVector corr_jetlv(0., 0., 0., 0.);
	  for(Int_t ipart = 0; ipart < Nparticles; ipart++)
 		{
	  		fourvector *fv = (fourvector*)simarr_corr->At(ipart);
	      TLorentzVector lv = fv->GetTLorentzVector();
//cout<<"particle "<<ipart<<" pT="<<lv.Pt()<<endl;
			corr_jetlv+=lv;
			fastjet::PseudoJet embededParticle(lv.Px(), lv.Py(), lv.Pz(), lv.E());
	      embededParticle.set_user_index(99999);
         //input_vector_pythia_dete.push_back(embededParticle);
			input_vector_pythia_dete_rec.push_back(embededParticle);
		}
     simarr_corr->Clear("C");     

		//pythia - detector level, no pT scaling
	  Int_t Nparticles2 = simarr_corr2->GetEntries();
	//cout<<"after correction: "<<Nparticles<<endl;
	  TLorentzVector corr_jetlv2(0., 0., 0., 0.);
	  for(Int_t ipart = 0; ipart < Nparticles2; ipart++)
 		{
	  		fourvector *fv = (fourvector*)simarr_corr2->At(ipart);
	      TLorentzVector lv = fv->GetTLorentzVector();
//cout<<"particle "<<ipart<<" pT="<<lv.Pt()<<endl;
			corr_jetlv2+=lv;
			fastjet::PseudoJet embededParticle(lv.Px(), lv.Py(), lv.Pz(), lv.E());
	      embededParticle.set_user_index(99999);
         input_vector_pythia_dete2.push_back(embededParticle);
			input_vector_pythia_dete_rec2.push_back(embededParticle);
		}
     simarr_corr2->Clear("C");     

//recalculate eta_rnd and phi_rnd for the jet after efficiency correction
Double_t eta_rnd_corr;
if(Nparticles==0)eta_rnd_corr=-10;
else eta_rnd_corr=corr_jetlv.PseudoRapidity();
Double_t phi_rnd_corr=corr_jetlv.Phi();
//Double_t eta_rnd_corr=eta_rnd;
//Double_t phi_rnd_corr=phi_rnd;
phi_rnd_corr=(phi_rnd_corr<0) ? phi_rnd_corr+2*TMath::Pi() : phi_rnd_corr;

Double_t eta_rnd_corr2;
if(Nparticles2==0)eta_rnd_corr2=-10;
else eta_rnd_corr2=corr_jetlv2.PseudoRapidity();
Double_t phi_rnd_corr2=corr_jetlv2.Phi();
*/
/*
cout<<"pT - eta - phi"<<endl;
cout<<"emb "<<pT_emb<<" "<<eta_rnd<<"  "<<phi_rnd<<endl;
cout<<"dete corr "<<corr_jetlv.Perp()<<" "<<eta_rnd_corr<<"  "<<phi_rnd_corr<<endl;
*/

//********************
//*JET RECONSTRUCTION*
//********************

     //single aprticle jets
	  FJWrapper akt_data_emb;
	  akt_data_emb.r = r;
	  akt_data_emb.maxrap = max_rap;
	  akt_data_emb.algor = fastjet::antikt_algorithm;
	  akt_data_emb.ghost_area = 0.01;
	  akt_data_emb.input_particles = input_vector_sp;
	  akt_data_emb.Run();

	  new ((*embedding_arr)[iemb]) embedding();
	  find_jet_sp(&akt_data_emb, (embedding*)embedding_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, rho);

/*	  //pythia jets - particle level, no BG
	  FJWrapper akt_data_emb2;
	  akt_data_emb2.r = r;
	  akt_data_emb2.maxrap = max_rap;
	  akt_data_emb2.algor = fastjet::antikt_algorithm;

	  akt_data_emb2.input_particles = input_vector_pythia_part;
	  akt_data_emb2.Run();
	  new ((*emb_pythia_part_arr)[iemb]) embedding();
     find_jet_seed(&akt_data_emb2, (embedding*)emb_pythia_part_arr->At(iemb), pT_emb, eta_rnd, phi_rnd, rho);

*/	  		
			//pythia jets - detector level, no BG
	  		/*FJWrapper akt_data_emb3;
			  akt_data_emb3.r = r;
			  akt_data_emb3.maxrap = max_rap;
			  akt_data_emb3.algor = fastjet::antikt_algorithm;

			  akt_data_emb3.input_particles = input_vector_pythia_dete;
			  akt_data_emb3.Run();
			  new ((*emb_pythia_dete_arr)[iemb]) embedding();
			  find_jet_seed(&akt_data_emb3, (embedding*)emb_pythia_dete_arr->At(iemb), pT_emb, eta_rnd_corr, phi_rnd_corr, rho);*/
				//cout<<"Memory used1: "<<memstat.Used()<<endl;

	  //pythia jets - detector level, with BG, jet pT scaled so pTjet=pTemb
/*	  FJWrapper akt_data_emb4;
	  akt_data_emb4.r = r;
	  akt_data_emb4.maxrap = max_rap;
	  akt_data_emb4.algor = fastjet::antikt_algorithm;

	  akt_data_emb4.input_particles = input_vector_pythia_dete_rec;
	  akt_data_emb4.Run();
	  new ((*emb_pythia_dete_rec_arr)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb4, (embedding*)emb_pythia_dete_rec_arr->At(iemb), pT_emb, eta_rnd_corr, phi_rnd_corr, r, rho);
		//cout<<"Memory used2: "<<memstat.Used()<<endl;

	  //pythia jets - detector level, no BG, no pT scaling
	  FJWrapper akt_data_emb5;
	  akt_data_emb5.r = r;
	  akt_data_emb5.maxrap = max_rap;
	  akt_data_emb5.algor = fastjet::antikt_algorithm;

	  akt_data_emb5.input_particles = input_vector_pythia_dete2;
	  akt_data_emb5.Run();
	  new ((*emb_pythia_dete_arr2)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb5, (embedding*)emb_pythia_dete_arr2->At(iemb), pT_emb, eta_rnd_corr2, phi_rnd_corr2, r, rho);

	  //pythia jets - detector level, with BG, no pT scaling
	  FJWrapper akt_data_emb6;
	  akt_data_emb6.r = r;
	  akt_data_emb6.maxrap = max_rap;
	  akt_data_emb6.algor = fastjet::antikt_algorithm;

	  akt_data_emb6.input_particles = input_vector_pythia_dete_rec2;
	  akt_data_emb6.Run();
	  new ((*emb_pythia_dete_rec_arr2)[iemb]) embedding();
	  find_jet_seed(&akt_data_emb6, (embedding*)emb_pythia_dete_rec_arr2->At(iemb), jetPt_corr, eta_rnd_corr2, phi_rnd_corr2, r, rho);

*/
   }//embedding_pT   
      foutput->cd();
      ftreejets->Fill();
      readerdata.PrintStatus(60); //every 1min 

      evt++;
    }

  foutput->cd();
  ftreejets->Write();
  foutput->Close();


  readerdata.PrintStatus();
  memstat.Stop();
}
