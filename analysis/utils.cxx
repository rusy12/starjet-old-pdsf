#include <iostream>
#include <TRandom.h>
#include <TFile.h>
#include <TF1.h>
#include "utils.h"
using namespace std;

std::vector<fastjet::PseudoJet> 
make_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
		  double minpt, double maxpt, Int_t indexOffset)
{
  TStarJetVector *part = 0;
  std::vector<fastjet::PseudoJet> vec;
    
  for (Int_t ip = 0; ip < container->GetEntries(); ip++)
    {
      part = container->Get(ip);
      if (part->Pt() > minpt && part->Pt() < maxpt)
	{
	  Double_t energy = TMath::Sqrt(part->Px() * part->Px() + 
					part->Py() * part->Py() + 
					part->Pz() * part->Pz());
	  
	  fastjet::PseudoJet pseudoJet(part->Px(), 
				       part->Py(), 
				       part->Pz(), 
				       energy);
	  pseudoJet.set_user_index(ip+indexOffset); // keep track of particles
	  vec.push_back(pseudoJet);
	}
    }
  return vec;
}

std::vector<fastjet::PseudoJet> 
make_charged_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
			  double minpt, double maxpt, Int_t indexOffset)
{
  TStarJetVector *part = 0;
  std::vector<fastjet::PseudoJet> vec;
    
  for (Int_t ip = 0; ip < container->GetEntries(); ip++)
    {
      part = container->Get(ip);
      if(part->IsChargedHadron())
	if (part->Pt() > minpt && part->Pt() < maxpt)
	  {
	    Double_t energy = TMath::Sqrt(part->Px() * part->Px() + 
					  part->Py() * part->Py() + 
					  part->Pz() * part->Pz());
	    
	    fastjet::PseudoJet pseudoJet(part->Px(), 
					 part->Py(), 
					 part->Pz(), 
					 energy);
	    pseudoJet.set_user_index(ip+indexOffset); // keep track of particles
	    vec.push_back(pseudoJet);
	  }
    }
  return vec;
}

std::vector<fastjet::PseudoJet> 
make_HT_trigger_vector(TStarJetVectorContainer<TStarJetVector> *container,
			  double minpt, double maxpt, Int_t indexOffset)
{
  TStarJetVector *part = 0;
  std::vector<fastjet::PseudoJet> vec;
    
  for (Int_t ip = 0; ip < container->GetEntries(); ip++)
    {
      part = container->Get(ip);
      if(!part->IsChargedHadron())
	if (part->Pt() > minpt && part->Pt() < maxpt)
	  {
	    Double_t energy = TMath::Sqrt(part->Px() * part->Px() + 
					  part->Py() * part->Py() + 
					  part->Pz() * part->Pz());
	    
	    fastjet::PseudoJet pseudoJet(part->Px(), 
					 part->Py(), 
					 part->Pz(), 
					 energy);
	    pseudoJet.set_user_index(ip+indexOffset); // keep track of particles
	    vec.push_back(pseudoJet);
	  }
    }
  return vec;
}

/*
std::vector<fastjet::PseudoJet> 
make_efficorrected_charged_input_vector(TStarJetVectorContainer<TStarJetVector> *container,
			  double minpt, double maxpt, Int_t indexOffset)
{
  TStarJetVector *part = 0;
  std::vector<fastjet::PseudoJet> vec;
    
  for (Int_t ip = 0; ip < container->GetEntries(); ip++)
    {
      part = container->Get(ip);
      if(part->IsChargedHadron())
	if (part->Pt() > minpt && part->Pt() < maxpt)
	  {
	    Double_t energy = TMath::Sqrt(part->Px() * part->Px() + 
					  part->Py() * part->Py() + 
					  part->Pz() * part->Pz());

		 Double_t pT_part = part->perp();
		 Double_t eta_part= part->pseudorapidity();
		 Double_t epsilon=efficiency(eta_part, pT_part);
		 if(epsilon>0){
			Int_t count=(Int_t)(1/epsilon)-1;
			Double_t prob=1-(((Int_t)(1/epsilon))*epsilon);	    
			if(gRandom->Uniform(0,1)<prob){count+=1;}
			for(Int_t addpart=0; addpart<count; addpart++){
				Double_t smear;
				smear=gRandom->Gaus(0,0.04);
				Double_t px=part->Px()+smear*part->Px();
				smear=gRandom->Gaus(0,0.04);
				Double_t py=part->Py()+smear*part->Py();
				smear=gRandom->Gaus(0,0.04);
				Double_t pz=part->Pz()+smear*part->Pz();
				Double_t en=px*px+py*py+pz*pz;
	    		fastjet::PseudoJet pseudoJetAdd(px,py,pz,en);
	    pseudoJetAdd.set_user_index(ip+indexOffset+10000+addpart); // keep track of particles
	    vec.push_back(pseudoJetAdd);
			}
		 } 		
	    fastjet::PseudoJet pseudoJet(part->Px(), 
					 part->Py(), 
					 part->Pz(), 
					 energy);
	    pseudoJet.set_user_index(ip+indexOffset); // keep track of particles
	    vec.push_back(pseudoJet);
	  }
    }
  return vec;
}
*/

//_____________________________________________________________________
Double_t Eff_track_rec_function(Double_t* x,Double_t* par)
{
    // Track reconstruction efficiency parametrization
    Double_t pt,y;
    Double_t A,B,C;A=par[0];B=par[1];C=par[2];
    pt=x[0];
    y=A*(exp(-pow(B/pt,C)));
    return y;
}

//_____________________________________________________________________

Double_t efficiency08(Double_t eta, Double_t pT)
{
if(pT>6)pT=6;

	Double_t eff;
	Double_t C0=0.632;
	Double_t C1=0.118;
	Double_t C2=-0.290;
	Double_t C3=0.523;
	Double_t C4=-0.570;
	Double_t C5=-1.39;
	Double_t C6=-6.73;
	Double_t C7=0.0588;
	Double_t C8=-0.00687;
	Double_t C9=0.111;
	Double_t C10=0.295;
	Double_t C11=0.145;
	Double_t C12=0.296;
	Double_t C13=0.00291;

	eff=C0+C1*eta*eta+C2*TMath::Power(eta,4)+C3*TMath::Power(eta,6)+C4*TMath::Power(eta,8)+C5*TMath::Exp(C6*pT)+C7*pT+C8*pT*pT+C9*TMath::Exp(-((TMath::Abs(eta)-C10)*(TMath::Abs(eta)-C10)/C11)-((TMath::Abs(pT)-C12)*(TMath::Abs(pT)-C12)/C13));

	return eff;
}
//_____________________________________________________________________

Double_t efficiency11(Double_t pt, TF1* effLow, TF1* effHigh)
{
	Double_t eff;
	if(pt<=1.2)eff = effLow->Eval(pt);
	else eff = effHigh->Eval(pt);
   //eff=eff+eff*0.05;
return eff;
}

//_____________________________________________________________________

Double_t efficiencyAlex(Double_t pt,Double_t increment,bool kcentral)
{
   if(pt>9.9)pt=9.9;
   TF1* f_Efficiency = new TF1("f_EfficiencyCent",Eff_track_rec_function,0,10.0,3);
   if(kcentral)
      f_Efficiency->SetParameters(7.45643e-01,1.43725e-01,2.02904e+00);
   else
      f_Efficiency->SetParameters(9.06946e-01,1.45242e-01,2.87409e+00);
   Double_t eff=f_Efficiency->Eval(pt);
   eff=eff+increment;
	delete f_Efficiency;
return eff;
}

