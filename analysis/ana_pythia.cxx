#include <TSystem.h>
#include <TFile.h>
#include <TH1I.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TH2D.h>
#include <TRandom.h>
#include <TTree.h>
#include <TParticle.h>
#include <TClonesArray.h>
#include <TBranch.h>
#include <TStopwatch.h>

#include <TStarJetPicoReader.h>
#include <TStarJetPicoEventCuts.h>
#include <TStarJetPicoEvent.h>
#include <TStarJetPicoEventHeader.h>
#include <TStarJetPicoV0Cuts.h>
#include <TStarJetPicoTrackCuts.h>
#include <TStarJetPicoUtils.h>
#include <TStarJetVectorContainer.h>
#include <TStarJetVector.h>

#include "jet.h"
#include "fourvector.h"
#include "fjwrapper.h"
#include "utils.h"
#include "checker.h"
#include "PySimSimulationHeader.h"
//#include "stdhistograms.h"
//#include "ppauau.h"
//#include "randomcones.h"

#include <iostream>
using namespace std;

bool is_inside(Double_t eta1, Double_t eta2, Double_t phi1, Double_t phi2, Double_t R)
{
	Double_t deta2=(eta1-eta2)*(eta1-eta2);
   Double_t dphi = TMath::Abs(phi1-phi2);
      if(dphi>TMath::Pi())dphi=(2*TMath::Pi())-dphi;
      Double_t dphi2=dphi*dphi;

	if(TMath::Sqrt(dphi2+deta2)<R)
		return true;
	else 
		return false;
}
   

void ana_pythia(Long64_t nev)
{
  TStopwatch timer;
  timer.Start();
  
  Int_t dete_jets_only=atoi(gSystem->Getenv("DETE_JETS_ONLY"));

  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  TString dataDir = gSystem->Getenv("OUTPUTDIR");
  TString eventDir = gSystem->Getenv("INPUTDIR");

  double r = atof(gSystem->Getenv("RPARAM"));
  double acut = atof(gSystem->Getenv("ACUT"));

  cout << Form("[i] WRKDIR (jets)=%s", dataDir.Data()) << endl;
  cout << Form("[i] WRKDIR (events)=%s", eventDir.Data()) << endl;
  cout << Form("[i] R=%1.1f Acut=%.1lf", r, acut) << endl;

  Double32_t rho = 0;     //[0, 0, 16]
  Double32_t sigma = 0;   //[0, 0, 16]
  //Double32_t rho2 = 0;     //[0, 0, 16]
  //Double32_t sigma2 = 0;   //[0, 0, 16]

//INPUT - jets
  TString str = "null";
  str = Form("%s/pythia_R%.1lf.root", dataDir.Data(), r);
  if(dete_jets_only)
  str = Form("%s/pythia_R%.1lf_eff.root", dataDir.Data(), r);
  TFile *finput = new TFile(str.Data(), "OPEN");
  TTree* ftreejets = (TTree*)finput->Get("PythiaJets");
  TH1D* htrackpT=(TH1D*)finput->Get("htrackpT");
  TClonesArray *pythiaarr = new TClonesArray("jet", 10);
  ftreejets->SetBranchAddress("rho", &rho);
  ftreejets->SetBranchAddress("sigma", &sigma);
  ftreejets->SetBranchAddress("akt_pythia", &pythiaarr);

  //TTree* ftreejets2;
  //TClonesArray *pythiaarr2;
/*  if(dete_jets_only){
  str = Form("%s/pythia_R%.1lf_eff.root", dataDir.Data(), r);
  finput = new TFile(str.Data(), "OPEN");
  ftreejets2 = (TTree*)finput->Get("PythiaJets");
  pythiaarr2 = new TClonesArray("jet", 10);
  ftreejets2->SetBranchAddress("rho", &rho2);
  ftreejets2->SetBranchAddress("sigma", &sigma2);
  ftreejets2->SetBranchAddress("akt_pythia", &pythiaarr2);
	}*/

//INPUT - events 
  str = Form("%s/pythia.root", eventDir.Data());
  TFile *fevents = new TFile(str.Data(), "OPEN");
  //TClonesArray *aparticles = new TClonesArray("TParticle", 100);
  //TTree * tevents = (TTree*)fevents->Get("event");
  //tevents->SetBranchAddress("particles", &aparticles);

  PySimSimulationHeader* header = (PySimSimulationHeader*)fevents->Get("PySimSimulationHeader");

 
//OUTPUT
  str = Form("%s/histos_pythiajet_R%.1lf.root", dataDir.Data(), r);
if(dete_jets_only)
  str = Form("%s/histos_pythiajet_R%.1lf_eff.root", dataDir.Data(), r);
  TFile *foutput = new TFile(str.Data(), "RECREATE");


//scale factors - for connecting pT hard bins
Double_t xsection = header->GetXsection();
//Int_t nentries = tevents->GetEntries();
Double_t nevents = header->GetNevents();
Double_t ntrials = header->GetNtrials();


//histogram definitions
  Int_t nptbins=1000;
  Float_t ptminbin=0;
  Float_t ptmaxbin=50;
  Int_t netabins=100*2*(1-r);
  Float_t etaminbin=-(1-r);
  Float_t etamaxbin=1-r;
  Int_t nphibins=120;
  Float_t phiminbin=-TMath::Pi();
  Float_t phimaxbin=TMath::Pi();

  const Int_t npTlead=6;
  Float_t pTleadCuts[npTlead]={0,3,4,5,6,7};

//HISTOGRAMS
  	TH1I *hevts = new TH1I("hevts", "hevts", 2, 0, 2);
  	TH1D *hpT_pTl[npTlead];
  	TH1D *hpT_pTl_dete[npTlead];
	for(int pTlcut=0; pTlcut<npTlead; pTlcut++)
	{
		TString hname=Form("hpT_pTl%.0lf",pTleadCuts[pTlcut]);
      TString hdesc=Form("jet pT for pTlead>%.0lf ; p_{T} [GeV/c]",pTleadCuts[pTlcut]);
		hpT_pTl[pTlcut] = new TH1D(hname,hdesc, nptbins, ptminbin, ptmaxbin); 	
		hpT_pTl[pTlcut]->Sumw2();

		hname=Form("hpT_pTl%.0lf_dete",pTleadCuts[pTlcut]);
		hdesc=Form("detector level jet pT for pTlead>%.0lf ; p_{T} [GeV/c]",pTleadCuts[pTlcut]);
		hpT_pTl_dete[pTlcut] = new TH1D(hname,hdesc, nptbins, ptminbin, ptmaxbin); 	
		hpT_pTl_dete[pTlcut]->Sumw2();
	}
  TH2D *heta_phi = new TH2D("heta_phi", "jet eta vs phi;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  //TH2D *hpT_pTsmear = new TH2D("hpT_pTsmear", "jet pT smearing; p_{T}^{true} [GeV/c]; (p_{T}^{smeared}-p_{T}^{true})/p_{T}^{true}", nptbins, ptminbin, ptmaxbin, 200,-1,1);

//----------------------------

//define bad TPC sectors, bad days,...
   checker * eventCheck = new checker();
//----------------------------

  for(Int_t ievt = 0; ievt < ftreejets->GetEntries(); ievt++)
   {
      ftreejets->GetEntry(ievt);
		//if(dete_jets_only)
      //ftreejets2->GetEntry(ievt);
      
      hevts->Fill(1);

      for(Int_t ijet = 0; ijet < pythiaarr->GetEntries(); ijet++)
	   {
 	   	jet *pythiajet = (jet*)pythiaarr->At(ijet);
	   	TLorentzVector jetlv = (pythiajet->jet_fv).GetTLorentzVector();
	   	jetlv.SetPhi(TVector2::Phi_mpi_pi(jetlv.Phi()));

	   	Double_t area=pythiajet->area;
		   //Double_t pTcorr = jetlv.Pt() - area * rho;
		   Double_t pT = jetlv.Pt();
		   Double_t phi = jetlv.Phi();
		   Double_t eta = jetlv.Eta();
		   Double_t pTlead = pythiajet->pTleading;
	   	Int_t nconst = pythiajet->Nconst;

			Double_t pT_dete;
			Double_t dpT;
			Double_t pTlead_dete;
			Double_t eta_dete;
			if(!dete_jets_only){			
		   	pT_dete=pythiajet->pTDete;
		   	eta_dete=pythiajet->etaDete;
				dpT=pT_dete-pT;			
			   pTlead_dete=pythiajet->pTleadingDete;
			}
			//bool matched=false;

			//set acceptance
		   Float_t etaMinCut=-(1-r);
			Float_t etaMaxCut=(1-r);

	   	if(area < acut) continue;

			//---filling histograms--------------

			if(eta>etaMinCut && eta<etaMaxCut)
			{
				heta_phi->Fill(eta,phi);

				for(int pTlcut=0; pTlcut<npTlead; pTlcut++)
			   {			
				if(pTlead>pTleadCuts[pTlcut])
				hpT_pTl[pTlcut]->Fill(pT);}

			}
			if(!dete_jets_only){			
			if(eta_dete>etaMinCut && eta_dete<etaMaxCut){
				for(int pTlcut=0; pTlcut<npTlead; pTlcut++)
            {
            if(pTlead>pTleadCuts[pTlcut])
				hpT_pTl_dete[pTlcut]->Fill(pT_dete);}
			}
		/*
			if(eta>etaMinCut && eta<etaMaxCut)
				hpT_pTsmear->Fill(pT,(pT_dete-pT)/pT);
			for(Int_t pTl=0; pTl<npTlead; pTl++){
			if((eta>etaMinCut && eta<etaMaxCut) )
				{
					heffi[pTl]->Fill(-pT); //efficiency - fill particle level jet pT
					if(pTlead_dete>0)
					   hdpT2D[pTl]->Fill(pT,dpT); //fill deltapT histograms
				}
			if((eta_dete>etaMinCut && eta_dete<etaMaxCut) && pTlead_dete>pTl)
				{ 
				   heffi[pTl]->Fill(pT); //efficiency - fill pT of particle level jet with detected detector level jet
				}
			}//pTleading cut loop
		*/
			}//!dete_jets_only
     	}//jet loop
   }//event loop

//SCALE HISTOGRAMS
Double_t jetscale = 1./(2.*TMath::Pi()*2*(1-r)) * (nevents/ntrials * xsection);
Double_t scale = (nevents/ntrials * xsection);

cout<<"xsection: "<<xsection<<endl;
//cout<<"path: "<<eventDir.Data()<<endl;

for(int pTlcut=0; pTlcut<npTlead; pTlcut++)
            {
hpT_pTl[pTlcut]->Scale(jetscale, "width");
hpT_pTl_dete[pTlcut]->Scale(jetscale, "width");
htrackpT->Scale(scale);
}


/*
for(int binx=1; binx<hpT_pTsmear->GetNbinsX();binx++){
 Double_t intg=hpT_pTsmear->Integral(binx,binx,1,hpT_pTsmear->GetNbinsY());
for(int biny=1; biny<hpT_pTsmear->GetNbinsY();biny++){
 Double_t bincon=hpT_pTsmear->GetBinContent(binx,biny);
if(intg>0)hpT_pTsmear->SetBinContent(binx,biny,bincon/intg);
}
}*/
  foutput->cd();
  foutput->Write();
  foutput->Close();
  delete foutput;

  finput->Close();
  delete finput;

  timer.Stop();
  timer.Print();
}
