#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc
module load use.own
module load starjet/starjet

TRG=$1
BASEDIR="$HOME/jet_analysis/STARJet"
OUTDIR="out/$TRG/embedding2"
MacroDir="$BASEDIR/macros"

export TYPE="charged"
export RPARAM=0.2
#export RPARAM=0.4
export MAXRAP=1.0
export REFMULT=396 # YEAR 11 DATA, 0-10%
export ZVERTEX=30
#export ACUT=0.4
export ACUT=0.09

cd $MacroDir

FILELISTFILE="$HOME/jet_analysis/run11/xrootd_"$TRG".list"
#MAX=1
LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"
NFILES=1 #how many files to merge together
N=50 #take every Nth batch
let ADD=$NFILES*$N 
for ((FIRST=2; FIRST < MAX; FIRST += $ADD))
  do
  export FIRST
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/${FIRST}"
  export INPUTDIR="${BASEDIR}/${OUTDIR}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
  fi
root -b -q -l runAnaEmbedding.C 2>&1
done

