#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet

cd $MacroDir

PTHARD=("3pThard4" "4pThard5" "5pThard7" "7pThard9" "9pThard11" "11pThard15" "15pThard20" "20pThard25" "25pThard30" "30pThard35" "35pThard40" "40pThard50" "50pThard60" "60pThard-1")

for ((SET=0; SET < 10; SET += 1))
#for ((SET=0; SET < 2; SET += 1))
  do
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/pythia_${TYPE}/"${PTHARD[$PTBIN]}"/${SET}"
  export INPUTDIR="${HOME}/jet_analysis/pythia/"${PTHARD[$PTBIN]}"/"${SET}


root -b -q -l runPythia.C 2>&1
root -b -q -l runAnaPythia.C 2>&1
done
