#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet

cd $MacroDir

#root -b -q -l 'runEmbedding.C(10000)' 2>&1
root -b -q -l runEmbedding.C 2>&1
root -b -q -l runAnaEmbedding.C 2>&1
