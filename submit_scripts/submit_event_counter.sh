#!/bin/bash

TRG=$1
SCRIPT="run_qa"

BASEDIR="/home/rusnak/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
OUTDIR="out_test/$STYPE"
export MacroDir="$BASEDIR/macros"

export TRIGGER=$TRG"11" #trigger names for run11 are MB11 or HT11 (this is set in the eventStructure) - however "All" can be used also, since there are only HT/MB events in the filelists
export FILELISTFILE="$HOME/jet_analysis/run11/"$TRG"_run11_tmp.list"
export NFILES=

#MAX=1
MAX=614

let ADD=$NFILES*1  #take every Nth batch
for ((FIRST=0; FIRST < MAX; FIRST += $ADD))
  do
  export NAME=qa_${FIRST}
  export FIRST
  export NSKIPFILES=${FIRST}
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/qa_${TRIGGER}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
  fi
qsub -P star -M rusn@email.cz -m a -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPT.sh
done
