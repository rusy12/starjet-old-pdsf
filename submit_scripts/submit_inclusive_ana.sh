#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

export TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage


BASEDIR="/home/rusnak/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
export NAME="inclusive_ana"

#sh run_inclusive_ana.sh
qsub -P star -M rusn@email.cz -m a -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_inclusive_ana.sh
