#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE"
    exit 1
}

TRG=$1 #HT, MB
export RMATRIX_TYPE=$2 #BG_sp BG_pyt dete BGD - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "TRIGGER: $TRG"
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

prior_type=(flat pythiadete pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex)

BASEDIR="$HOME/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
export MacroDIR="$BASEDIR/macros/unfolding"
WRKDIR="$BASEDIR/out/$TRG"
export SVD=1 # SVD unfolding instead of Bayes
#export NBINS=100
export NBINS=VAR
export NITER=9 #number of k-terms
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
   export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
export EFFICORR=1 # do efficiency correction
CENTRAL=1
SUF="" #output dir suffix



if [ $CENTRAL -eq 1 ]; then
	SUFFIX=""
	PTLEADCUTS="5 7"
else
	SUFFIX="_peripheral"
	PTLEADCUTS="3 4 5"
fi
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi


for SYSSUFF in "_gluon" #"_normal" "_m5" "_p5" "_2u1g" #choice of different results for sys. err. calculation: "_normal" | "_m5" | "_p5" | "_2u1g" | "_AuAu"
do
for RPARAM in 0.2 0.4
do
export RPARAM
export DATA_PATH="$WRKDIR/inclusive_R${RPARAM}${SUFFIX}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding_R${RPARAM}${SUFFIX}/rmatrix${SYSSUFF}"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUFF}/rmatrix"

#choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
for BININGCH in 0 1 2 
do
export BININGCH 

for PRIOR in 2 4 5 6 7 8 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:pT^(-6) 7: levy I 8: levy II
do
   OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_SVD_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUF}${SYSSUFF}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
	export OUT_DIR
   export PRIOR

	for PTTHRESH in `echo $PTLEADCUTS`
   do
     export PTTHRESH
      NAME="unfSVD_R${RPARAM}_pTl${PTTHRESH}_$PRIOR"
		qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_unfoldSVD.sh
		#cd $MacroDIR
		#root -b -l -q unfold_roounfold.C
	done #pTleading
	done #prior
done #bining
done #R
done #suffix
