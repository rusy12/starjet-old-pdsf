#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

BASEDIR="/home/rusnak/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"

export NAME="embedding_ana";
qsub -P star -M rusn@email.cz -m a -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_embedding_ana.sh $TRG
