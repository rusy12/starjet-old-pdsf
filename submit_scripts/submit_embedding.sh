#!/bin/bash

SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

BASEDIR="$HOME/jet_analysis/STARJet"
OUTDIR="out/$TRG"
export MacroDir="$BASEDIR/macros"
LOGDIR="$BASEDIR/submit_scripts/log"

export TRIGGER=$TRG"11" # YEAR11: HT11, MB11
export CHARGED=1
ALEXPICO=1 #Alex's or my picodst

for CENTRALITY in "0-10" #"60-80" 
do
for RPARAM in 0.2 #0.3 0.4
do
export RPARAM
export TYPE="charged_R${RPARAM}_${CENTRALITY}cent_kTR04"
export MAXRAP=1.0
if [ $CENTRALITY == "0-10" ]; then
	REFMULTMIN=396 # YEAR 11 DATA, 0-10%
	REFMULTMAX=10000 # YEAR 11 DATA, 0-10%
   NJETSREMOVE=3
	CENTRALITY_LIST="0-10" #for alex's picodsts
elif [ $CENTRALITY == "60-80" ]; then
	REFMULTMIN=10 # YEAR 11 DATA, 60-80%
	REFMULTMAX=31 # YEAR 11 DATA, 60-80%
   NJETSREMOVE=2
	CENTRALITY_LIST="60-80" 
fi
export REFMULTMIN
export REFMULTMAX
export NJETSREMOVE

export PTEMBFIXED=1 # 1 - probe pT fixed, 0 - probe pT in an interval
# FOR HISTOGRAMS

   if [ $RPARAM == "0.2" ]; then
      ACUT=0.09
   fi
   if [ $RPARAM == "0.3" ]; then
      ACUT=0.2
   fi
   if [ $RPARAM == "0.4" ]; then
      ACUT=0.4
   fi
export ACUT
echo "Area cut: $ACUT"

export NFILES=2 #number of files per job
if [ $ALEXPICO -eq 1 ]; then
	FILELISTFILE="$HOME/jet_analysis/run11/alex_${CENTRALITY_LIST}.list"
	SCRIPTNAME="run_embedding_alex.sh"
else
	FILELISTFILE="$HOME/jet_analysis/run11/xrootd_${TRG}.list"
	SCRIPTNAME="run_embedding.sh"
fi
export FILELISTFILE

#MAX=1
LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"
N=20 #take every Nth batch
let ADD=$NFILES*$N 
for ((FIRST=1; FIRST < MAX; FIRST += $ADD))
  do
  export FIRST
  export NSKIPFILES=$FIRST
  export NAME="embedding_${TRG}_${FIRST}"
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/emb_${TRIGGER}_year11_${TYPE}/${FIRST}"
  export INPUTDIR="${BASEDIR}/${OUTDIR}/emb_${TRIGGER}_year11_${TYPE}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
  fi

  qsub -P star -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME
done
done
done
