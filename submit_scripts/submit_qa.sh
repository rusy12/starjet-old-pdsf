#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

SCRIPT="run_qa"
BASEDIR="$HOME/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
OUTDIR="out/${TRG}/qa_test"
export MacroDir="$BASEDIR/macros"
export REFMULT=396 #0-10% run11
export TRIGGER=$TRG"11" #trigger names for run11 are MB11 or HT11 (this is set in the eventStructure) - however "All" can be used also, since there are only HT/MB events in the filelists

export FILELISTFILE="$HOME/jet_analysis/run11/xrootd_"$TRG".list"
export NFILES=4 #how many files merge into one batch
#MAX=1
LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"

N=100 #take every Nth batch
let ADD=$NFILES*$N 
for ((FIRST=0; FIRST < MAX; FIRST += $ADD))
  do
  export NAME=qa_${FIRST}
  export FIRST
  export NSKIPFILES=${FIRST}
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/qa_${TRIGGER}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
  fi
qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPT.sh
done
