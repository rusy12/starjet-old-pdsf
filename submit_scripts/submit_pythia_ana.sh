#!/bin/bash
export BASEDIR="/home/rusnak/jet_analysis/STARJet"
export OUTDIR="out/Pythia"
export MacroDir="$BASEDIR/macros"
LOGDIR="$BASEDIR/submit_scripts/log"

export RPARAM=0.3
	if [ $RPARAM == "0.2" ]; then
		ACUT=0.09
	fi
	if [ $RPARAM == "0.3" ]; then
	   ACUT=0.2
	fi
	if [ $RPARAM == "0.4" ]; then
   	ACUT=0.4
	fi

export ACUT
echo "R: $RPARAM"
echo "Area cut set to: $ACUT"

export TYPE="charged_R${RPARAM}_pTsmear_only"
export CHARGED=1
export MAXRAP=1.0
export EFFICORR_JETS_ONLY=0 #save only detector level jets 
#export EFFICORR=0 #apply efficiency correction (for detector level jets)
#export PTSMEAR=1 #simulate track pT smearing (due to detector resolution) (for detector level jets)

for ((SET=0; SET < 10; SET += 1))
  do
  export SET
  NAME=pythia_ana
  qsub -P star -M rusn@email.cz -m a -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_pythia_ana.sh
done
