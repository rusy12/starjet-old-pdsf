#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc
module load use.own
module load starjet/starjet

BASEDIR="$HOME/jet_analysis/STARJet"
OUTDIR="out/$TRG"
MacroDir="$BASEDIR/macros"

export RPARAM=0.4
export TRIGGER=$TRG"11" # YEAR11: HT11, MB11
export TYPE="charged_R${RPARAM}_0-10cent_ep"
export CHARGED=1
export MAXRAP=1.0
#export REFMULT=466 # YEAR 11 DATA, 0-5%
export REFMULT=396 # YEAR 11 DATA, 0-10%
export ACUT=0.4

cd $MacroDir

export NFILES=4
export FILELISTFILE="$HOME/jet_analysis/run11/xrootd_"$TRG".list"
#MAX=1
LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"

N=2 #take every Nth batch
let ADD=$NFILES*$N 
for ((FIRST=1; FIRST < MAX; FIRST += $ADD))
  do
  export FIRST
  export NSKIPFILES=$FIRST
  export NAME=inclusive_${FIRST}
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/inclusive_${TRIGGER}_year11_${TYPE}/${FIRST}"
  export INPUTDIR="${BASEDIR}/${OUTDIR}/inclusive_${TRIGGER}_year11_${TYPE}/${FIRST}"
#  if [ ! -e $OUTPUTDIR ]; then
#      mkdir -p $OUTPUTDIR
#  fi
root -b -q -l runAnaInclusive.C 2>&1
done

