#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER "
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

BASEDIR="$HOME/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
export WORKDIR="$BASEDIR/macros/response_matrix"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=1 # correct delta pT for event plane bias
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #path to correction histograms

for SUFF in "_peripheral" ""
do
   if [ $SUFF == "_peripheral" ]; then
   CENTRAL=0
   else
   CENTRAL=1
   fi
export CENTRAL
echo "Central: $CENTRAl"

for RPARAM in 0.2 0.3 0.4
do
export RPARAM
export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/STARJet/out/${TRG}/embedding_R${RPARAM}$SUFF"
for PTLEAD in 3 4 5 7
do
	export PTLEAD
   NAME="buildRM_R${RPARAM}_pTl${PTLEAD}"
qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRM.sh 
done
done
done
