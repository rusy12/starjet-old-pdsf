#!/bin/bash
export BASEDIR="$HOME/jet_analysis/STARJet"
export OUTDIR="out/Pythia"
export EFFPATH="$BASEDIR/analysis/efficiency"
export MacroDir="$BASEDIR/macros"
LOGDIR="$BASEDIR/submit_scripts/log"

for RPARAM in 0.2 0.4 #0.4
do
export RPARAM

	if [ $RPARAM == "0.2" ]; then
		ACUT=0.09
	fi
	if [ $RPARAM == "0.3" ]; then
	   ACUT=0.2
	fi
	if [ $RPARAM == "0.4" ]; then
   	ACUT=0.4
	fi
	if [ $RPARAM == "0.6" ]; then
   	ACUT=0.8
	fi
export ACUT
echo "R: $RPARAM"
echo "Area cut set to: $ACUT"
export CHARGED=1
export CENTRAL=1 #central|peripheral events (for delta-pT and efficiency selection)
export MAXRAP=1.0
export DETE_JETS_ONLY=0 #save only detector level jets 
export EFFICORR=1 #apply efficiency correction (for detector level jets)
export PTSMEAR=1 #simulate track pT smearing (due to detector resolution) (for detector level jets)
export DPTSMEAR=0 #smear jets with delta-pT
export V2CORR=1 #use v2-corrected delta-pT histograms
export V2PATH="$HOME/jet_analysis/STARJet/analysis/EP_corrections" #pah to v2-correction coefficients
export PTLEAD=0 #pTleading cut (for delta-pT selection)

if [ $CENTRAL -eq 1 ]; then
SUFF=""
else
SUFF="_peripheral"
fi
export DPTPATH="$HOME/jet_analysis/STARJet/out/MB/embedding_R${RPARAM}$SUFF" #path to delta-pT histograms
if [ $PTSMEAR -eq 1 ]; then
SUFF1="_pTsmear"
else
SUFF1=""
fi
if [ $EFFICORR -eq 1 ]; then
SUFF2="_effi"
else
SUFF2=""
fi
if [ $DPTSMEAR -eq 1 ]; then
SUFF3="_dpTsmear"
else
SUFF3=""
fi
#export TYPE="charged_R${RPARAM}_pTsmear_effi_dpTsmear${SUFF}"
export TYPE="charged_R${RPARAM}${SUFF}${SUFF1}${SUFF2}${SUFF3}_allpThardbins"

# FOR HISTOGRAMS
PTHARD=("3pThard4" "4pThard5" "5pThard7" "7pThard9" "9pThard11" "11pThard15" "15pThard20" "20pThard25" "25pThard30" "30pThard35" "35pThard40" "40pThard50" "50pThard60" "60pThard-1")
#WIDTH=(1 1 2 2 2 4 5 5 5 5 5 10 10 10)
for PTBIN in `seq 0 13` 
#for PTBIN in `seq 0 1` 
do
   export PTBIN
	for ((SET=0; SET < 10; SET += 1))
   do
   OUTPUTDIR="${BASEDIR}/${OUTDIR}/pythia_${TYPE}/"${PTHARD[$PTBIN]}"/${SET}"
   if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
   fi
   done

   NAME="pythia_${PTBIN}_R$RPARAM"
   qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_pythia.sh
done
done
