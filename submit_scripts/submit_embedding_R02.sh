#!/bin/bash

SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER"
    exit 1
}

TRG=$1 #HT, MB
echo "TRIGGER: $TRG"
#check arguments
[ -n "$TRG" ] || _usage

BASEDIR="/home/rusnak/jet_analysis/STARJet"
OUTDIR="out/$TRG"
export MacroDir="$BASEDIR/macros"
LOGDIR="$BASEDIR/submit_scripts/log"

export TRIGGER=$TRG"11" # YEAR11: HT11, MB11
export TYPE="charged_R02_0-10cent_pyth_28probes"
export CHARGED=1
export RPARAM=0.2
#export RPARAM=0.2
export MAXRAP=1.0
#export REFMULT=466 # YEAR 11 DATA, 0-5%
export REFMULT=396 # YEAR 11 DATA, 0-10%
#export REFMULT=441 # YEAR 10 DATA, 0-5%
#export REFMULT=266 # YEAR 10 DATA, 0-20%
export PTEMBFIXED=1 # 1 - probe pT fixed, 0 - probe pT in an interval
# FOR HISTOGRAMS
#export ACUT=0.4 #for R=0.4
export ACUT=0.09 #for R=0.2

#export FILELISTFILE="$HOME/jet_analysis/run10/run10_filelists/run10_central_ok.list"
export FILELISTFILE="$HOME/jet_analysis/run11/xrootd_"$TRG".list"

export NFILES=1
#MAX=1
LINES=`cat $FILELISTFILE | wc -l` #number of files in filelist
let MAX=`expr $LINES + 1`
echo "MAX set to: $MAX"
N=50
let ADD=$NFILES*$N #take every Nth batch
for ((FIRST=2; FIRST < MAX; FIRST += $ADD))
  do
  export FIRST
  export NSKIPFILES=$FIRST
  export NAME="embedding_${TRG}_${FIRST}"
  export OUTPUTDIR="${BASEDIR}/${OUTDIR}/emb_${TRIGGER}_year11_${TYPE}/${FIRST}"
  export INPUTDIR="${BASEDIR}/${OUTDIR}/emb_${TRIGGER}_year11_${TYPE}/${FIRST}"

  if [ ! -e $OUTPUTDIR ]; then
      mkdir -p $OUTPUTDIR
  fi

  qsub -P star -M rusn@email.cz -m a -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_embedding_R02.sh
done
