#!/bin/bash

source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet

cd $MacroDir

root -b -q -l runInclusiveAlex.C 2>&1
root -b -q -l runAnaInclusive.C 2>&1
