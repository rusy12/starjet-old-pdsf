#!/bin/bash
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load starjet/starjet

cd $MacroDIR

if [ $NBINS == "VAR" ]; then
  root -b -l -q unfold_roounfold_uneqbin.C
else
  root -b -l -q unfold_roounfold.C
fi

