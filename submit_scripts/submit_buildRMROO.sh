#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} TRIGGER RMATRIX_TYPE (BG_sp | BG_pyt | dete | BGD)"
    exit 1
}

TRG=$1 #HT, MB
export RMATRIX_TYPE=$2 #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TRIGGER: $TRG"
echo "TYPE: $RMATRIX_TYPE"
#check arguments
[ -n "$TRG" ] || _usage
[ -n "$RMATRIX_TYPE" ] || _usage

BASEDIR="$HOME/jet_analysis/STARJet"
LOGDIR="$BASEDIR/submit_scripts/log"
export WORKDIR="$BASEDIR/macros/response_matrix"
CENTRAL=1

if [ $CENTRAL -eq 1 ]; then
CENTSUFF=""
PTLEADCUTS="5 7"
else
CENTSUFF="_peripheral"
PTLEADCUTS="3 4 5"
fi

for SUFF in "_AuAu" #"_m5" "_p5" "_normal" "_2u1g"
do
for RPARAM in 0.2 0.3 0.4 
do
export RPARAM
export RM_PATH="$BASEDIR/out/${TRG}/embedding_R${RPARAM}${CENTSUFF}/rmatrix${SUFF}"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${SUFF}"
export PRIOR_PATH="$HOME/jet_analysis/STARJet/out/prior"

for PTTHRESH in `echo $PTLEADCUTS`
do
	for PRIOR in 2 4 5 6 7 8 #0: -, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4.5) 5: pT^(-5) 6:pT^(-5.5) 7: levy 8: levy II
	do
      export PTTHRESH
		export PRIOR
      NAME="buildRMROO_${RMATRIX_TYPE}_R${RPARAM}"
qsub -P star -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRMROO.sh $TRG $TYPE
#sh run_buildRMROO.sh $TRG $TYPE
	done
done #pTtresh
done #R
done #SUFFIX
