#!/bin/csh

setenv JETPICODIR ${HOME}/jet_analysis/STARJet/software/eventStructure
set LD_LIBRARY_PATH = ( ${JETPICODIR}:${LD_LIBRARY_PATH} )
setenv FASTJETDIR ${HOME}/jet_analysis/STARJet/software/fastjet
set LD_LIBRARY_PATH = ( ${FASTJETDIR}/lib:${LD_LIBRARY_PATH} )
setenv STARJETBASEDIR ${HOME}/jet_analysis/STARJet
setenv JETPICODIR  ${HOME}/jet_analysis/STARJet/software/eventStructure
setenv FJWRAPPER ${HOME}/jet_analysis/STARJet/software/fjwrapper
setenv ROOTUTILS ${HOME}/jet_analysis/STARJet/software/rootutil
setenv ANALYSISDIR ${HOME}/jet_analysis/STARJet/analysis

#setenv KTJETDIR ${HOME}/run10/ana/software/ktJet
#set PATH = ( ${KTJETDIR}:${PATH} )
#set LD_LIBRARY_PATH = ( ${KTJETDIR}:${LD_LIBRARY_PATH} )
